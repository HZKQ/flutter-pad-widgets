import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'kq_flutter_pad_widgets_method_channel.dart';

abstract class KqFlutterPadWidgetsPlatform extends PlatformInterface {
  /// Constructs a KqFlutterPadWidgetsPlatform.
  KqFlutterPadWidgetsPlatform() : super(token: _token);

  static final Object _token = Object();

  static KqFlutterPadWidgetsPlatform _instance = MethodChannelKqFlutterPadWidgets();

  /// The default instance of [KqFlutterPadWidgetsPlatform] to use.
  ///
  /// Defaults to [MethodChannelKqFlutterPadWidgets].
  static KqFlutterPadWidgetsPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [KqFlutterPadWidgetsPlatform] when
  /// they register themselves.
  static set instance(KqFlutterPadWidgetsPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
