
import 'kq_flutter_pad_widgets_platform_interface.dart';

class KqFlutterPadWidgets {
  Future<String?> getPlatformVersion() {
    return KqFlutterPadWidgetsPlatform.instance.getPlatformVersion();
  }
}
