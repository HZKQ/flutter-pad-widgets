import '../resources/l10n/kq_pad_strings.dart';

/// 默认主题配置
const String kqPadThemeConfigId = 'KQ_PAD_THEME_CONFIG_ID';

class KqPadGlobal {

  /// 组件库包名，组件库里面的图片引用需要用到这个
  static const String packageName = 'kq_flutter_pad_widgets';

  /// [KqPadTabBar] 左右滚动的开关的Mask，如果=true，则一定能滚动，否则根据控件里的[KqPadTabBar]的[scrollable]控制。
  static bool tabBarScrollable = false;

  /// 当前主题id，如果有多个主题时，切换这个
  static String themeConfigId = kqPadThemeConfigId;

  /// 全局语言处理，默认只处理中文，其他语言请继承[KqPadBaseString]自行处理
  static KqPadBaseString Function()? globalString;
}
