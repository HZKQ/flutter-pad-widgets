import 'package:flutter/material.dart';

// 16进制设置颜色，前面两位是设置透明度，FF透明度是1
class KqPadThemeColors {
  static const Color bgTransparent = Color(0x00000000);

  /// 黑色透明度4%
  static const Color bgBlack04 = Color(0x0A000000);
  static const Color bgBlue = Color(0xFF3A559B);

  /// 相较于bgDefault暗色背景
  static const Color bgDark = Color(0xFFD6DDE5);

  //白色50%透明度
  static const Color bgWhite50 = Color(0x80FFFFFF);
  static const Color bgWhite = Color(0xFFFFFFFF);

  static const Color bgBlack5 = Color(0x0D000000);
  static const Color bgBlack50 = Color(0x80000000);
  static const Color bgBlack75 = Color(0xC0000000);
  static const Color bgBlack05 = Color(0x0D000000);
  static const Color bgBlack15 = Color(0x26000000);
  static const Color bgF0 = Color(0xFFF0F0F0);
  static const Color bgF4 = Color(0xFFF4F4F4);
  static const Color bgF7 = Color(0xFFF7F7F7);
  static const Color bgF9 = Color(0xFFF9F9F9);
  static const Color bgFA = Color(0xFFFAFAFA);
  static const Color bgD8 = Color(0xFFD8D8D8);
  static const Color bgD = Color(0xFFDDDDDD);
  static const Color bgEB = Color(0xFFEBEBEB);
  static const Color bgHeader = Color(0xFFD6D8E1);
  static const Color lineColor = Color(0x1A000000);
  static const Color lineColorLight = Color(0xFFF0F0F0);
  static const Color text333 = Color(0xFF333333);
  static const Color text666 = Color(0xFF666666);
  static const Color bgDialog = Color(0xFFDDE4EC);

  static const Color text222 = Color(0xFF222222);
  static const Color text26 = Color(0xFF262626);
  static const Color text59 = Color(0xFF595959);
  static const Color text8C = Color(0xFF8C8C8C);
  static const Color textBF = Color(0xFFBFBFBF);

  static const Color textBlack85 = Color(0xD9000000);
  static const Color textBlack65 = Color(0xA6000000);
  static const Color textBlack45 = Color(0x73000000);
  static const Color textBlack25 = Color(0x40000000);

  static const Color text999 = Color(0xFF999999);
  static const Color textCCC = Color(0xFFCCCCCC);
  static const Color textWhite = Color(0xFFFFFFFF);
  static const Color textWaterBlue5 = Color(0xFFDBEFFF);
  static const Color textLightBlue = Color(0xFF0A84FF);
  static const Color textLightBlue10 = Color(0x0A0A84FF);
  static const Color textBlue = Color(0xFF3A559B);
  static const Color textRed = Color(0xFFEC403B);
  static const Color textYellow = Color(0xFFFAAD14);
  static const Color textGreen = Color(0xFF73D13D);
  static const Color textBFB = Color(0xFFBFBFBF);

  static const Color textWork = Color(0xFF34C759);
  static const Color textIdle = Color(0xFFFF9F0A);
  static const Color textAlarm = Color(0xFFFF453A);
  static const Color textOffline = Color(0xFFB4BACA);

  static const Color bgDialogTitle = Color(0xffD6DDE5);
  static const Color radioUnCheckBgColor = Color(0xffEAEEF2);
  static const Color radioUnCheckBorderColor = Color(0xffCAD2DF);

  /// 新规范APP界面底色
  static const Color bgF5 = Color(0xFFF5F5F5);
  static const Color bgDefault = Color(0xFFDDE4EC);

  static const Color borderLine = Color(0xFFD9D9D9);

  /// 步骤条选中的颜色
  static const Color db46 = Color(0xFF46BDBE);

  /// 输入框内阴影颜色
  static const Color shadowColor1 = Color(0xFFB2B7CE);
  static const Color shadowColor2 = Color(0xFFC9CCD9);

  /// TabBar阴影
  static const Color shadowColor3 = Color(0xFFBDC1D1);
  static const Color shadowColor19 = Color(0x19BDC1D1);

  /// TabBar背景
  static const Color tabBarBgColor = Color(0xFFEBECF0);
  static const Color padLightBg = Color(0xFFEBECF0);

  static const Color bgHtGreen = Color(0xFF46BDBE); // 海天系统色

  static const Color textFF7E00 = Color(0xFFFF7E00);
  static const Color text7326 = Color(0x73262626);
}
