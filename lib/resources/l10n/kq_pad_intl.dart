import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';

import 'kq_pad_strings.dart';

///
/// 多语言支持
///
class KqPadIntl {
  /// 内置支持的语言和资源
  final Map<String, KqPadBaseString> _defaultResourceMap = {
    'zh': KqPadStringZh()
  };

  /// 缓存当前语言对应的资源，用于无 context 的情况
  static KqPadIntl? _current;

  static KqPadBaseString get currentResource {
    assert(
    _current != null,
    'No instance of kqIntl was loaded. \n'
        'Try to initialize the KqLocalizationDelegate before accessing kqIntl.currentResource.');

    /// 若应用未做本地化，则默认使用 zh-CN 资源
    _current ??= KqPadIntl(KqPadStringZh.locale);
    return _current!.localizedResource;
  }

  final Locale locale;

  KqPadIntl(this.locale);

  /// 获取当前语言下对应的资源，若为 null 则返回 [KqResourceZh]
  KqPadBaseString get localizedResource {
    // 支持动态资源文件
    KqPadBaseString? resource =
    _KqPadIntlHelper.findIntlResourceOfType<KqPadBaseString>(locale);
    if (resource != null) return resource;
    // 常规的多语言资源加载
    return KqPadGlobal.globalString?.call() ?? _defaultResourceMap[locale.languageCode] ??
        _defaultResourceMap['zh']!;
  }

  /// 获取[KqPadIntl]实例
  static KqPadIntl of(BuildContext context) {
    return Localizations.of(context, KqPadIntl);
  }

  /// 获取当前语言下 [KqPadBaseString] 资源
  static KqPadBaseString i10n(BuildContext context) {
    return KqPadIntl.of(context).localizedResource;
  }

  /// 应用加载本地化资源
  static Future<KqPadIntl> _load(Locale locale) {
    _current = KqPadIntl(locale);
    return SynchronousFuture<KqPadIntl>(_current!);
  }

  /// 支持非内置的本地化能力
  static void add(Locale locale, KqPadBaseString resource) {
    _KqPadIntlHelper.add(locale, resource);
  }

  /// 支持非内置的本地化能力
  static void addAll(Locale locale, List<KqPadBaseString> resources) {
    _KqPadIntlHelper.addAll(locale, resources);
  }
}

///
/// 组件多语言适配代理
///
class KqPadLocalizationDelegate extends LocalizationsDelegate<KqPadIntl> {
  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<KqPadIntl> load(Locale locale) {
    debugPrint('$runtimeType load: locale = $locale, ${locale.countryCode}, ${locale.languageCode}');
    return KqPadIntl._load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<KqPadIntl> old) => false;

  /// 需在app入口注册
  static KqPadLocalizationDelegate delegate = KqPadLocalizationDelegate();
}

///
/// 支持外部动态添加其他语言支的本地化
///
final Map<Locale, Map<Type, dynamic>> _additionalIntls = {};

class _KqPadIntlHelper {
  ///
  /// 根据 locale 查找 value 类型为[T]的资源
  ///
  static T? findIntlResourceOfType<T>(Locale locale) {
    Map<Type, dynamic>? res = _additionalIntls[locale];
    if (res != null && res.isNotEmpty) {
      for (var entry in res.entries) {
        if (entry.value is T) {
          return entry.value;
        }
      }
    }
    return null;
  }

  ///
  /// 根据 locale 查找Type为[T]的资源
  ///
  static T? findIntlResourceOfExactType<T>(Locale locale) {
    Map<Type, dynamic>? res = _additionalIntls[locale];
    if (res != null && res.isNotEmpty) {
      for (var entry in res.entries) {
        if (entry.key == T) {
          return entry.value;
        }
      }
    }
    return null;
  }

  ///
  /// 根据 locale 查找 value 类型为 T 的一系列资源
  ///
  static Iterable<T>? findIntlResourcesOfType<T>(Locale locale) {
    final res = _additionalIntls[locale];
    if (res != null && res.isNotEmpty) {
      List<T> resources = [];
      for (var entry in res.entries) {
        if (entry.value is T) {
          resources.add(entry.value);
        }
      }
      return resources;
    }
    return null;
  }

  ///
  /// 设置自定义 locale 的资源
  ///
  static void addAll(Locale locale, List<KqPadBaseString> resources) {
    var res = _additionalIntls[locale];
    if (res == null) {
      res = {};
      _additionalIntls[locale] = res;
    }
    for (KqPadBaseString resource in resources) {
      res[resource.runtimeType] = resource;
    }
  }

  ///
  /// 设置自定义 locale 的资源
  ///
  static void add(Locale locale, KqPadBaseString resource) {
    var res = _additionalIntls[locale];
    if (res == null) {
      res = {};
      _additionalIntls[locale] = res;
    }
    res[resource.runtimeType] = resource;
  }
}
