import 'dart:core';
import 'dart:ui';

/// 资源抽象类
abstract class KqPadBaseString {
  String get ok;

  String get cancel;

  String get confirm;

  String get loading;

  String get pleaseEnter;

  String get pleaseChoose;

  String get pleaseAdd;

  String get segment;

  String get minValueOfError;

  String get maxValueOfError;

  String get known;

  String get backPrevious;

  String get search;

  String get low;

  String get mid;

  String get high;

  String get imgUploadFail;

  String get choosePhoto;

  String get takePhoto;

  String get chooseFile;

  String get upperLowerTips;

  String get noDataTip;
}

///
/// 中文资源
///
class KqPadStringZh extends KqPadBaseString {
  static Locale locale = const Locale('zh', 'CN');

  @override
  String get ok => '确定';

  @override
  String get cancel => '取消';

  @override
  String get confirm => '确认';

  @override
  String get loading => '加载中...';

  @override
  String get pleaseEnter => '请输入';

  @override
  String get pleaseChoose => '请选择';

  @override
  String get pleaseAdd => '请添加';

  @override
  String get segment => '段';

  @override
  String get minValueOfError => '最小值不能大于最大值';

  @override
  String get maxValueOfError => '最大值不能小于最小值';

  @override
  String get known => '我知道了';

  @override
  String get backPrevious => '返回上一步';

  @override
  String get search => '搜索';

  @override
  String get low => '低';

  @override
  String get mid => '中';

  @override
  String get high => '高';

  @override
  String get imgUploadFail => '图片上传失败，请重试';

  @override
  String get choosePhoto => "相册选图";

  @override
  String get takePhoto => "相机拍照";

  @override
  String get chooseFile => "选择文件";

  @override
  String get upperLowerTips => "范围左侧必须小于等于右侧";

  @override
  String get noDataTip => "暂无数据";
}
