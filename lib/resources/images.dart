class Images {
  static String get commonIcClose16 => 'assets/images/common/ic_close_16.png';

  static String get commonIcCheckboxes16SelectedEnabledCircle =>
      'assets/images/common/ic_checkboxes_16_selected_enabled_circle.png';

  static String get commonIcRadio16SelectedEnabled =>
      'assets/images/common/ic_radio_16_selected_enabled.png';

  static String get commonIcRadio16UnselectedEnabled =>
      'assets/images/common/ic_radio_16_unselected_enabled.png';

  static String get commonIcCloseDialog16 =>
      'assets/images/common/ic_close_dialog_16.png';

  static String get commonIcMove20 => 'assets/images/common/ic_move_20.png';

  static String get commonIcRemoveFillDisabled =>
      'assets/images/common/ic_remove_fill_disabled.png';

  static String get commonIcCheckboxes16SelectedEnabledLine =>
      'assets/images/common/ic_checkboxes_16_selected_enabled_line.png';

  static String get commonIcHuakuai28 =>
      'assets/images/common/ic_huakuai_28.png';

  static String get commonIcQipao => 'assets/images/common/ic_qipao.png';

  static String get commonIcSearch16 => 'assets/images/common/ic_search_16.png';

  static String get commonIcBuzhou => 'assets/images/common/ic_buzhou.png';

  static String get commonIcJiantouRight16 =>
      'assets/images/common/ic_jiantou_right_16.png';

  static String get commonPicUpload88 =>
      'assets/images/common/pic_upload_88.png';

  static String get commonIcSanjiao => 'assets/images/common/ic_sanjiao.png';

  static String get commonIcCancel16Fill =>
      'assets/images/common/ic_cancel_16_fill.png';

  static String get commonIcArrowDown =>
      'assets/images/common/ic_arrow_down.png';

  static String get commonIcArrowDown0 =>
      'assets/images/common/ic_arrow_down_0.png';

  static String get commonIcJiantouStep =>
      'assets/images/common/ic_jiantou_step.png';

  static String get commonIcWendu => 'assets/images/common/ic_wendu.png';
}
