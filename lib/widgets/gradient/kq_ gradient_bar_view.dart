import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';

class KqGradientBar extends StatelessWidget {
  final List<Color> colors;
  final List<String> data;
  final double? width;
  final double? height;

  const KqGradientBar(
      {super.key,
      required this.colors,
      required this.data,
      this.width,
      this.height});

  @override
  Widget build(BuildContext context) {
    var stops = List<double>.generate(
        colors.length, (index) => index / (colors.length - 1));

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // 渐变条
        Container(
          width: width ?? 20.dm,
          height: height ?? 200.dm, // 高度也可以根据需要调整
          decoration: BoxDecoration(
            border: Border.all(color: KqPadThemeColors.text26, width: 0.5.dm),
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: stops,
              colors: colors,
            ),
          ),
        ),
        // 刻度列表
        SizedBox(
          height: height ?? 200.dm,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: data
                .map(
                  (value) => Row(
                    children: [
                      // 刻度
                      Container(
                        width: 4.dm, // 刻度宽度
                        height: 0.5.dm, // 刻度高度
                        color: KqPadThemeColors.text26,
                      ),
                      // 数值
                      Padding(
                        padding: EdgeInsets.only(left: 2.dm),
                        child: Text(
                          value,
                          style: TextStyle(
                            fontSize: 7.dm,
                            color: KqPadThemeColors.text26,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
                .toList(),
          ),
        ),
      ],
    );
  }
}
