import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';

import '../../resources/kq_pad_theme_colors.dart';

/// pad专用，筛选时的单选或多选按钮
///
/// 支持选中和取消选中
class KqPadFilterCheckView extends StatelessWidget {
  /// 是否选中
  final bool selected;

  /// 宽度
  final double? width;

  /// 高度
  final double? height;

  /// 圆角，默认100.dm
  final double? radius;

  /// 内边距
  final EdgeInsets? padding;

  /// 点击事件
  final VoidCallback onTap;

  /// 如果只需要显示文字，可以直接用[text]，否则需要自定义[child]
  final String? text;

  /// 子View
  final Widget? child;

  /// 是否显示阴影，默认显示
  final bool showShadow;

  /// 选中的背景色
  final Color? selectedColor;

  /// 未选中的背景色
  final Color? unSelectedColor;

  /// 选中的文本颜色
  final Color? selectedTextColor;

  /// 未选中的文本颜色
  final Color? unSelectedTextColor;

  const KqPadFilterCheckView(
      {super.key,
      required this.selected,
      required this.onTap,
      this.radius,
      this.width,
      this.height,
      this.padding,
      this.text,
      this.child,
      this.showShadow = true,
      this.selectedColor,
      this.unSelectedColor,
      this.selectedTextColor,
      this.unSelectedTextColor});

  @override
  Widget build(BuildContext context) {
    return KqInkWell(
      enableRipple: false,
      onTap: onTap,
      child: Container(
        width: width,
        height: height,
        padding: padding ?? EdgeInsets.symmetric(horizontal: 5.dm),
        decoration: KqBoxDecoration(
          gradient: selected
              ? null
              : showShadow
                  ? const LinearGradient(
                      colors: [Color(0xFFE6E7ED), Color(0xFFF7F8FA)],
                      stops: [0, 1],
                    )
                  : null,
          color: selected
              ? selectedColor ?? KqPadThemeColors.bgHtGreen
              : unSelectedColor,
          borderRadius: BorderRadius.circular(radius ?? 100.dm),
          boxShadow: showShadow
              ? [
                  KqBoxShadow(
                    color: const Color(0xFFB1B5C6),
                    offset: Offset(1.dm, 1.dm),
                    blurRadius: 3.dm,
                    spreadRadius: 0,
                  ),
                  KqBoxShadow(
                    color: const Color(0xFFFAFAFC),
                    offset: Offset(-1.dm, -1.dm),
                    blurRadius: 4.dm,
                    spreadRadius: 0,
                  ),
                ]
              : null,
        ),
        child: child ??
            Align(
              alignment: Alignment.center,
              child: Text(
                text ?? '',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 7.dm,
                    color: selected
                        ? selectedTextColor ?? KqPadThemeColors.textWhite
                        : unSelectedTextColor ?? KqPadThemeColors.text26),
              ),
            ),
      ),
    );
  }
}
