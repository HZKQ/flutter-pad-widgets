import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/native/file_picker_native_util.dart';
import 'package:kq_flutter_pad_widgets/widgets/dialog/kq_pad_dialog_util.dart';
import '../../resources/l10n/kq_pad_intl.dart';

/// 文件选择器
class KqPadFilePicker {
  int maxCount;
  int? compress;

  /// 构建文件选择器
  /// [compress]指定图片压缩后的大小，[compress]为null或[compress]<=0时不压缩，图片本身大小 <= [compress]时也不压缩，
  /// 如果选择的文件非图片也不压缩，单位KB
  KqPadFilePicker({this.maxCount = 1, this.compress = 2048});

  /// 选择图片
  void choosePhoto(
      BuildContext context, Function(List<KqFile> files)? callBack) {
    KqPadDialog.showListDialog([
      KqPadIntl.currentResource.takePhoto,
      KqPadIntl.currentResource.choosePhoto,
    ], callback: ((index, item) async {
      if (index == 1) {
        //相册
        var files = await FilePickerNativeUtil.pickImages(
            maxCount: maxCount, compress: compress);
        if (callBack != null) {
          callBack(files);
        }
      } else {
        //拍照
        var file = await FilePickerNativeUtil.takePhoto(compress: compress);
        if (callBack != null) {
          callBack(file != null ? [file] : []);
        }
      }
    }));
  }

  /// 选择文件
  ///
  /// [extensions]为文件后缀，指定哪些类型文件可选，不传则表示所有文件可选。空列表则不能选择任何文件
  void chooseFile(BuildContext context,
      {List<String>? extensions, Function(List<KqFile> file)? callBack}) {
    KqPadDialog.showListDialog([
      KqPadIntl.currentResource.takePhoto,
      KqPadIntl.currentResource.chooseFile
    ], callback: ((index, item) async {
      if (index == 1) {
        var files = await FilePickerNativeUtil.pickFiles(
            maxCount: maxCount, extensions: extensions, compress: compress);
        if (callBack != null) {
          callBack(files);
        }
      } else {
        // 拍照
        var file = await FilePickerNativeUtil.takePhoto(compress: compress);
        if (callBack != null) {
          callBack(file != null ? [file] : []);
        }
      }
    }));
  }
}
