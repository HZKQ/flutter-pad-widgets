import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:get/get.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';

import '../../resources/kq_pad_theme_colors.dart';
import '../../resources/l10n/kq_pad_intl.dart';
import '../divider/kq_pad_divider.dart';

/// 通用确认对话框
///
/// @author 周卓
///
/// 顶部标题 + 中间消息 + 底部2个按钮
class KqPadConfirmDialog extends AlertDialog {
  /// 标题
  final String? titleString;

  /// 消息
  final String msg;

  /// 左边按钮文字，默认"取消"
  final String? leftBtnText;

  /// 左边按钮文字，默认"确定"
  final String? rightBtnText;

  /// 左边按钮回调
  final Function()? onLeftBtnTap;

  /// 右边按钮回调
  final Function()? onRightBtnTap;

  /// 标题文字颜色
  final Color? titleColor;

  /// 消息文字颜色
  final Color? msgColor;

  /// 左边按钮文字颜色
  final Color? leftBtnColor;

  /// 右边按钮文字颜色
  final Color? rightBtnColor;

  /// 标题文字大小
  final double? titleFontSize;

  /// 消息文字大小
  final double? msgFontSize;

  /// 按钮文字
  final double? btnFontSize;

  const KqPadConfirmDialog(
      {Key? key,
      this.titleString,
      required this.msg,
      this.leftBtnText,
      this.rightBtnText,
      this.titleColor,
      this.leftBtnColor,
      this.rightBtnColor,
      this.msgColor,
      this.titleFontSize,
      this.msgFontSize,
      this.btnFontSize,
      this.onLeftBtnTap,
      this.onRightBtnTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = <Widget>[];

    if (titleString != null) {
      children.add(Container(
        margin: EdgeInsets.only(top: 8.dm),
        padding: EdgeInsets.symmetric(horizontal: 15.dm),
        alignment: Alignment.center,
        child: Text(
          titleString!,
          style: TextStyle(
              fontSize: 8.dm,
              color: KqPadThemeColors.text26,
              fontWeight: FontWeight.bold),
        ),
      ));
      children.add(SizedBox(
        width: 1.dm,
        height: 6.dm,
      ));
      children.add(const KqPadDivider(
        showShadow: true,
      ));
      children.add(SizedBox(
        width: 1.dm,
        height: 7.dm,
      ));
    } else {
      /// 上边距
      children.add(SizedBox(
        width: 1.dm,
        height: 8.dm,
      ));
    }

    /// 消息
    children.add(LimitedBox(
      maxHeight: 300.dm,
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15.dm),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              msg,
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: 8.dm, color: KqPadThemeColors.text59),
            ),
          ),
        ),
      ),
    ));

    /// 下边距
    children.add(SizedBox(
      width: 1.dm,
      height: 17.dm,
    ));

    /// 底部按钮
    children.add(const KqPadDivider(
      showShadow: true,
    ));

    children.add(Row(
      children: [
        Expanded(
            child: KqInkWell(
          onTap: () {
            if (onLeftBtnTap != null) {
              onLeftBtnTap!();
            }
          },
          child: Container(
            alignment: Alignment.center,
            width: double.infinity,
            height: 22.dm,
            child: Text(
              leftBtnText ?? KqPadIntl.currentResource.cancel,
              style: TextStyle(fontSize: 8.dm, color: KqPadThemeColors.text8C),
            ),
          ),
        )),
        KqPadVerticalDivider(
          height: 22.dm,
          showShadow: true,
        ),
        Expanded(
            child: KqInkWell(
          onTap: () {
            if (onRightBtnTap != null) {
              onRightBtnTap!();
            }
          },
          child: Container(
            alignment: Alignment.center,
            width: double.infinity,
            height: 22.dm,
            child: Text(
              rightBtnText ?? KqPadIntl.currentResource.confirm,
              style:
                  TextStyle(fontSize: 8.dm, color: KqPadThemeColors.bgHtGreen),
            ),
          ),
        ))
      ],
    ));

    Widget dialogChild = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children,
    );
    return UnconstrainedBox(
        child: SizedBox(
            width: 160.dm,
            child: Material(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4.dm))),
                color: KqPadThemeColors.padLightBg,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(4.dm)),
                  child: dialogChild,
                ))));
  }
}
