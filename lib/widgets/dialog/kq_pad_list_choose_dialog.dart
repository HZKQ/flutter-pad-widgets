import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/widgets/divider/kq_pad_divider.dart';
import 'package:kq_flutter_pad_widgets/widgets/searchBar/kq_pad_search_bar.dart';
import 'package:kq_flutter_pad_widgets/widgets/sectionList/kq_pad_section_grid_view.dart';

class KqPadListChooseDialog<T> extends StatefulWidget {
  final String title;
  final Widget Function(int index, T item)? itemBuilder;
  final String Function(int index, T item)? itemContent;
  final bool Function(int index, T item) isSelected;
  final void Function(int index, T item) selectedItem;
  final List<T> Function() itemList;
  final List<T> Function(String content)? onSearch;
  final List<T> Function(String content)? onSubmit;
  const KqPadListChooseDialog(
      {required this.title,
      required this.itemList,
      required this.selectedItem,
      required this.isSelected,
      this.itemBuilder,
      this.itemContent,
      this.onSearch,
      this.onSubmit,
      super.key});

  @override
  State<KqPadListChooseDialog> createState() =>
      _KqPadListChooseDialogState<T>();
}

class _KqPadListChooseDialogState<T> extends State<KqPadListChooseDialog<T>> {
  final TextEditingController editingController = TextEditingController();

  late List<T> dataSource;

  @override
  void initState() {
    super.initState();
    dataSource = widget.itemList();
  }

  @override
  Widget build(BuildContext context) {
    // var padding = MediaQuery.of(context).viewInsets.bottom;
    return UnconstrainedBox(
      child: SizedBox(
          width: 486.dm,
          child: Material(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.dm))),
              color: KqPadThemeColors.padLightBg,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _renderHeader(),
                  const KqPadDivider(
                    showShadow: true,
                  ),
                  _renderSearch(),
                  _readerList()
                ],
              ))),
    );
  }

  Widget _renderHeader() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 13.dm, vertical: 8.dm),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 12.dm,
            height: 12.dm,
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(
                height: 2.dm,
              ),
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: 8.dm,
                    fontWeight: FontWeight.bold,
                    color: KqPadThemeColors.text26),
                textAlign: TextAlign.center,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )
            ],
          )),
          KqInkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            verticalPadding: 4.dm,
            horizontalPadding: 4.dm,
            radius: 100.dm,
            child: Image.asset(
              Images.commonIcCloseDialog16,
              width: 12.dm,
              height: 12.dm,
              fit: BoxFit.fill,
              package: KqPadGlobal.packageName,
            ),
          )
        ],
      ),
    );
  }

  Widget _renderSearch() {
    return widget.onSearch != null || widget.onSubmit != null
        ? Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 12.dm,
              vertical: 6.dm,
            ),
            child: KqPadSearchBar(
              cancelBtnEnable: false,
              editController: editingController,
              onChanged: (value) {
                if (widget.onSearch != null) {
                  setState(() {
                    dataSource = widget.onSearch!(value);
                  });
                }
              },
              onSubmit: (value, state) {
                if (widget.onSubmit != null) {
                  setState(() {
                    dataSource = widget.onSubmit!(value);
                  });
                }
              },
            ),
          )
        : Container();
  }

  Widget _readerList() {
    return Padding(
      padding: EdgeInsets.only(left: 12.dm, right: 12.dm, bottom: 6.dm),
      child: SizedBox(
        height: 278.dm,
        child: KqPadSectionGridView(
          section: 1,
          columns: 2,
          itemList: (section) {
            return dataSource;
          },
          itemBuilder: (section, row, object) {
            bool selected = widget.isSelected(row, object);
            Widget? cell = widget.itemBuilder?.call(row, object);
            if (cell == null) {
              String? content = widget.itemContent?.call(row, object);
              cell = Padding(
                padding: EdgeInsets.all(8.dm),
                child: Text(
                  content ?? '',
                  style: TextStyle(
                      fontSize: 8.dm,
                      color: selected
                          ? KqPadThemeColors.textWhite
                          : KqPadThemeColors.text26),
                ),
              );
            }
            return KqInkWell(
                onTap: () {
                  widget.selectedItem(row, object);
                  Navigator.of(context).pop();
                },
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: 6.dm,
                      left: row % 2 != 0 ? 3.dm : 0,
                      right: row % 2 == 0 ? 3.dm : 0),
                  child: Container(
                    decoration: KqBoxDecoration(
                        // borderRadius: BorderRadius.circular(2.dm),
                        color: selected
                            ? KqPadThemeColors.db46
                            : KqPadThemeColors.bgF7,
                        boxShadow: [
                          KqBoxShadow(
                            color: const Color(0xFFB1B5C6),
                            offset: Offset(1.dm, 1.dm),
                            blurRadius: 3.dm,
                            spreadRadius: 0,
                          ),
                          KqBoxShadow(
                            color: const Color(0xFFFAFAFC),
                            offset: Offset(-1.dm, -1.dm),
                            blurRadius: 4.dm,
                            spreadRadius: 0,
                          ),
                        ]),
                    child: cell,
                  ),
                ));
          },
        ),
      ),
    );
  }
}
