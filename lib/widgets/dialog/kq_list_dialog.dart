import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';

import '../../resources/kq_pad_theme_colors.dart';
import '../divider/kq_pad_divider.dart';
import '../searchBar/kq_pad_search_bar.dart';

/// 通用列表对话框
///
/// @author 周卓
///
class KqPadListDialog<T> extends AlertDialog {
  /// 数据源
  final List<T> items;

  /// 转换 T -> String 转换
  final String Function(int index, T item)? nameConvert;

  /// 回调
  final Function(int index, T item)? callback;

  /// 是否显示搜索，[searchEnable]=true时，需要配合[onSearchItem]使用，默认false
  final bool searchEnable;

  /// 搜索内容匹配方法，仅[searchEnable]=true时生效
  final bool Function(String searchText, T item)? onSearchItem;

  const KqPadListDialog(
      {Key? key,
      required this.items,
      this.searchEnable = false,
      this.onSearchItem,
      this.nameConvert,
      this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var padding = MediaQuery.of(context).viewInsets.bottom;
    return UnconstrainedBox(
        child: Padding(
      padding: EdgeInsets.only(bottom: padding),
      child: SizedBox(
          width: 240.dm,
          child: Material(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.dm))),
              color: KqPadThemeColors.padLightBg,
              child: KqPadSearchListView(
                items: items,
                searchEnable: searchEnable,
                onSearchItem: onSearchItem,
                nameConvert: nameConvert,
                callback: callback,
              ))),
    ));
  }
}

/// 搜索列表
class KqPadSearchListView<T> extends StatefulWidget {
  /// 数据源
  final List<T> items;

  /// 是否显示搜索，默认不显示
  final bool searchEnable;

  /// 转换 T -> String 转换
  final String Function(int index, T item)? nameConvert;

  /// 搜索内容匹配方法，仅[searchEnable]=true时生效
  final bool Function(String searchText, T item)? onSearchItem;

  /// 回调
  final Function(int index, T item)? callback;

  const KqPadSearchListView(
      {Key? key,
      required this.items,
      this.onSearchItem,
      this.searchEnable = false,
      this.nameConvert,
      this.callback})
      : super(key: key);

  @override
  KqPadSearchListViewState createState() => KqPadSearchListViewState<T>();
}

class KqPadSearchListViewState<T> extends State<KqPadSearchListView<T>> {
  /// 搜索内容
  var _searchText = "";

  List<T> _allItems = [];

  List<T> _showItems = [];

  @override
  void initState() {
    _allItems = widget.items;
    _showItems = [..._allItems];
    super.initState();
  }

  @override
  void didUpdateWidget(covariant KqPadSearchListView<T> oldWidget) {
    if (oldWidget.items != widget.items) {
      _allItems = widget.items;
      searchData();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    // 小诸葛规范，最多显示8个，多余滚动
    var maxHeight = 8 * 24.dm;

    final List<Widget> children = <Widget>[];

    /// item
    for (int i = 0; i < _showItems.length; i++) {
      var item = _showItems[i];
      String title;
      if (widget.nameConvert != null) {
        title = widget.nameConvert!(i, item);
      } else {
        if (item is String) {
          title = item;
        } else if (item is Map) {
          title = item['title'];
        } else {
          title = '';
        }
      }
      children.add(KqInkWell(
        onTap: () => {
          if (widget.callback != null) {widget.callback!(i, item)}
        },
        child: Container(
          height: 24.dm,
          alignment: Alignment.center,
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10.dm),
          child: Text(
            title,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontSize: 8.dm,
                color: KqPadThemeColors.text26),
          ),
        ),
      ));
      if (i < (_showItems.length - 1)) {
        children.add(const KqPadDivider());
      }
    }

    Widget dialogChild = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children,
    );
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(4.dm)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (widget.searchEnable)
            KqPadSearchBar(
              newValue: _searchText,
              onChanged: (value) {
                _searchText = value;
                searchData();
              },
              onCancel: () {
                _searchText = "";
                searchData();
              },
              onCleared: () {
                _searchText = "";
                searchData();
              },
            ),
          LimitedBox(
            maxHeight: maxHeight,
            child: SingleChildScrollView(
              child: dialogChild,
            ),
          ),
        ],
      ),
    );
  }

  void searchData() {
    setState(() {
      if (_searchText.isEmpty) {
        _showItems = [..._allItems];
      } else {
        _showItems.clear();
        for (T item in _allItems) {
          bool showItem = widget.onSearchItem?.call(_searchText, item) ?? true;
          if (showItem) {
            _showItems.add(item);
          }
        }
      }
    });
  }
}
