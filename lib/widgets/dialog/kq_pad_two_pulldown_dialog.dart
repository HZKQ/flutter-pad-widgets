import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/config/kq_core_global.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/widgets/dialog/kq_custom_dialog.dart';
import 'package:kq_flutter_pad_widgets/widgets/dialog/kq_pad_dialog_util.dart';

class KqPadTwoPullDownDialog extends StatefulWidget {
  final String dialogTitle;
  final String? firstTitle;
  final String? secondTitle;
  final String firstPlaceholder;
  final String secondPlaceholder;
  final List firstDataSource;
  final List secondDataSource;
  final String Function(int, dynamic) firstNameRender;
  final String Function(int, dynamic) secondNameRender;
  final String Function(dynamic) firstSelectedCallback;
  final String Function(dynamic) secondSelectedCallback;
  final void Function()? onRightBtnTap;
  final void Function()? onLeftBtnTap;
  const KqPadTwoPullDownDialog(
      {super.key,
      required this.firstDataSource,
      required this.secondDataSource,
      required this.firstPlaceholder,
      required this.secondPlaceholder,
      required this.firstNameRender,
      required this.secondNameRender,
      required this.firstSelectedCallback,
      required this.secondSelectedCallback,
      this.onRightBtnTap,
      this.onLeftBtnTap,
      required this.dialogTitle,
      this.firstTitle,
      this.secondTitle});

  static show(
      {required List firstDataSource,
      required List secondDataSource,
      required String firstPlaceholder,
      required String secondPlaceholder,
      required String Function(int, dynamic) firstNameRender,
      required String Function(int, dynamic) secondNameRender,
      required String Function(dynamic) firstSelectedCallback,
      required String Function(dynamic) secondSelectedCallback,
      required String dialogTitle,
      void Function()? onRightBtnTap,
      void Function()? onLeftBtnTap,
      String? firstTitle,
      String? secondTitle,
      Function()? onDismiss}) {
    showDialog(
        context: KqCoreGlobal.globalContext,
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.2),
        builder: (context) {
          return KqPadTwoPullDownDialog(
            firstDataSource: firstDataSource,
            secondDataSource: secondDataSource,
            firstPlaceholder: firstPlaceholder,
            secondPlaceholder: secondPlaceholder,
            firstNameRender: firstNameRender,
            secondNameRender: secondNameRender,
            firstSelectedCallback: firstSelectedCallback,
            secondSelectedCallback: secondSelectedCallback,
            dialogTitle: dialogTitle,
            firstTitle: firstTitle,
            secondTitle: secondTitle,
          );
        }).then((value) => onDismiss?.call());
  }

  @override
  State<KqPadTwoPullDownDialog> createState() => _KqPadTwoPullDownDialogState();
}

class _KqPadTwoPullDownDialogState extends State<KqPadTwoPullDownDialog> {
  String? firstResName;
  String? secondResName;

  @override
  Widget build(BuildContext context) {
    return KqPadCustomDialog(
      titleString: widget.dialogTitle,
      dialogWidth: 240.dm,
      customBottomPadding: 0,
      customWidget: Padding(
        padding: EdgeInsets.all(16.dm),
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          renderFirstPulldown(),
          SizedBox(
            height: 8.dm,
          ),
          renderSecondPulldown()
        ]),
      ),
      onRightBtnTap: () {
        Navigator.pop(context);
        widget.onRightBtnTap?.call();
      },
      onLeftBtnTap: () {
        Navigator.pop(context);
        widget.onLeftBtnTap?.call();
      },
    );
  }

  Widget renderFirstPulldown() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        if (widget.firstTitle != null && widget.firstTitle!.isNotEmpty)
          Text(
            widget.firstTitle!,
            style: TextStyle(fontSize: 7.dm, color: KqPadThemeColors.text26),
          ),
        SizedBox(
          width: 10.dm,
        ),
        Expanded(
            child: Container(
          padding: EdgeInsets.symmetric(horizontal: 6.dm),
          constraints: BoxConstraints(minHeight: 20.dm),
          decoration: KqBoxDecoration(
            boxShadow: [
              KqBoxShadow(
                  offset: Offset(0.5.dm, 0.5.dm),
                  blurRadius: 0,
                  spreadRadius: 0,
                  color: const Color(0x80FFFFFF)),
              KqBoxShadow(
                  offset: Offset(0, 0.5.dm),
                  blurRadius: 3.dm,
                  inset: true,
                  spreadRadius: 0,
                  color: const Color(0xFFB2B7CE)),
              KqBoxShadow(
                  offset: Offset(0.5.dm, 0.5.dm),
                  blurRadius: 0.5.dm,
                  inset: true,
                  spreadRadius: 0,
                  color: const Color(0xFFC9CCD9)),
            ],
            borderRadius: BorderRadius.circular(1.dm),
            color: KqPadThemeColors.bgWhite,
          ),
          child: InkWell(
            onTap: () {
              /// 下拉展开
              KqPadDialog.showListDialog(
                widget.firstDataSource,
                nameConvert: widget.firstNameRender,
                callback: (index, item) {
                  String name = widget.firstSelectedCallback(item);
                  setState(() {
                    firstResName = name;
                  });
                },
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  firstResName ?? widget.firstPlaceholder,
                  style: TextStyle(
                      fontSize: 7.dm,
                      color: firstResName != null
                          ? KqPadThemeColors.text26
                          : KqPadThemeColors.textBFB),
                ),
                Image.asset(
                  // Images.commonIcJiantouRight16,
                  Images.commonIcArrowDown0,
                  width: 7.dm,
                  fit: BoxFit.fitHeight,
                  package: KqPadGlobal.packageName,
                )
              ],
            ),
          ),
        ))
      ],
    );
  }

  Widget renderSecondPulldown() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        if (widget.secondTitle != null && widget.secondTitle!.isNotEmpty)
          Text(
            widget.secondTitle!,
            style: TextStyle(fontSize: 7.dm, color: KqPadThemeColors.text26),
          ),
        SizedBox(
          width: 10.dm,
        ),
        Expanded(
            child: Container(
          padding: EdgeInsets.symmetric(horizontal: 6.dm),
          constraints: BoxConstraints(minHeight: 20.dm),
          decoration: KqBoxDecoration(
            boxShadow: [
              KqBoxShadow(
                  offset: Offset(0.5.dm, 0.5.dm),
                  blurRadius: 0,
                  spreadRadius: 0,
                  color: const Color(0x80FFFFFF)),
              KqBoxShadow(
                  offset: Offset(0, 0.5.dm),
                  blurRadius: 3.dm,
                  inset: true,
                  spreadRadius: 0,
                  color: const Color(0xFFB2B7CE)),
              KqBoxShadow(
                  offset: Offset(0.5.dm, 0.5.dm),
                  blurRadius: 0.5.dm,
                  inset: true,
                  spreadRadius: 0,
                  color: const Color(0xFFC9CCD9)),
            ],
            borderRadius: BorderRadius.circular(1.dm),
            color: KqPadThemeColors.bgWhite,
          ),
          child: InkWell(
            onTap: () {
              /// 下拉展开
              KqPadDialog.showListDialog(
                widget.secondDataSource,
                nameConvert: widget.secondNameRender,
                callback: (index, item) {
                  String name = widget.secondSelectedCallback(item);
                  setState(() {
                    secondResName = name;
                  });
                },
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  secondResName ?? widget.secondPlaceholder,
                  style: TextStyle(
                      fontSize: 7.dm,
                      color: secondResName != null
                          ? KqPadThemeColors.text26
                          : KqPadThemeColors.textBFB),
                ),
                Image.asset(
                  // Images.commonIcJiantouRight16,
                  Images.commonIcArrowDown0,
                  width: 7.dm,
                  fit: BoxFit.fitHeight,
                  package: KqPadGlobal.packageName,
                )
              ],
            ),
          ),
        ))
      ],
    );
  }
}
