import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/theme/kq_theme_manager.dart';

import '../../resources/images.dart';
import '../../resources/kq_pad_theme_colors.dart';
import '../../resources/l10n/kq_pad_intl.dart';
import '../divider/kq_pad_divider.dart';

/// 操作类对话框
///
/// @author 周卓
///
/// 顶部标题和关闭按钮 + 中间自定义 + 底部1个蓝色按钮
class KqPadOptDialog extends AlertDialog {
  /// 标题
  final String? titleString;

  /// 自定义控件
  final Widget child;

  /// 按钮文字，默认"确定"
  final String? btnText;

  /// 自定义对话框宽度，默认210.r
  final double? width;

  /// 自定义对话框高度，默认自适应
  final double? height;

  /// 按钮回调
  final Function()? onBtnTap;

  const KqPadOptDialog(
      {Key? key,
      required this.child,
      this.titleString,
      this.width,
      this.height,
      this.btnText,
      this.onBtnTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = <Widget>[];

    /// 标题
    children.add(Stack(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24.dm, vertical: 8.dm),
          alignment: Alignment.center,
          child: Text(
            titleString ?? '',
            style: TextStyle(
                fontSize: 10.dm,
                color: KqPadThemeColors.text26,
                fontWeight: FontWeight.bold),
          ),
        ),
        Positioned(
          right: 6.dm,
          top: 6.dm,
          height: 16.dm,
          width: 16.dm,
          child: KqInkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            verticalPadding: 4.dm,
            horizontalPadding: 4.dm,
            radius: 100.dm,
            child: Image.asset(
              Images.commonIcCloseDialog16,
              width: 12.dm,
              height: 12.dm,
              fit: BoxFit.fill,
              package: KqPadGlobal.packageName,
            ),
          ),
        )
      ],
    ));

    children.add(const KqPadDivider(
      showShadow: true,
    ));

    /// 消息
    children.add(child);

    /// 底部按钮
    if (onBtnTap != null) {
      /// 下边距
      children.add(SizedBox(
        width: 1.dm,
        height: 8.dm,
      ));
      children.add(Row(
        children: [
          Expanded(
              child: Align(
            child: SizedBox(
              width: 100.dm,
              child: KqInkWell(
                onTap: () {
                  if (onBtnTap != null) {
                    onBtnTap!();
                  }
                },
                radius: 1.dm,
                backgroundColor: KqPadThemeColors.bgHtGreen,
                child: Container(
                  alignment: Alignment.center,
                  height: 20.dm,
                  child: Text(
                    btnText ?? KqPadIntl.currentResource.confirm,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: 8.dm, color: KqPadThemeColors.textWhite),
                  ),
                ),
              ),
            ),
          ))
        ],
      ));

      /// 下边距
      children.add(SizedBox(
        width: 1.dm,
        height: 8.dm,
      ));
    }

    Widget dialogChild = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children,
    );
    var padding = MediaQuery.of(context).viewInsets.bottom;
    return UnconstrainedBox(
        child: Container(
            padding: EdgeInsets.only(bottom: padding / 2),
            width: width ?? 210.dm,
            height: height,
            child: Material(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4.dm))),
                color: KqPadThemeColors.padLightBg,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(4.dm)),
                  child: dialogChild,
                ))));
  }
}
