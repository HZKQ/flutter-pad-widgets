import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:get/get.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';
import '../../resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_core_widget/utils/text_filed_utils.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';

import '../../resources/l10n/kq_pad_intl.dart';
import '../../theme/kq_theme_manager.dart';
import '../divider/kq_pad_divider.dart';
import '../textFiled/kq_pad_textfiled.dart';

/// 通用输入对话框
///
/// @author 周卓
///
class KqPadInputDialog extends AlertDialog {
  /// 标题
  final String? titleString;

  /// 默认值
  final String? defValue;

  /// 提示文字，默认"请输入"
  final String? placeHolder;

  /// 当行模式点击键盘换行键是否自动提交
  final bool autoSubmitWhenReturn;

  /// 是否多行，默认false
  final bool multiline;

  /// 最大行数，不传不限制
  final int? maxLines;

  /// 左边按钮文字，默认"取消"
  final String? leftBtnText;

  /// 中间按钮文字，默认null,不传则不显示
  final String? middleBtnText;

  /// 左边按钮文字，默认"确定"
  final String? rightBtnText;

  /// 输入类型，默认[TextInputType.text]
  final TextInputType? inputType;

  /// 输入内容限制
  final List<TextInputFormatter>? inputFormatters;

  /// 输入框文字对齐方式
  final TextAlign? inputTextAlign;

  /// 是否显示清空按钮
  final bool enableClearButton;

  /// 输入回调
  final Function(String value)? onValueChanged;

  /// 左边按钮回调
  final Function(String value)? onLeftBtnTap;

  /// 中间按钮回调
  final Function(String value)? onMiddleBtnTap;

  /// 右边按钮回调
  final Function(String value)? onRightBtnTap;

  /// 输入框内容控制器
  final TextEditingController editController;

  /// 最大字符数，默认不限制
  final int? maxCharCount;

  /// 是否显示阴影，默认true
  final bool showShadow;

  /// 用于自定义键盘顶起来的高度
  final double Function(double bottomPadding)? bottomPaddingBuilder;

  KqPadInputDialog({
    Key? key,
    this.defValue,
    this.titleString,
    this.multiline = false,
    this.autoSubmitWhenReturn = false,
    this.maxLines,
    this.inputFormatters,
    this.placeHolder,
    this.inputTextAlign,
    this.inputType,
    this.leftBtnText,
    this.middleBtnText,
    this.rightBtnText,
    this.enableClearButton = false,
    this.onValueChanged,
    this.onLeftBtnTap,
    required this.editController,
    this.onMiddleBtnTap,
    this.onRightBtnTap,
    this.maxCharCount,
    this.showShadow = true,
    this.bottomPaddingBuilder,
  }) : super(key: key) {
    if (defValue != null) {
      TextFiledUtil.setValue(editController, defValue!);
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isLangScape = context.orientation == Orientation.landscape;

    final List<Widget> children = <Widget>[];

    if (titleString != null) {
      children.add(Container(
        margin: EdgeInsets.symmetric(vertical: 6.dm),
        padding: EdgeInsets.symmetric(horizontal: 15.dm),
        alignment: Alignment.center,
        child: Text(
          titleString!,
          style: TextStyle(
              fontSize: 8.dm,
              color: KqPadThemeColors.text26,
              fontWeight: FontWeight.bold),
        ),
      ));
      children.add(const KqPadDivider(
        showShadow: true,
      ));
      children.add(SizedBox(
        height: 12.dm,
      ));
    } else {
      /// 上边距
      children.add(SizedBox(
        height: 18.dm,
      ));
    }

    /// 输入框
    children.add(Container(
      height: 22.dm,
      margin: EdgeInsets.symmetric(horizontal: 12.dm),
      padding: EdgeInsets.symmetric(horizontal: 7.dm),
      alignment: Alignment.centerLeft,
      decoration: KqBoxDecoration(
        borderRadius: BorderRadius.circular(1.dm),
        color: KqPadThemeColors.bgWhite,
        boxShadow: showShadow
            ? [
                KqBoxShadow(
                    offset: Offset(0.5.dm, 0.5.dm),
                    blurRadius: 0,
                    spreadRadius: 0,
                    color: const Color(0x80FFFFFF)),
                KqBoxShadow(
                    offset: Offset(0, 0.5.dm),
                    blurRadius: 3.dm,
                    spreadRadius: 0,
                    inset: true,
                    color: const Color(0xFFB2B7CE)),
                KqBoxShadow(
                    offset: Offset(0.5.dm, 0.5.dm),
                    blurRadius: 0.5.dm,
                    spreadRadius: 0,
                    inset: true,
                    color: const Color(0xFFC9CCD9)),
              ]
            : null,
      ),
      child: KqPadTextFiled(
        fontSize: 7.dm,
        multiline: multiline,
        useParentHeight: false,
        placeHolder: placeHolder,
        autofocus: true,
        showShadow: false,
        controller: editController,
        maxLines: maxLines,
        maxCharCount: maxCharCount,
        inputFormatters: inputFormatters,
        enableClearButton: enableClearButton,
        onChanged: (value) {
          // editController.text = value;
          if (onValueChanged != null) {
            onValueChanged!(value);
          }
        },
        onSubmitted: (value) {
          if (onRightBtnTap != null && autoSubmitWhenReturn) {
            onRightBtnTap!(value);
          }
        },
        defValue: defValue,
        inputType: inputType,
        textInputAction:
            multiline ? TextInputAction.newline : TextInputAction.done,
        textAlign: inputTextAlign ?? TextAlign.start,
      ),
    ));

    /// 下边距
    children.add(SizedBox(
      width: 1.dm,
      height: 12.dm,
    ));

    children.add(const KqPadDivider(
      showShadow: true,
    ));

    /// 按钮
    children.add(Row(
      children: [
        Expanded(
            child: KqInkWell(
          onTap: () {
            if (onLeftBtnTap != null) {
              onLeftBtnTap!(editController.text);
            }
          },
          child: Container(
            alignment: Alignment.center,
            width: double.infinity,
            height: 22.dm,
            child: Text(
              leftBtnText ?? KqPadIntl.currentResource.cancel,
              style: TextStyle(fontSize: 8.dm, color: KqPadThemeColors.text8C),
            ),
          ),
        )),
        KqPadVerticalDivider(
          height: 22.dm,
          color: const Color(0xFFD6D8E1),
        ),
        Expanded(
            child: KqInkWell(
          onTap: () {
            if (onRightBtnTap != null) {
              onRightBtnTap!(editController.text);
            }
          },
          child: Container(
            alignment: Alignment.center,
            width: double.infinity,
            height: 22.dm,
            child: Text(
              rightBtnText ?? KqPadIntl.currentResource.confirm,
              style: TextStyle(
                  fontSize: 8.dm, color: KqPadThemeColors.bgHtGreen),
            ),
          ),
        ))
      ],
    ));

    Widget dialogChild = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children,
    );
    var padding = MediaQuery.of(context).viewInsets.bottom;
    return UnconstrainedBox(
        child: Container(
            padding: EdgeInsets.only(bottom: bottomPaddingBuilder?.call(padding) ?? padding / 2),
            width: 160.dm,
            child: Material(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4.dm))),
                color: KqPadThemeColors.padLightBg,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(4.dm)),
                  child: dialogChild,
                ))));
  }
}
