import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import '../../config/kq_pad_global.dart';
import '../../resources/images.dart';
import '../../resources/kq_pad_theme_colors.dart';
import '../../resources/l10n/kq_pad_intl.dart';
import '../divider/kq_pad_divider.dart';

/// 自定义弹窗
class KqPadCustomDialog extends AlertDialog {
  /// 标题
  final String? titleString;

  final Widget customWidget;

  /// 按钮的自定义布局
  final Widget? customBtnWidget;

  /// 左边按钮文字，默认"取消"
  final String? leftBtnText;

  /// 左边按钮文字，默认"确定"
  final String? rightBtnText;

  /// 左边按钮回调
  final Function()? onLeftBtnTap;

  /// 右边按钮回调
  final Function()? onRightBtnTap;

  /// 标题文字颜色
  final Color? titleColor;

  /// 左边按钮文字颜色
  final Color? leftBtnColor;

  /// 右边按钮文字颜色
  final Color? rightBtnColor;

  /// 左边按钮背景颜色
  final Color? leftBtnBgColor;

  /// 右边按钮背景颜色
  final Color? rightBtnBgColor;

  /// 标题文字大小
  final double? titleFontSize;

  /// 按钮文字
  final double? btnFontSize;

  /// 按钮高度
  final double? btnHeight;

  //只显示左侧
  final bool isShowLeft;

  //只显示右侧
  final bool isShowRight;

  ///是否展示右上角的x
  final bool isShowClose;

  // 上边距
  final double? customTopPadding;

  // 下边距
  final double? customBottomPadding;

  // 弹窗宽度
  final double? dialogWidth;

  // 弹窗圆角
  final double? radius;

  /// 用于自定义键盘顶起来的高度
  final double Function(double bottomPadding)? bottomPaddingBuilder;

  const KqPadCustomDialog({
    Key? key,
    this.titleString,
    required this.customWidget,
    this.customBtnWidget,
    this.leftBtnText,
    this.rightBtnText,
    this.leftBtnBgColor,
    this.rightBtnBgColor,
    this.titleColor,
    this.leftBtnColor,
    this.rightBtnColor,
    this.titleFontSize,
    this.btnFontSize,
    this.btnHeight,
    this.onLeftBtnTap,
    this.onRightBtnTap,
    this.customTopPadding,
    this.customBottomPadding,
    this.dialogWidth,
    this.isShowLeft = true,
    this.isShowRight = true,
    this.radius,
    this.bottomPaddingBuilder,
    this.isShowClose = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // todo 平板布局写死横屏，竖屏要重写布局，用不到pad的组件库，所以不用判断横竖屏
    // bool isLangScape = context.orientation == Orientation.landscape;

    final List<Widget> children = <Widget>[];

    /// 标题
    if (titleString != null) {
      // 上边距
      children.add(Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(
                vertical: customTopPadding ?? 8.dm, horizontal: 24.dm),
            child: Text(
              titleString!,
              textAlign: TextAlign.center,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontSize: 8.dm,
                  color: KqPadThemeColors.text26,
                  height: 1,
                  fontWeight: FontWeight.w600),
            ),
          ),
          if (isShowClose)
            Positioned(
                right: 6.dm,
                top: 0,
                bottom: 0,
                child: Container(
                  alignment: Alignment.center,
                  child: KqInkWell(
                    onTap: () {
                      if (onLeftBtnTap != null) {
                        onLeftBtnTap!();
                      }
                    },
                    verticalPadding: 4.dm,
                    horizontalPadding: 4.dm,
                    radius: 100.dm,
                    child: Image.asset(
                      Images.commonIcCloseDialog16,
                      width: 10.dm,
                      height: 10.dm,
                      fit: BoxFit.contain,
                      package: KqPadGlobal.packageName,
                    ),
                  ),
                )),
        ],
      ));
      children.add(KqPadDivider.normal());
    } else {
      children.add(SizedBox(
        width: 1.dm,
        height: customTopPadding ?? 18.dm,
      ));
    }

    //自定义内容
    children.add(customWidget);

    /// 下边距
    children.add(SizedBox(
      width: 1.dm,
      height: customBottomPadding ?? 18.dm,
    ));

    /// 底部按钮
    if (isShowLeft || isShowRight) {
      children.add(KqPadDivider.normal(
        height: 1,
        showShadow: false,
      ));
    }

    children.add(customBtnWidget ??
        Row(
          children: [
            //设置了左侧
            if (isShowLeft)
              Expanded(
                  child: KqInkWell(
                onTap: () {
                  if (onLeftBtnTap != null) {
                    onLeftBtnTap!();
                  }
                },
                child: Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  height: btnHeight ?? 24.dm,
                  color: leftBtnBgColor,
                  child: Text(
                    leftBtnText ?? KqPadIntl.currentResource.cancel,
                    style: TextStyle(
                        fontSize: btnFontSize ?? 8.dm,
                        color: leftBtnColor ?? KqPadThemeColors.text59),
                  ),
                ),
              )),
            //左右都存在 展示间隙
            if (isShowLeft && isShowRight)
              KqPadVerticalDivider.normal(
                  height: 22.dm, width: 1, showShadow: false),

            //显示右侧
            if (isShowRight)
              Expanded(
                  child: KqInkWell(
                onTap: () {
                  if (onRightBtnTap != null) {
                    onRightBtnTap!();
                  }
                },
                child: Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  height: btnHeight ?? 24.dm,
                  color: rightBtnBgColor ?? KqPadThemeColors.bgHtGreen,
                  child: Text(
                    rightBtnText ?? KqPadIntl.currentResource.ok,
                    style: TextStyle(
                        fontSize: btnFontSize ?? 8.dm,
                        color: rightBtnColor ?? KqPadThemeColors.textWhite),
                  ),
                ),
              )),

            //不展示左侧 右侧
            if (!isShowLeft && !isShowRight) Container()
          ],
        ));

    Widget dialogChild = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children,
    );
    var padding = MediaQuery.of(context).viewInsets.bottom;
    return UnconstrainedBox(
        child: Container(
            padding: EdgeInsets.only(
                bottom: bottomPaddingBuilder?.call(padding) ?? padding / 2),
            width: dialogWidth ?? 160.dm,
            child: Material(
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.all(Radius.circular(radius ?? 8.dm))),
                color: KqPadThemeColors.padLightBg,
                child: ClipRRect(
                  borderRadius:
                      BorderRadius.all(Radius.circular(radius ?? 8.dm)),
                  child: dialogChild,
                ))));
  }
}
