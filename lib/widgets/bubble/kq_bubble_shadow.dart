import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';

class KqPadBubbleShadow extends StatefulWidget {
  /// 值
  final String? value;

  /// 字体大小
  final double? fontSize;

  /// 宽：默认74.r
  final double? width;

  /// 高：默认74.r
  final double? height;

  /// icon
  final String? bgIcon;

  /// icon package
  final String? packageName;
  const KqPadBubbleShadow(
      {this.value = '',
      this.fontSize,
      this.width,
      this.height,
      this.bgIcon,
      this.packageName,
      super.key});

  @override
  State<KqPadBubbleShadow> createState() => _KqPadBubbleShadowState();
}

class _KqPadBubbleShadowState extends State<KqPadBubbleShadow> {
  String newValue = '';
  @override
  void initState() {
    super.initState();
    newValue = widget.value ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width ?? 28.dm,
      height: widget.height ?? 19.dm,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(widget.bgIcon ?? Images.commonIcQipao,
                  package: widget.packageName ?? KqPadGlobal.packageName))),
      child: Center(
        child: Text(
          newValue,
          style: TextStyle(
              fontSize: widget.fontSize ?? 8.dm,
              color: KqPadThemeColors.text26,
              fontWeight: FontWeight.w500),
        ),
      ),
    );
  }

  @override
  void didUpdateWidget(covariant KqPadBubbleShadow oldWidget) {
    super.didUpdateWidget(oldWidget);
    newValue = widget.value ?? '';
  }
}
