import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';

import '../../resources/images.dart';
import 'kq_pad_drag_list_view.dart';

class KqPadDeleteAndDragListView extends StatefulWidget {
  final int itemCount;
  final ReorderCallback onReorder;
  final Function(int index) itemBuilder;
  final Function(int index)? onDeleted;
  final double? deleteImageLeftMargin;
  final double? height;

  const KqPadDeleteAndDragListView({
    super.key,
    required this.itemCount,
    required this.onReorder,
    required this.itemBuilder,
    this.onDeleted,
    this.deleteImageLeftMargin,
    this.height,
  });

  @override
  State<StatefulWidget> createState() => _KqPadDeleteAndDragListViewState();
}

class _KqPadDeleteAndDragListViewState
    extends State<KqPadDeleteAndDragListView> {
  @override
  Widget build(BuildContext context) {
    return KqPadDragListView.builder(
      itemBuilder: (context, index) => SizedBox(
        key: ValueKey("index$index"),
        height: widget.height ?? 20.dm,
        child: Stack(
          children: [
            Positioned.fill(child: widget.itemBuilder.call(index)),
            Positioned(
              left: widget.deleteImageLeftMargin ?? 0,
              top: 0,
              bottom: 0,
              child: Center(
                child: KqInkWell(
                  onTap: () {
                    widget.onDeleted?.call(index);
                  },
                  child: KqImage.assets(
                    url: Images.commonIcRemoveFillDisabled,
                    width: 8.dm,
                    height: 8.dm,
                    package: KqPadGlobal.packageName,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      itemCount: widget.itemCount,
      onReorder: widget.onReorder,
    );
  }
}
