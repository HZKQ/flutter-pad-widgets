import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/widgets/ImmTemperatureViewExpo/kq_pad_imm_temperature_view_expo.dart';

/// 注塑机多段温度-海天展会组件
class KqPadTemperatureCompleteExpo extends StatefulWidget {
  const KqPadTemperatureCompleteExpo(
      {super.key,
      this.data,
      this.isCanEdit = false,
      this.decimalPlaces = '0',
      this.lowerLimitValue,
      this.upperLimitValue,
      this.fontColor,
      this.tempImageColor,
      this.valueCallback});

  /// 用逗号隔开的熔体温度
  final String? data;

  /// 保留位数
  final String? decimalPlaces;

  /// 是否可编辑吗
  final bool isCanEdit;

  /// 字体颜色
  final Color? fontColor;

  /// 温度图片颜色
  final Color? tempImageColor;

  /// 下限值
  final double? lowerLimitValue;

  /// 上限值
  final double? upperLimitValue;

  final void Function(String data)? valueCallback;

  @override
  State<StatefulWidget> createState() => KqPadTemperatureCompleteExpoState();
}

class KqPadTemperatureCompleteExpoState
    extends State<KqPadTemperatureCompleteExpo> {
  List<String> value = ['-'];

  @override
  void initState() {
    super.initState();
    if (widget.data != null) {
      value = widget.data!.split(',');
    }
  }

  @override
  void didUpdateWidget(covariant KqPadTemperatureCompleteExpo oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.data != null) {
      value = widget.data!.split(',');
    } else {
      value = ['-'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        double width = constraints.maxWidth;

        return Column(
          children: [
            Row(
              children: _renderImg(width),
            ),
            KqPadImmTemperatureViewExpo(
              data: widget.data,
              isCanEdit: widget.isCanEdit,
              fontColor: widget.fontColor,
              upperLimitValue: widget.upperLimitValue,
              lowerLimitValue: widget.lowerLimitValue,
              decimalPlaces: widget.decimalPlaces,
              valueCallback: widget.valueCallback,
            )
          ],
        );
      },
    );
  }

  List<Widget> _renderImg(double width) {
    double temperaturePartWidth =
        (width - 18.dm - 6.dm * (value.length + 1)) / (value.length);
    List<Widget> imgs = [];
    for (var i = 0; i < value.length; i++) {
      imgs.add(Container(
        margin: EdgeInsets.only(
          left: i == 0 ? 18.dm : 6.dm,
        ),
        width: temperaturePartWidth,
        child: Center(
          child: KqImage.assets(
            fit: BoxFit.cover,
            url: Images.commonIcWendu,
            package: KqPadGlobal.packageName,
            width: 24.dm,
            height: 24.dm,
            color: widget.tempImageColor,
          ),
        ),
      ));
    }
    return imgs;
  }
}
