import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/widgets/slider/kq_gradient_slider_bar.dart';

class KqPadBurst extends StatefulWidget {
  /// 最小值
  final double min;

  /// 最大值
  final double max;

  /// 分段背景色
  final List<Color> bgColors;

  /// 分段值，比背景色数组元素数量少1
  final List<String> burstValue;

  /// 当前值
  final String? currentValue;

  /// bar高度
  final double? barHeight;

  /// 滑块颜色
  final Color? sliderBlockColor;

  /// 是否显示分段圆点
  final bool isShowBurstCircle;

  const KqPadBurst(
      {this.min = 0,
      this.max = 100,
      this.barHeight,
      this.sliderBlockColor = KqPadThemeColors.bgWhite,
      this.currentValue,
      this.isShowBurstCircle = false,
      required this.bgColors,
      required this.burstValue,
      super.key});

  @override
  State<KqPadBurst> createState() => _KqPadBurstState();
}

class _KqPadBurstState extends State<KqPadBurst> {
  double height = 0;
  @override
  void initState() {
    super.initState();
    height = widget.barHeight ?? 8.dm;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        double width = constraints.maxWidth;

        return Container(
          height: height,
          width: width,
          clipBehavior: Clip.none,
          child: Stack(
            alignment: AlignmentDirectional.center,
            clipBehavior: Clip.none,
            children: [
              Container(
                  width: width,
                  height: height,
                  decoration: KqBoxDecoration(
                    gradient: const LinearGradient(
                        begin: Alignment.centerRight,
                        end: Alignment.bottomLeft,
                        stops: [
                          0.0,
                          1.0
                        ],
                        colors: [
                          Color.fromRGBO(230, 231, 237, 1),
                          Color.fromRGBO(247, 248, 250, 1)
                        ]),
                    borderRadius: BorderRadius.circular(height / 2),
                    boxShadow: [
                      KqBoxShadow(
                        color: const Color(0xFFBDC1D1),
                        offset: Offset(2.dm, 2.dm),
                        blurRadius: 10.dm,
                        spreadRadius: 0,
                      ),
                      KqBoxShadow(
                        color: const Color(0xFFFAFBFC),
                        offset: Offset(-3.dm, -3.dm),
                        blurRadius: 8.dm,
                        spreadRadius: 0,
                      ),
                      KqBoxShadow(
                        color: const Color(0xFFE9EAF2),
                        offset: Offset(1.dm, 1.dm),
                        blurRadius: 8.dm,
                        spreadRadius: 0,
                        inset: true,
                      ),
                    ],
                  )),
              ..._renderBurstBar(width)
            ],
          ),
        );
      },
    );
  }

  List<Widget> _renderBurstBar(double width) {
    List<Widget> bars = [];
    List<double> burstRightValue = [];
    for (var i = 0; i < widget.bgColors.length; i++) {
      if (i == 0) {
        if (i < widget.burstValue.length) {
          double value = ((double.parse(widget.burstValue[i]) - widget.min) /
                  (widget.max - widget.min)) *
              width;
          burstRightValue.add(value);
          bars.add(Positioned(
            left: 0,
            top: 0,
            width: value,
            height: height,
            child: Container(
              width: value,
              height: height,
              decoration: BoxDecoration(
                  color: widget.bgColors[i],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(height / 2),
                      bottomLeft: Radius.circular(height / 2))),
            ),
          ));

          bars.add(Positioned(
            left: value - 50.dm / 2,
            width: 50.dm,
            top: height + 2.dm,
            child: Center(
              child: Text(
                widget.burstValue[i].toString(),
                style:
                    TextStyle(fontSize: 8.dm, color: KqPadThemeColors.text59),
              ),
            ),
          ));
        }
      } else if (i == widget.bgColors.length - 1) {
        bars.add(Positioned(
          left: burstRightValue[i - 1],
          top: 0,
          right: 0,
          height: height,
          child: Container(
            height: height,
            decoration: BoxDecoration(
                color: widget.bgColors[i],
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(height / 2),
                    bottomRight: Radius.circular(height / 2))),
          ),
        ));
      } else {
        if (i < widget.burstValue.length) {
          double value = ((double.parse(widget.burstValue[i]) - widget.min) /
                  (widget.max - widget.min)) *
              width;
          burstRightValue.add(value);
          bars.add(Positioned(
              left: burstRightValue[i - 1],
              top: 0,
              width: value,
              height: height,
              child: Container(
                width: value,
                height: height,
                decoration: BoxDecoration(
                    color: widget.bgColors[i],
                    borderRadius: BorderRadius.only(
                        topLeft: burstRightValue[i - 1] == 0
                            ? Radius.circular(height / 2)
                            : const Radius.circular(0),
                        bottomLeft: burstRightValue[i - 1] == 0
                            ? Radius.circular(height / 2)
                            : const Radius.circular(0),
                        topRight: value >= width
                            ? Radius.circular(height / 2)
                            : const Radius.circular(0),
                        bottomRight: value >= width
                            ? Radius.circular(height / 2)
                            : const Radius.circular(0))),
              )));

          bars.add(Positioned(
            left: value - 50.dm / 2,
            width: 50.dm,
            top: height + 2.dm,
            child: Center(
              child: Text(
                widget.burstValue[i].toString(),
                style:
                    TextStyle(fontSize: 8.dm, color: KqPadThemeColors.text59),
              ),
            ),
          ));
        }
      }
    }

    if (widget.isShowBurstCircle) {
      for (var i = 0; i < burstRightValue.length; i++) {
        bars.add(Positioned(
          left: burstRightValue[i] - height / 2,
          top: 0,
          width: height,
          height: height,
          child: Container(
            width: height,
            height: height,
            decoration: BoxDecoration(
                color: widget.sliderBlockColor,
                borderRadius: BorderRadius.circular(height / 2)),
          ),
        ));
      }
    }

    if (widget.currentValue != null) {
      double value = ((double.parse(widget.currentValue!) - widget.min) /
              (widget.max - widget.min)) *
          width;
      bars.add(Positioned(
        left: value - height / 2,
        top: 0,
        width: height,
        height: height,
        child: Container(
          width: height,
          height: height,
          decoration: BoxDecoration(
              color: widget.sliderBlockColor,
              borderRadius: BorderRadius.circular(height / 2)),
        ),
      ));

      bars.add(Positioned(
        left: value - 50.dm / 2,
        top: -24.dm,
        width: 50.dm,
        height: 22.dm,
        child: Center(
          child: KqPadShadowPop(
              width: 42.dm,
              height: 19.dm,
              radius: 4.dm,
              textTop: 4.dm,
              arrowWidth: 4.5.dm,
              arrowHeight: 2.5.dm,
              arrowOffsetX: 0,
              text: widget.currentValue!),
        ),
      ));
    }
    return bars;
  }
}
