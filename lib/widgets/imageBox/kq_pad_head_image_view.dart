import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/utils/file_preview_util.dart';
import 'package:kq_flutter_core_widget/utils/native/file_picker_native_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/widgets/imageBox/kq_image_box.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_core_widget/utils/ex/kq_ex.dart';
import '../../config/kq_pad_global.dart';
import '../../resources/images.dart';
import '../../resources/kq_pad_theme_colors.dart';
import '../../theme/kq_theme_manager.dart';
import '../dialog/kq_pad_file_picker.dart';
import '../form/entity/kq_form_entity.dart';

class KqPadHeadImageView extends StatefulWidget {
  const KqPadHeadImageView({
    super.key,
    required this.entity,
    required this.defaultUrl,
    this.didSelected,
  });

  //展示文本
  final KqPadFormEntity entity;

  //默认展示的
  final ImageUrl defaultUrl;

  /// 图片添加按钮点击回调
  final Function(KqFile file)? didSelected;

  @override
  State<KqPadHeadImageView> createState() => _KqPadHeadImageViewState();
}

class _KqPadHeadImageViewState extends State<KqPadHeadImageView> {
  //没有上传：
  //编辑状态下    显示   默认图 + 文本
  //只读状态下    显示   默认图
  //有上传
  //编辑状态下 图片
  //只读状态下 图片
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.dm,
      alignment: Alignment.center,
      color: widget.entity.backgroundColor ?? KqPadThemeColors.padLightBg,
      padding: EdgeInsets.symmetric(vertical: 2.dm),
      child: _buildNormal(),
    );
  }

  ///图片处理
  _buildNormal() {
    return ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: Stack(
        children: [
          //上传了图片
          if (widget.entity.imageUrls.isNotNullOrEmpty)
            KqInkWell(
                onTap: () {
                  //不允许编辑放大图片
                  if (widget.entity.editable) {
                    handleUpload();
                  } else {
                    if (widget.entity.imageUrls.isNotNullOrEmpty) {
                      handlePreview(widget.entity.imageUrls![0]);
                    }
                  }
                },
                child: Container(
                    color: widget.entity.backgroundColor ??
                        KqPadThemeColors.padLightBg,
                    alignment: Alignment.center,
                    padding: EdgeInsets.zero,
                    child: renderIcon(widget.entity.imageUrls![0]))),
          //未上传图片
          if (widget.entity.imageUrls.isNullOrEmpty)
            KqInkWell(
                onTap: () {
                  //点击上传
                  if (widget.entity.editable) {
                    handleUpload();
                  }
                },
                child: Container(
                    color: widget.entity.backgroundColor ??
                        KqPadThemeColors.padLightBg,
                    height: widget.entity.editable ? 44.dm : 56.dm,
                    alignment: Alignment.center,
                    child: renderIcon(widget.defaultUrl))),

          ///编辑+未上传图片
          Visibility(
              visible: widget.entity.editable &&
                  widget.entity.imageUrls.isNullOrEmpty,
              child: Positioned(
                  bottom: 4.dm,
                  left: 0,
                  right: 0,
                  child: Container(
                    padding: EdgeInsets.only(top: 2.dm),
                    alignment: Alignment.center,
                    child: Text(
                      widget.entity.title,
                      style: TextStyle(
                          color: widget.entity.contentColor ??
                              KqPadThemeColors.text26,
                          fontSize: KqPadThemeManager.instance
                                  .getConfig()
                                  .formConfig
                                  .titleFontSize ??
                              7.dm),
                    ),
                  ))),

          ///编辑状态 + 已经上传了图片
          Visibility(
              visible: widget.entity.editable &&
                  widget.entity.imageUrls.isNotNullOrEmpty,
              child: Positioned(
                  right: 8.dm, //偏移点，要不然会有边
                  top: 2,
                  child: InkWell(
                      onTap: () {
                        //删除
                        setState(() {
                          widget.entity.imageUrls = [];
                        });
                      },
                      child: Container(
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.all(3.dm),
                        child: KqImage.assets(
                          fit: BoxFit.cover,
                          url: Images.commonIcClose16,
                          package: KqPadGlobal.packageName,
                          width: 14.dm,
                          height: 14.dm,
                        ),
                      )))),
        ],
      ),
    );
  }

  Widget renderIcon(ImageUrl imageUrl) {
    return KqImage.custom(
      imageType: imageUrl.imageType,
      fit: BoxFit.contain,
      url: imageUrl.url,
      width: context.width,
      height: widget.entity.editable && widget.entity.imageUrls.isNullOrEmpty
          ? 44.dm
          : 56.dm,
      placeHolder: widget.defaultUrl.url,
      errorImage: widget.defaultUrl.url,
    );
  }

// 放大图片
  void handlePreview(ImageUrl? imgUrl) {
    FilePreviewUtil.previewSingleImg(imgUrl!);
  }

  //上传
  void handleUpload() {
    KqPadFilePicker().choosePhoto(context, ((files) {
      KqFile file = files.first;
      if (widget.didSelected != null) {
        widget.didSelected!(file);
      }
    }));
  }
}
