import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kq_flutter_pad_widgets/widgets/form/kq_empty_item_view.dart';
import 'package:kq_flutter_core_widget/widgets/imageBox/kq_image_box.dart';
import 'package:kq_flutter_pad_widgets/widgets/form/kq_select_item_view.dart';
import '../kq_radio_item_view.dart';

/// 控件类型
enum ItemType {
  /// 输入控件 [KqPadEditItemView]
  ///
  /// 【是否必填】 + 左边标题 + 【左边自定义】 + 右边输入框  + 【右边自定义】 + 【单位】
  edit,

  /// 单选控件 [KqPadRadioItemView]
  ///
  /// 【是否必填】 + 左边标题 + 右侧单选按钮组
  radio,

  /// 选择控件 [KqPadSelectItemView]
  ///
  /// 【是否必填】 + 左边标题 + 【左边自定义】 + 右侧内容 + 【右边自定义】+ 右侧箭头
  select,

  /// 头部 [KqPadHeaderItemView]
  ///
  /// 【是否必填】 + 左边标题
  header,

  /// 输入上限和下限的双重输入控件 [KqPadEditUpperLowerItemView]
  ///
  /// 【是否必填】 + 左边标题 + 下限输入框 + 上限输入框 + 【单位】
  editUpperLower,

  /// 图片上传 [KqImageBoxItemView]
  ///
  /// 【是否必填】 + 标题 + 底部图片容器
  imageBox,

  /// 空行 [KqPadEmptyItemView]，高度6
  empty,
}

/// 通用表单组件实体类
class KqPadFormEntity {
  /// 控件类型，用来判断当前属于那种表单控件，不传默认empty
  final ItemType? itemType;

  /// 左边标题
  late String title;

  /// 输入框提示文字
  String? hint;

  /// 右边单位
  String? unit;

  /// 是否展示右边单位
  final bool showUnit;

  /// 是否可以编辑，默认true，[editableMask]&[editable] 代表真实的能否编辑
  late bool editable;

  /// 是否可以编辑，默认true，[editableMask]&[editable] 代表真实的能否编辑
  late bool editableMask;

  /// 非编辑模式是否强制显示hint
  final bool forceShowPlaceHolder;

  /// 数量组件是否可以删除，默认true
  late bool removable;

  /// 是否多行输入，默认false
  late bool multiline;

  /// 最大行数
  late int? maxLines;

  /// 最多输入几个字符
  final int? maxCharCount;

  /// 标题样式自定义，为了满足某些特殊场景标题样式不一致
  final TextStyle? titleStyle;

  /// 标题颜色,注意：[titleStyle]优先
  Color? titleColor;

  /// 标题字体大小，如果配置了[titleStyle]，则优先使用[titleStyle]
  double? titleFontSize;

  /// 粗体
  FontWeight? titleFontWeight;

  /// 内容字体大小
  double? contentFontSize;

  /// 内容颜色
  Color? contentColor;

  /// 单位颜色
  Color? unitColor;

  /// 必填项*号颜色
  Color? mustInputStarColor;

  /// 选择控件右边文字单行时，超出处理方式
  final TextOverflow? overflow;

  /// 默认值或实际值，用于显示
  String? valueForShow;

  /// 多个默认值或实际值，用于显示[valuesForShow[0]对应从左到右第一个文本框或输入框的显示值，以此类推，未填值的要传null，不能跳过]
  List<String?>? valuesForShow;

  /// 提交值，用于提交给后端
  String? valueForSubmit;

  /// 多个提交值，用于提交给后端[valuesForSubmit[0]对应从左到右第一个文本框或输入框的提交值，以此类推，未填值的要传null，不能跳过]
  List<String?>? valuesForSubmit;

  /// 自定义的标识符，保留字段
  String? itemId;

  /// 用于提交给后端的字段名，提交时对应[valueForSubmit]
  String? contentKey;

  /// 用于提交给后端的字段名，提交时对应[valuesForSubmit]
  List<String>? contentKeyForChild;

  /// 用于提交给后端的字段名，有时候后端不仅要提交数据id，还要求提交数据名称时，用这个，提交时对应[valueForShow]
  String? contentKeyForShow;

  /// 用于提交给后端的字段名，数字表单控件[KqOnlyNumberItemView]，因此数字使用这个字段，提交时对应[qty]
  String? contentKeyForQty;

  /// 用于提交给后端的字段名，提交时对应[unit]
  String? contentKeyForUnit;

  /// 是否可见，默认可见
  late bool visible;

  /// 整体背景颜色
  final Color? backgroundColor;

  /// 是否隐藏[KqImageBoxItemView]右上角的数量统计"1/8"
  final bool hideImageBoxCountInfo;

  /// 数量控件的最小值，小于这个值都显示这个值
  final num minQty;

  /// [KqOnlyNumberItemView] 加减幅度，默认1，比如[qtyStep]=2,则点击加号，数字+2，点击减号，数字-2
  late num qtyStep;

  /// 数量控件的数量
  num qty;

  /// 键盘显示类型
  TextInputType? inputType;

  /// 键盘右下角按钮类型
  TextInputAction? textInputAction;

  /// 输入框提交回调
  ValueChanged<String>? onSubmitted;

  /// 输入框是否带清空按钮
  final bool enableClearButton;

  /// 输入内容限制，建议使用[TextInputFormatUtils]
  List<TextInputFormatter>? inputFormatters;

  /// TextEditingController
  TextEditingController? controller;

  /// 多个输入框的TextEditingController
  List<TextEditingController?>? controllers;

  /// 自定义标题距离右边的距离
  double? titleRightMargin;

  /// 图片添加按钮点击回调
  final Function(KqPadFormEntity entity, KqImageBoxState state)? onAddTap;

  /// 图片删除按钮点击回调
  final Function(KqPadFormEntity entity, ImageUrl imageUrl, int position,
      KqImageBoxState state)? onDeleteTap;

  /// 图片点击回调
  final Function(KqPadFormEntity entity, ImageUrl imageUrl, int position,
      KqImageBoxState state)? onImageTap;

  /// 输入控件或选择控件的左侧控件
  Widget Function(KqPadFormEntity entity)? customLeftView;

  /// 输入控件或选择控件的标题和内容之间的控件
  Widget Function(KqPadFormEntity entity)? customTitleRightView;

  /// 输入控件或选择控件或自定义控件的右侧控件
  Widget Function(KqPadFormEntity entity)? customRightView;

  /// 自定义控件的底部控件
  Widget Function(KqPadFormEntity entity)? customBottomView;

  /// 输入变化回调
  ValueChanged<String>? onChanged;

  /// 多个输入框项的输入变化回调
  Function(String value, int index)? onChangedWidthIndex;

  /// 输入变化回调，延迟500ms回调，适用于[KqEditItemView]
  Function(KqPadFormEntity entity, String value)? onMonitor;

  /// 多个输入框项的输入变化回调，延迟500ms回调
  Function(KqPadFormEntity entity, String value, int index)?
      onMonitorWidthIndex;

  /// 焦点变化
  Function(bool hasFocus)? onFocusChanged;

  /// 多个输入框项的焦点变化
  Function(bool hasFocus, int index)? onFocusChangedWidthIndex;

  /// [KqSwitcherItemView]变化监听
  ValueChanged<bool>? onCheckedChanged;

  /// 控件状态State对象
  late dynamic widgetState;

  /// 自定义内容
  late dynamic custom;

  /// 自定义内容
  late dynamic extraData;

  /// 最大数量，适用于 [KqImageBoxItemView] 和 [KqOnlyNumberItemView]
  num? maxQty;

  /// 图片选择显示列数，默认4
  int imageBoxCols;

  /// 图片类型和路径
  List<ImageUrl>? imageUrls;

  /// 强制使用onTap属性，忽略控件是否处于编辑状态。适用于[KqSelectItemView]
  bool forceOnTap;

  /// 单选控件的数据，如果要么默认选中某个，[valueForSubmit] 赋值成选中的 [RadioData]的[itemId]
  final List<RadioData>? radioData;

  /// [KqPadRadioItemView]单选组件单选点击回调
  final Function(KqPadFormEntity entity, RadioData radioData)? onRadioChanged;

  /// [KqPadRadioItemView]组件的，为true时可以反选
  bool? canInvert;

  /// 自定义FocusNode
  FocusNode? focusNode;

  /// 自定义或选择控件的标题点击回调
  Function(KqPadFormEntity entity)? onTitleTap;

  /// 垂直边距，不赋值则使用主题配置
  final double? paddingVertical;

  /// 水平边距，不赋值则使用主题配置
  final double? paddingHorizontal;

  /// 是否必填，必填有星号，默认false
  late bool mustInput;

  /// 自定义或选择控件点击回调
  Function(KqPadFormEntity entity)? onTap;

  /// 选择控件箭头右侧的自定义控件
  Widget Function(KqPadFormEntity entity)? customSelectArrowRightView;

  /// 头的文本大小，考虑到目前有时候存在多种头的文本大小，所以从统一配置中提出来
  double? headerFontSize;

  /// 标题固定宽度时使用的中文字个数，默认8个
  int? titleFixedWidthCharCount;

  /// 标题对齐方式，默认左对齐
  TextAlign titleTextAlign;

  /// 内容对齐方式，默认编辑状态左对齐，非编辑状态右对齐
  TextAlign? contentTextAlign;

  /// 是否显示分隔栏，和分割线有所区别，这个上下有一定的高度，默认false
  bool showDivider2;

  /// 是否强制隐藏分割线1，分割线1是没有上下边距的，默认编辑状态隐藏，非编辑状态显示。
  bool forceHideDivider1;

  /// 当 showDivider2 = true 时， 是否隐藏分割栏顶部的5.dm高度
  bool hideDivider2TopMargin;

  /// 当 showDivider2 = true 时， 是否隐藏分割栏底部的5.dm高度
  bool hideDivider2BottomMargin;

  /// 选择控件清空按钮点击回调，仅适用于 [KqPadSelectItemView]
  final Function(KqPadFormEntity entity)? onClearTap;

  KqPadFormEntity({
    this.itemType = ItemType.edit,
    this.title = '',
    this.hint,
    this.unit,
    this.titleFontSize,
    this.titleFontWeight,
    this.contentFontSize,
    this.titleTextAlign = TextAlign.start,
    this.contentTextAlign,
    this.showUnit = false,
    this.editable = true,
    this.editableMask = true,
    this.multiline = false,
    this.forceShowPlaceHolder = false,
    this.removable = true,
    this.enableClearButton = false,
    this.visible = true,
    this.forceOnTap = false,
    this.titleFixedWidthCharCount,
    this.maxQty,
    this.imageBoxCols = 4,
    this.onTap,
    this.mustInput = false,
    this.paddingHorizontal,
    this.paddingVertical,
    this.backgroundColor,
    this.maxCharCount,
    this.controller,
    this.controllers,
    this.hideImageBoxCountInfo = false,
    this.inputType,
    this.textInputAction,
    this.onSubmitted,
    this.maxLines,
    this.onAddTap,
    this.onDeleteTap,
    this.onImageTap,
    this.onTitleTap,
    this.minQty = 1,
    this.qty = 1,
    this.qtyStep = 1,
    this.titleStyle,
    this.titleColor,
    this.contentColor,
    this.unitColor,
    this.mustInputStarColor,
    this.headerFontSize,
    this.overflow,
    this.inputFormatters,
    this.valueForShow,
    this.valuesForShow,
    this.valueForSubmit,
    this.valuesForSubmit,
    this.contentKeyForChild,
    this.titleRightMargin,
    this.customLeftView,
    this.customTitleRightView,
    this.customRightView,
    this.customBottomView,
    this.onFocusChanged,
    this.onFocusChangedWidthIndex,
    this.onChanged,
    this.onChangedWidthIndex,
    this.onMonitor,
    this.onMonitorWidthIndex,
    this.onCheckedChanged,
    this.contentKey,
    this.radioData,
    this.onRadioChanged,
    this.contentKeyForQty,
    this.contentKeyForShow,
    this.contentKeyForUnit,
    this.customSelectArrowRightView,
    this.onClearTap,
    this.itemId,
    this.widgetState,
    this.custom,
    this.extraData,
    this.canInvert,
    this.focusNode,
    this.imageUrls,
    this.showDivider2 = false,
    this.forceHideDivider1 = false,
    this.hideDivider2TopMargin = false,
    this.hideDivider2BottomMargin = false,
  });

  /// 是否真正能编辑
  bool isEditable() {
    return editable && editableMask;
  }

  /// 刷新绑定的控件
  void updateWidget() {
    if (widgetState != null) {
      widgetState.update();
    }
  }
}
