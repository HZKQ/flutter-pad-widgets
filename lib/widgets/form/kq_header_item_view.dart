import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import '../../resources/kq_pad_theme_colors.dart';
import '../../theme/kq_theme_manager.dart';
import 'entity/kq_form_entity.dart';

/// 通用表单组件 - 表单分类头
///
/// [ItemType.header]
///
/// @author 周卓
///
class KqPadHeaderItemView extends StatelessWidget {
  /// 通用属性
  final KqPadFormEntity entity;

  const KqPadHeaderItemView({Key? key, required this.entity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var bgColor = entity.backgroundColor ?? KqPadThemeColors.bgDark;
    return Visibility(
      visible: entity.visible,
      child: Container(
        width: double.infinity,
        color: bgColor,
        padding: EdgeInsets.only(
            left: 0,
            right: entity.paddingHorizontal ??
                KqPadThemeManager.instance
                    .getConfig()
                    .formConfig
                    .paddingHorizontal ??
                8.dm,
            top: entity.paddingVertical ?? 5.dm,
            bottom: entity.paddingVertical ?? 5.dm),
        child: Row(
          children: [
            /// 是否必填
            Container(
              width: entity.paddingHorizontal ??
                  KqPadThemeManager.instance
                      .getConfig()
                      .formConfig
                      .paddingHorizontal ??
                  8.dm,
              padding: EdgeInsets.only(right: 2.dm),
              child: Text(
                "∗",
                textAlign: TextAlign.end,
                style: TextStyle(
                    fontSize: KqPadThemeManager.instance
                            .getConfig()
                            .formConfig
                            .titleFontSize ??
                        7.dm,
                    color: entity.mustInput
                        ? KqPadThemeColors.textRed
                        : KqPadThemeColors.bgTransparent),
              ),
            ),

            /// 标题
            Expanded(
              child: Text(
                entity.title,
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: entity.headerFontSize ??
                        (KqPadThemeManager.instance
                                .getConfig()
                                .formConfig
                                .headerFontSize ??
                            7.dm),
                    color: entity.titleColor ?? KqPadThemeColors.text59,
                    fontWeight: entity.titleFontWeight),
              ),
            ),

            ///右侧展示文本
            if (entity.valueForShow != null && entity.valueForShow != '')
              Text(
                entity.valueForShow!,
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: entity.contentFontSize ??
                        (KqPadThemeManager.instance
                                .getConfig()
                                .formConfig
                                .contentFontSize ??
                            7.dm),
                    color: entity.contentColor ?? KqPadThemeColors.text59),
              ),
          ],
        ),
      ),
    );
  }
}
