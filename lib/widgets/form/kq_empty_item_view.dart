import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:get/get.dart';

/// 通用表单组件 - 空白行，高度默认6
///
/// [ItemType.empty]
///
/// @author 周卓
///
class KqPadEmptyItemView extends StatelessWidget {
  /// 空白高度
  final double? height;

  /// 背景颜色，默认透明
  final Color? bgColor;

  const KqPadEmptyItemView({Key? key, this.height, this.bgColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// 空白
    return Container(
      width: context.width,
      height: height ?? 6.dm,
      color: bgColor,
    );
  }
}
