import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/utils/ex/kq_ex.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/widgets/eventTimeLine/entity/kq_event_time_line_entity.dart';

class KqEventTimeLine extends StatefulWidget {
  /// 时间轴数据
  final List<KqEventTimeLineEntity> data;
  final KqEventTimeLineEntity? entity;
  final Function(int index, KqEventTimeLineEntity entity)? selectedCallback;
  const KqEventTimeLine(
      {required this.data, this.entity, this.selectedCallback, super.key});

  @override
  State<KqEventTimeLine> createState() => _KqEventTimeLineState();
}

class _KqEventTimeLineState extends State<KqEventTimeLine> {
  @override
  void didUpdateWidget(covariant KqEventTimeLine oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.data.length != widget.data.length ||
        widget.entity?.id != oldWidget.entity?.id) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.data.isEmpty
        ? Container()
        : ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: widget.data.length + 1,
            itemBuilder: (context, index) {
              if (index == 0) {
                return renderArrowHeadCell();
              } else {
                KqEventTimeLineEntity entity = widget.data[index - 1];
                return renderTimeLineCell(entity, index);
              }
            },
          );
  }

  Widget renderArrowHeadCell() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 1,
          width: 66.dm,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            KqImage.assets(
              url: Images.commonIcSanjiao,
              package: KqPadGlobal.packageName,
              width: 8.dm,
              height: 8.dm,
            ),
            Container(
              color: KqPadThemeColors.bgHtGreen,
              width: 1.dm,
              height: 3.dm,
            )
          ],
        ),
      ],
    );
  }

  Widget renderTimeLineCell(KqEventTimeLineEntity entity, int index) {
    return KqInkWell(
        onTap: () {
          readWithData();
          entity.isSpread = true;
          widget.selectedCallback?.call(index - 1, entity);
          setState(() {});
        },
        child: Container(
          height: entity.event.isNotNullOrEmpty ? 33.dm : 23.dm,
          color: widget.entity != null && entity.id == widget.entity!.id
              ? KqPadThemeColors.bgHeader
              : KqPadThemeColors.bgTransparent,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 7.dm),
                // height: 8.dm,
                width: 66.dm,
                child: Container(
                  margin: EdgeInsets.only(left: 16.dm),
                  child: Text(
                    entity.time ?? '',
                    textDirection: TextDirection.ltr,
                    style: TextStyle(
                        fontSize: 7.dm, color: KqPadThemeColors.text59),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    color: KqPadThemeColors.bgHtGreen,
                    width: 1.dm,
                    height: 8.dm,
                  ),
                  Container(
                    width: 8.dm, // 设置圆形的直径
                    height: 8.dm,
                    decoration: BoxDecoration(
                      color: Colors.white, // 设置背景颜色为白色
                      shape: BoxShape.circle, // 设置形状为圆形
                      border: Border.all(
                        color: KqPadThemeColors.bgHtGreen, // 设置边框颜色为蓝色
                        width: 1.dm, // 设置边框宽度
                      ),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    color: KqPadThemeColors.bgHtGreen,
                    width: 1.dm,
                  ))
                ],
              ),
              SizedBox(
                width: 8.dm,
              ),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 7.dm, right: 4.dm),
                    child: Text(
                      entity.title ?? '',
                      style: TextStyle(
                          fontSize: 7.dm, color: KqPadThemeColors.text26),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                  if (entity.event.isNotNullOrEmpty)
                    Container(
                      margin: EdgeInsets.only(right: 4.dm, bottom: 5.dm),
                      child: Text(
                        entity.event ?? '',
                        style: TextStyle(
                            color: KqPadThemeColors.textFF7E00, fontSize: 6.dm),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    ),
                ],
              )),
            ],
          ),
        ));
  }

  readWithData() {
    for (var i = 0; i < widget.data.length; i++) {
      KqEventTimeLineEntity entity = widget.data[i];
      entity.isSpread = false;
    }
  }
}
