class KqEventTimeLineEntity {
  /// 时间
  String? time;

  ///标题
  String? title;

  ///事件
  String? event;

  /// id
  int? id;

  /// 是否展开
  bool isSpread;

  /// "参数版本类型:0开机参数1黏度2射出压力3保压压力4保压时间5冷却时间6型腔平衡7DOE8锁模力9预测10优化11多级注射(默认：)"
  int? parameterVersionType;

  /// 额外数据
  dynamic extraData;

  KqEventTimeLineEntity(
      {this.time,
      this.title,
      this.event,
      this.id,
      this.isSpread = false,
      this.parameterVersionType,
      this.extraData});
}
