import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:get/get.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';

/// 通用二级进度条
class KqPadHorizontalProgressBar extends StatelessWidget {
  /// 进度条高度，默认6
  final double? height;

  /// 1级进度条的进度值
  final double progress;

  /// 2级进度条的进度值
  final double secondProgress;

  /// 是否启用二级进度条，默认不启用
  final bool enableSecondProgress;

  /// 最大值，默认100
  final double max;

  /// 最小值，默认0
  final double min;

  /// 背景颜色，默认分割线颜色
  final Color bgColor;

  /// 1级进度颜色
  final Color inactiveColor;

  /// 2级进度颜色
  final Color activeColor;

  /// 进度条和背景的圆角大小，默认6
  final double? radius;

  /// 二级进度条形状
  final SecondProgressShape secondProgressShape;

  const KqPadHorizontalProgressBar(
      {Key? key,
      this.height,
      this.progress = 0,
      this.secondProgress = 0,
      this.enableSecondProgress = false,
      this.max = 100,
      this.min = 0,
      this.bgColor = KqPadThemeColors.bgF0,
      this.inactiveColor = KqPadThemeColors.textGreen,
      this.activeColor = KqPadThemeColors.textYellow,
      this.radius,
      this.secondProgressShape = SecondProgressShape.point})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: SizedBox(
          height: height ?? 4.dm,
          child: CustomPaint(
            size: Size(context.width, height ?? 4.dm),
            painter: _KqPadHorizontalProgressBarPaint(
                progress: fixProgress(progress),
                secondProgress: fixProgress(secondProgress),
                max: fixProgress(max),
                min: fixProgress(min),
                bgColor: bgColor,
                inactiveColor: inactiveColor,
                enableSecondProgress: enableSecondProgress,
                activeColor: activeColor,
                radius: radius ?? 6.dm,
                secondProgressShape: secondProgressShape),
          )),
    );
  }

  /// 小于0，进度为0
  double fixProgress(double value) {
    if (value < 0) {
      return 0;
    }
    return value;
  }
}

/// 二级进度条形状
enum SecondProgressShape { point, roundRect }

class _KqPadHorizontalProgressBarPaint extends CustomPainter {
  /// 1级进度条的进度值
  final double progress;

  /// 2级进度条的进度值
  final double secondProgress;

  /// 最大值
  final double max;
  final double min;

  /// 背景颜色
  final Color bgColor;

  final Color inactiveColor;

  final Color activeColor;

  /// 进度条和背景的圆角大小
  final double radius;

  /// 二级进度条形状
  final SecondProgressShape secondProgressShape;

  /// 启动二级进度条
  final bool enableSecondProgress;

  _KqPadHorizontalProgressBarPaint(
      {required this.progress,
      required this.secondProgress,
      required this.max,
      required this.min,
      required this.bgColor,
      required this.inactiveColor,
      required this.activeColor,
      required this.radius,
      required this.enableSecondProgress,
      required this.secondProgressShape});

  @override
  void paint(Canvas canvas, Size size) {
    _drawBg(canvas, size);
    _drawProgress(canvas, size);
    if (enableSecondProgress) {
      _drawSecondProgress(canvas, size);
    }
  }

  // 绘制1级进度条
  _drawProgress(Canvas canvas, Size size) {
    Paint pbPaint = Paint()
      ..isAntiAlias = true
      ..strokeCap = StrokeCap.round
      ..color = activeColor
      ..style = PaintingStyle.fill;
    double left = size.width * ((secondProgress - min) / (max - min));
    double top = 0;
    double bottom = size.height;
    double width = size.width;
    double right = width * ((progress - min) / (max - min));
    if (right > width) {
      right = width;
    }
    canvas.drawRRect(
        RRect.fromLTRBR(left, top, right, bottom, Radius.circular(radius)),
        pbPaint);
  }

  // 绘制2级进度条
  _drawSecondProgress(Canvas canvas, Size size) {
    Paint pbPaint = Paint()
      ..isAntiAlias = true
      ..strokeCap = StrokeCap.round
      ..color = inactiveColor
      ..style = PaintingStyle.fill;
    double left = 0;
    double top = 0;
    double width = size.width;
    double bottom = size.height;
    double halfHeight = bottom / 2;
    if (secondProgressShape == SecondProgressShape.point) {
      //圆点
      double right =
          (size.width - bottom) * ((secondProgress - min) / (max - min)) +
              halfHeight;
      if (right > width - halfHeight) {
        right = width - halfHeight;
      }
      canvas.drawCircle(Offset(right, halfHeight), halfHeight, pbPaint);
    } else {
      //普通进度条
      double right = size.width * ((secondProgress - min) / (max - min));
      if (right > width) {
        right = width;
      }
      double bottom = size.height;
      canvas.drawRRect(
          RRect.fromLTRBR(left, top, right, bottom, Radius.circular(radius)),
          pbPaint);
    }
  }

  // 绘制背景
  _drawBg(Canvas canvas, Size size) {
    Paint bgPaint = Paint()
      ..isAntiAlias = true
      ..strokeCap = StrokeCap.round
      ..color = KqPadThemeColors.bgTransparent
      ..style = PaintingStyle.fill;
    double left = 0;
    double top = 0;
    double right = size.width;
    double bottom = size.height;
    canvas.drawRRect(
        RRect.fromLTRBR(left, top, right, bottom, Radius.circular(radius)),
        bgPaint);

    // const gradient = LinearGradient(
    //     begin: Alignment.topCenter,
    //     end: Alignment.bottomCenter,
    //     colors: [KqPadThemeColors.shadowColor19, Colors.transparent],
    //     tileMode: TileMode.clamp);

    // Paint offsetPaint = Paint()..isAntiAlias = true;

    // double offsetLeft = 0;
    // double offsetTop = 0;
    // double offsetRight = size.width;
    // double offsetBottom = 3.dm;

    // offsetPaint.shader = gradient.createShader(
    //     Rect.fromLTWH(offsetLeft, offsetTop, offsetRight, offsetBottom));
    // canvas.drawRRect(
    //     RRect.fromLTRBR(offsetLeft, offsetTop, offsetRight, offsetBottom,
    //         Radius.circular(radius)),
    //     offsetPaint);
  }

  @override
  bool shouldRepaint(covariant _KqPadHorizontalProgressBarPaint oldDelegate) {
    return true;
  }
}
