import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/widgets/slider/rangeSlider/kq_pad_horizontal_progress_bar.dart';

class KqPadRangeSliderBar extends StatefulWidget {
  final void Function(double start, double end)? onChange;

  final void Function(double start, double end)? onChangeEnd;

  /// seekBar值的format
  final NumberFormat? numberFormat;

  /// 第一个seekbar位置
  final double start;

  /// 第二个seekbar位置
  final double end;

  /// 进度条最大值
  final double max;

  /// 进度条最小值
  final double min;

  final double thumSize;

  ///背景进度颜色
  final Color bgColor;

  /// 未选中的进度条颜色
  final Color inactiveColor;

  /// 选中范围的进度条颜色
  final Color activeColor;

  final Widget? startThumbIcon;
  final Widget? endThumbIcon;

  ///进度条的高度
  final double? progressHeight;

  ///控件高度，默认25.r
  final double height;

  const KqPadRangeSliderBar(
      {this.onChange,
      this.onChangeEnd,
      this.numberFormat,
      this.endThumbIcon,
      this.startThumbIcon,
      required this.start,
      required this.end,
      required this.max,
      required this.min,
      required this.bgColor,
      required this.inactiveColor,
      required this.activeColor,
      required this.height,
      required this.thumSize,
      required this.progressHeight,
      super.key});

  @override
  State<KqPadRangeSliderBar> createState() => _KqPadRangeSliderBarState();
}

class _KqPadRangeSliderBarState extends State<KqPadRangeSliderBar> {
  double _padding = 0;
  double _progress = 0;
  double _secondProgress = 0;
  double _max = 0;
  double _min = 0;

  @override
  void initState() {
    super.initState();
    _max = widget.max;
    _min = widget.min;
    _progress = widget.start;
    _secondProgress = widget.end;
    _padding = widget.thumSize / 2;

    _max = _format(widget.numberFormat, _max);
  }

  @override
  Widget build(BuildContext context) {
    return _renderSliderBar();
  }

  Widget _renderSliderBar() {
    return LayoutBuilder(
      builder: (context, container) {
        double width = container.maxWidth - 2 * _padding;
        double leftOne = width * (_progress - _min) / (_max - _min);
        double leftTwo = width * (_secondProgress - _min) / (_max - _min);
        return SizedBox(
          width: double.infinity,
          height: widget.height,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                left: _padding,
                right: _padding,
                child: Container(
                  clipBehavior: Clip.none,
                  height: widget.progressHeight ?? 4.dm,
                  decoration: KqBoxDecoration(
                      color: KqPadThemeColors.tabBarBgColor,
                      borderRadius: BorderRadius.all(Radius.circular(20.dm)),
                      boxShadow: [
                        KqBoxShadow(
                            offset: Offset(1.dm, 1.dm),
                            blurRadius: 2.dm,
                            spreadRadius: 0,
                            color: KqPadThemeColors.shadowColor3,
                            inset: true),
                        KqBoxShadow(
                            offset: Offset(-1.dm, -1.dm),
                            blurRadius: 3.dm,
                            spreadRadius: 0,
                            color: KqPadThemeColors.textWhite,
                            inset: true)
                      ]),
                  child: KqPadHorizontalProgressBar(
                    height: widget.progressHeight ?? 4.dm,
                    max: _max,
                    min: _min,
                    bgColor: widget.bgColor,
                    inactiveColor: widget.inactiveColor,
                    activeColor: widget.activeColor,
                    enableSecondProgress: true,
                    secondProgressShape: SecondProgressShape.roundRect,
                    progress: _progress,
                    secondProgress: _secondProgress,
                  ),
                ),
              ),
              Positioned(
                left: leftOne,
                child: GestureDetector(
                  onHorizontalDragUpdate: (e) {
                    _progress += (_max - _min) * e.delta.dx / width;
                    if (_progress <= _min) {
                      _progress = _min;
                      if (_progress > _secondProgress) {
                        _secondProgress = _min;
                      }
                    } else if (_progress >= _max) {
                      _progress = _max;
                      _secondProgress = _max;
                    } else if (_progress > _secondProgress) {
                      _secondProgress = _progress;
                    }
                    widget.onChange?.call(
                        _format(widget.numberFormat, _progress),
                        _format(widget.numberFormat, _secondProgress));
                    setState(() {});
                  },
                  onHorizontalDragEnd: (e) {
                    widget.onChangeEnd?.call(
                        _format(widget.numberFormat, _progress),
                        _format(widget.numberFormat, _secondProgress));
                  },
                  child: widget.startThumbIcon ??
                      KqImage.assets(
                        url: Images.commonIcHuakuai28,
                        package: KqPadGlobal.packageName,
                        width: 18.dm,
                        height: 18.dm,
                      ),
                ),
              ),
              Positioned(
                left: leftTwo,
                child: GestureDetector(
                  onHorizontalDragUpdate: (e) {
                    _secondProgress += (_max - _min) * e.delta.dx / width;
                    if (_secondProgress <= _min) {
                      _secondProgress = _min;
                      _progress = _min;
                    } else if (_secondProgress >= _max) {
                      _secondProgress = _max;
                      if (_secondProgress < _progress) {
                        _progress = _max;
                      }
                    } else if (_secondProgress < _progress) {
                      _progress = _secondProgress;
                    }
                    widget.onChange?.call(
                        _format(widget.numberFormat, _progress),
                        _format(widget.numberFormat, _secondProgress));
                    setState(() {});
                  },
                  onHorizontalDragEnd: (e) {
                    widget.onChangeEnd?.call(
                        _format(widget.numberFormat, _progress),
                        _format(widget.numberFormat, _secondProgress));
                  },
                  child: widget.endThumbIcon ??
                      KqImage.assets(
                        url: Images.commonIcRemoveFillDisabled,
                        package: KqPadGlobal.packageName,
                        width: 18.dm,
                        height: 18.dm,
                      ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  double _format(NumberFormat? format, double value) {
    return format == null ? value : double.parse(format.format(value));
  }
}
