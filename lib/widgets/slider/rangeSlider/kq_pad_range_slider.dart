import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/widgets/slider/rangeSlider/kq_pad_range_slider_bar.dart';

class KqPadRangeSlider extends StatefulWidget {
  /// 最小值
  final double min;

  /// 最大值
  final double max;

  final double start;

  final double end;

  /// 未选中的进度条颜色
  final Color? inactiveColor;

  /// 选中范围的进度条颜色
  final Color? activeColor;

  ///背景进度颜色
  final Color? bgColor;

  /// 进度条高度
  final double? progressHeight;

  /// 滑块大小
  final double? thumSize;

  final double? height;

  /// 是否显示值
  final bool showProgressValue;

  final Widget? startThumbIcon;
  final Widget? endThumbIcon;

  /// 滑块下方文字大小
  final double? fontSize;

  /// 文本偏移量
  final double? textOffset;

  /// 文本颜色
  final Color? textColor;
  final void Function(double start, double end)? onChange;

  final void Function(double start, double end)? onChangeEnd;

  /// 第一个滑块的进度
  final String? Function(double progress)? startTextFormat;

  /// 第二个滑块的进度
  final String? Function(double progress)? endTextFormat;
  const KqPadRangeSlider(
      {this.max = 100.0,
      this.min = 0.0,
      this.start = 0.0,
      this.end = 100.0,
      this.progressHeight,
      this.showProgressValue = true,
      this.height,
      this.onChange,
      this.onChangeEnd,
      this.inactiveColor,
      this.activeColor,
      this.bgColor,
      this.fontSize,
      this.textOffset,
      this.textColor,
      this.startThumbIcon,
      this.endThumbIcon,
      this.thumSize,
      this.startTextFormat,
      this.endTextFormat,
      super.key});

  @override
  State<KqPadRangeSlider> createState() => _KqPadRangeSliderState();
}

class _KqPadRangeSliderState extends State<KqPadRangeSlider> {
  double startProgress = 0;

  double endProgress = 0;

  @override
  void initState() {
    super.initState();
    startProgress = widget.start;
    endProgress = widget.end;
  }

  update() {
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.showProgressValue
        ? Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _renderSliderBar(),
              showText(),
            ],
          )
        : _renderSliderBar();
  }

  Widget showText() {
    return LayoutBuilder(builder: (context, constraints) {
      double max = widget.max;
      double min = widget.min;
      double width = constraints.maxWidth;
      double padding = widget.thumSize ?? 18.dm;
      double progressWidth = width - padding;
      String textOne = widget.startTextFormat?.call(startProgress) ??
          startProgress.round().toString();
      double textOneWidth = paintWidthWithTextStyle(textOne, width);
      double leftOne = progressWidth * (startProgress - min) / (max - min) -
          (textOneWidth / 2) +
          padding / 2;
      String? textTwo = widget.endTextFormat?.call(endProgress) ??
          endProgress.round().toString();
      double? textTwoWidth = paintWidthWithTextStyle(textTwo, width);
      double? leftTwo = progressWidth * (endProgress - min) / (max - min) -
          (textTwoWidth / 2) +
          padding / 2;
      double height = math.max(
          paintHeightWithTextStyle(textOne), paintHeightWithTextStyle(textTwo));
      return SizedBox(
        height: height,
        width: width,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Positioned(
              top: 0,
              left: leftOne,
              child: Text(
                textOne,
                style: TextStyle(
                  color: widget.textColor ?? KqPadThemeColors.text26,
                  fontSize: widget.fontSize ?? 8.dm,
                ),
              ),
            ),
            Positioned(
                top: 0,
                left: leftTwo,
                child: Text(
                  textTwo,
                  style: TextStyle(
                    color: widget.textColor ?? KqPadThemeColors.text26,
                    fontSize: widget.fontSize ?? 8.dm,
                  ),
                )),
          ],
        ),
      );
    });
  }

  Widget _renderSliderBar() {
    return KqPadRangeSliderBar(
      onChange: (start, end) {
        startProgress = start;
        endProgress = end;
        widget.onChange?.call(start, end);
        update();
      },
      onChangeEnd: (start, end) {
        startProgress = start;
        endProgress = end;
        widget.onChangeEnd?.call(start, end);
        update();
      },
      start: startProgress,
      end: endProgress,
      max: widget.max,
      min: widget.min,
      bgColor: widget.bgColor ?? KqPadThemeColors.bgTransparent,
      inactiveColor: widget.inactiveColor ?? KqPadThemeColors.bgTransparent,
      activeColor: widget.activeColor ?? KqPadThemeColors.db46,
      thumSize: widget.thumSize ?? 18.dm,
      startThumbIcon: KqImage.assets(
        url: Images.commonIcHuakuai28,
        package: KqPadGlobal.packageName,
        width: widget.thumSize ?? 18.dm,
        height: widget.thumSize ?? 18.dm,
        fit: BoxFit.fill,
      ),
      endThumbIcon: KqImage.assets(
        url: Images.commonIcHuakuai28,
        package: KqPadGlobal.packageName,
        width: widget.thumSize ?? 18.dm,
        height: widget.thumSize ?? 18.dm,
        fit: BoxFit.fill,
      ),
      height: widget.height ?? 18.dm,
      progressHeight: widget.progressHeight ?? 7.dm,
    );
  }

  /// 计算内容宽度
  double paintWidthWithTextStyle(String text, double maxWidth) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(
            text: text,
            style: TextStyle(
              fontSize: widget.fontSize ?? 8.dm,
            )),
        textDirection: TextDirection.ltr,
        maxLines: 1)
      ..layout(minWidth: 0, maxWidth: maxWidth);
    return textPainter.size.width;
  }

  /// 计算内容高度
  double paintHeightWithTextStyle(String text) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(
            text: text,
            style: TextStyle(
              fontSize: widget.fontSize ?? 8.dm,
            )),
        textDirection: TextDirection.ltr,
        maxLines: 1)
      ..layout();
    return textPainter.size.height;
  }

  @override
  void didUpdateWidget(covariant KqPadRangeSlider oldWidget) {
    super.didUpdateWidget(oldWidget);
    startProgress = widget.start;
    endProgress = widget.end;
  }
}
