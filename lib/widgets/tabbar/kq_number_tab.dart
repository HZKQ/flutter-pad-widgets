import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';

/// 带数字角标的Tab
class KqPadNumberTab extends Tab {
  final double? top;

  /// 角标控件
  final Widget Function()? numberBuilder;

  const KqPadNumberTab({Key? key, this.top, this.numberBuilder, text, child, icon})
      : super(key: key, text: text, icon: icon);

  @override
  Widget build(BuildContext context) {
    final double calculatedHeight;
    final Widget label;
    if (icon == null) {
      calculatedHeight = 18.dm;
      label = _buildLabelText();
    } else if (text == null && child == null) {
      calculatedHeight = 18.dm;
      label = icon!;
    } else {
      calculatedHeight = 30.dm;
      label = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: iconMargin,
            child: icon,
          ),
          _buildLabelText(),
        ],
      );
    }
    if (numberBuilder == null) {
      return SizedBox(
        height: height ?? calculatedHeight,
        child: Center(
          widthFactor: 1.0,
          child: label,
        ),
      );
    }
    return SizedBox(
      height: height ?? calculatedHeight,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Center(
            widthFactor: 1.0,
            child: label,
          ),
          Positioned(
            right: -6.dm,
            top: top ?? 0,
            child: numberBuilder!(),
          )
        ],
      ),
    );
  }

  Widget _buildLabelText() {
    return child ?? Text(text!, textAlign: TextAlign.center, softWrap: false, overflow: TextOverflow.fade);
  }
}
