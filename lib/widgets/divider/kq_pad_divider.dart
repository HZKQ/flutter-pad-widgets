import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';

import '../../resources/kq_pad_theme_colors.dart';
import '../../theme/kq_theme_manager.dart';

/// 通用水平分割线
///
/// @author 周卓
///
class KqPadDivider extends StatelessWidget {
  /// 分割线高度
  final double? height;

  /// 分割线宽度
  final double width;

  /// 分割线颜色
  final Color? color;

  /// 左边margin
  final double leftMargin;

  /// 右侧margin
  final double rightMargin;

  /// 是否显示阴影
  final bool showShadow;

  final List<KqBoxShadow>? shadow;

  const KqPadDivider({
    Key? key,
    this.width = double.infinity,
    this.height,
    this.color,
    this.leftMargin = 0,
    this.rightMargin = 0,
    this.showShadow = false,
    this.shadow,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: leftMargin, right: rightMargin),
      width: double.infinity,
      height: height ?? 0.5,
      decoration: KqBoxDecoration(
        color: color ??
            KqPadThemeManager.getCommonConfig().lineColor ??
            const Color(0xFFD6D8E1),
        boxShadow: showShadow
            ? shadow ??
                [
                  const KqBoxShadow(
                    color: Color(0x80FFFFFF),
                    offset: Offset(0, 1),
                    blurRadius: 0,
                    spreadRadius: 0,
                  ),
                ]
            : null,
      ),
    );
  }

  /// 返回一个不会被覆盖阴影的通用分割线
  static Widget normal({double? width, double? height, bool showShadow = true}) {
    return Container(
      height: height ?? 1.5,
      width: width,
      alignment: Alignment.topCenter,
      child: KqPadDivider(
        showShadow: showShadow,
      ),
    );
  }
}

/// 垂直分割线
class KqPadVerticalDivider extends StatelessWidget {
  /// 宽度
  final double? width;

  /// 高度
  final double height;

  /// 分割线颜色
  final Color? color;

  /// 左边margin
  final double topMargin;

  /// 右侧margin
  final double bottomMargin;

  /// 是否显示阴影
  final bool showShadow;

  final List<KqBoxShadow>? shadow;

  const KqPadVerticalDivider(
      {Key? key,
      this.width,
      this.height = double.infinity,
      this.color,
      this.topMargin = 0,
      this.showShadow = false,
      this.shadow,
      this.bottomMargin = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 0.5,
      height: height,
      decoration: BoxDecoration(
        color: color ??
            KqPadThemeManager.getCommonConfig().lineColor ??
            const Color(0xFFD6D8E1),
        boxShadow: showShadow
            ? shadow ??
                [
                  const KqBoxShadow(
                    color: Color(0x80FFFFFF),
                    offset: Offset(1, 0),
                    blurRadius: 0,
                    spreadRadius: 0,
                  ),
                ]
            : null,
      ),
      margin: EdgeInsets.only(top: topMargin, bottom: bottomMargin),
    );
  }

  /// 返回一个不会被覆盖阴影的通用分割线
  static Widget normal({double? height, double? width, bool showShadow = true}) {
    return Container(
      width: width ?? 1.5,
      height: height,
      alignment: Alignment.centerLeft,
      child: KqPadVerticalDivider(
        showShadow: showShadow,
      ),
    );
  }
}
