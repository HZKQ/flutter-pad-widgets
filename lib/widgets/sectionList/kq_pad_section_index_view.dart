import 'package:flutter/cupertino.dart';
import 'package:kq_flutter_core_widget/utils/indexer/indexer.dart';
import 'package:kq_flutter_core_widget/utils/indexer/indexer_util.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';

import '../../resources/kq_pad_theme_colors.dart';
import 'kq_pad_section_list_view.dart';

///基于KqSectionListView，主要是快速实现索引和分区
class KqPadSectionIndexView<T extends Indexer> extends StatefulWidget {
  ///cell的对应UI
  final Widget Function(int section, int row, T object) itemBuilder;

  /// 数据，需要继承[Indexer],需要实现[getFullName]方法，返回需要排序索引的字段
  final List<T> data;

  /// 设置索引为大写或者小写，默认小写
  final LatterType type;

  /// 是否需要数字索引，默认不需要，不需要如果遇到数字会排在最后面的[#]分类
  final bool isIndexNumber;

  final ScrollPhysics? physics;

  ///行高是否相同默认false
  final bool isSameHeight;

  ///行高isSameHeight=true时设置才会被使用，默认为36
  final double Function(int section)? rowHeight;

  /// 是否在搜索状态
  final bool Function()? isSearchCallback;

  const KqPadSectionIndexView({
    super.key,
    required this.data,
    required this.itemBuilder,
    this.isSameHeight = false,
    this.rowHeight,
    this.type = LatterType.lowerCase,
    this.isIndexNumber = false,
    this.physics,
    this.isSearchCallback,
  });

  @override
  State<StatefulWidget> createState() => _KqPadSectionIndexViewState<T>();
}

class _KqPadSectionIndexViewState<T extends Indexer>
    extends State<KqPadSectionIndexView<T>> {
  Map<String, List<T>> sectionData = {};
  List<String> indexData = [];

  @override
  void initState() {
    super.initState();
    sectionData = IndexerUtil.getSectionData(
        data: widget.data,
        type: widget.type,
        isIndexNumber: widget.isIndexNumber);
    indexData = IndexerUtil.getIndexData(
        data: widget.data,
        type: widget.type,
        isIndexNumber: widget.isIndexNumber);
  }

  @override
  void didUpdateWidget(covariant KqPadSectionIndexView<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    sectionData = IndexerUtil.getSectionData(
        data: widget.data,
        type: widget.type,
        isIndexNumber: widget.isIndexNumber);
    indexData = IndexerUtil.getIndexData(
        data: widget.data,
        type: widget.type,
        isIndexNumber: widget.isIndexNumber);
  }

  @override
  Widget build(BuildContext context) {
    return KqPadSectionListView<T>(
      section: sectionData.length,
      isShowIndex: true,
      isSameHeight: widget.isSameHeight,
      rowHeight: widget.rowHeight,
      isFixTop: true,
      indexList: indexData,
      isSearchCallback: widget.isSearchCallback,
      physics: widget.physics,
      headerBuilder: ((section) {
        return Container(
          height: 16.dm,
          color: KqPadThemeColors.bgHeader,
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(left: 8.dm),
          child: Text(indexData[section],
              style: TextStyle(fontSize: 7.dm, color: KqPadThemeColors.text26)),
        );
      }),
      itemList: ((section) {
        List<T>? model = sectionData[indexData[section]];
        return model ?? [];
      }),
      itemBuilder: widget.itemBuilder,
    );
  }
}
