import 'package:flutter/material.dart';
import 'package:kq_flutter_pad_widgets/widgets/sectionList/kq_pad_grid_list_view.dart';

class KqPadSectionGridView<E> extends StatefulWidget {
  ///分区数
  final int section;

  ///列数
  final int columns;

  ///对应section分区内的内容数组
  final List<E> Function(int section) itemList;

  ///cell的对应UI
  final Widget Function(int section, int row, E object) itemBuilder;

  ///对应列的宽度，不传则该列自动填充剩余区域
  final double? Function(int section, int column)? columnWidth;

  ///列表头部UI
  final Widget Function()? gridHeaderBuilder;

  ///列表尾部UI
  final Widget Function()? gridFooterBuilder;

  ///分组头部UI
  final Widget Function(int section)? sectionHeaderBuilder;

  ///分组尾部UI
  final Widget Function(int section)? sectionFooterBuilder;

  final bool shrinkWrap;

  final ScrollPhysics? physics;

  const KqPadSectionGridView(
      {super.key,
      required this.section,
      required this.columns,
      required this.itemList,
      this.columnWidth,
      this.gridHeaderBuilder,
      this.gridFooterBuilder,
      this.sectionHeaderBuilder,
      this.sectionFooterBuilder,
      required this.itemBuilder,
      this.shrinkWrap = false,
      this.physics});

  @override
  State<KqPadSectionGridView> createState() => _KqPadSectionGridViewState<E>();
}

class _KqPadSectionGridViewState<E> extends State<KqPadSectionGridView<E>> {
  List<Widget> slivers = [];

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }

  Widget _buildBody() {
    return CustomScrollView(
      shrinkWrap: widget.shrinkWrap,
      physics: widget.physics ?? const AlwaysScrollableScrollPhysics(),
      slivers: getSlivers(),
    );
  }

  ///listView头部区域
  Widget _tableViewHeaderView(Widget child) {
    return SliverToBoxAdapter(
      child: child,
    );
  }

  ///listView头部区域
  Widget _tableViewFooterView(Widget child) {
    return SliverToBoxAdapter(
      child: child,
    );
  }

  ///Section头部区域布局
  Widget _buildSectionHeader({required int section, required Widget child}) {
    //后期处理section
    return SliverToBoxAdapter(
      child: child,
    );
  }

  ///Section尾部区域布局
  Widget _buildSectionFooter({required int section, required Widget child}) {
    //后期处理section
    return SliverToBoxAdapter(
      child: child,
    );
  }

  ///分区样式
  Widget _buildSliverList({required int section, required List<List<E>> list}) {
    List<Widget> axisList = [];
    for (var i = 0; i < list.length; i++) {
      if (widget.columnWidth == null ||
          widget.columnWidth!(section, i) == null) {
        axisList.add(SliverCrossAxisExpanded(
            flex: 1,
            sliver: SliverPadChildList(
                count: list[i].length,
                section: section,
                itemList: list[i],
                child: (section, row, object) {
                  return widget.itemBuilder.call(section,
                      widget.columns * row + (i % widget.columns), object);
                })));
      } else {
        axisList.add(SliverConstrainedCrossAxis(
          maxExtent: widget.columnWidth!(section, i)!,
          sliver: SliverPadChildList(
              count: list[i].length,
              section: section,
              itemList: list[i],
              child: (section, row, object) {
                return widget.itemBuilder.call(section,
                    widget.columns * row + (i % widget.columns), object);
              }),
        ));
      }
    }
    return SliverCrossAxisGroup(
      slivers: axisList,
    );
  }

  List<Widget> getSlivers() {
    slivers = [];
    if (widget.gridHeaderBuilder != null) {
      slivers.add(_tableViewHeaderView(widget.gridHeaderBuilder!()));
    }
    for (int i = 0; i < widget.section; i++) {
      List<E> rowList = widget.itemList(i);
      if (widget.sectionHeaderBuilder != null) {
        slivers.add(_buildSectionHeader(
          section: i,
          child: widget.sectionHeaderBuilder!(i),
        ));
      }
      List<List<E>> allList = [];

      for (int n = 0; n < widget.columns; n++) {
        List<E> columnList = [];
        for (int j = 0; j < rowList.length; j++) {
          if (n + (widget.columns * j) < rowList.length) {
            columnList.add(rowList[n + (widget.columns * j)]);
          }
        }
        allList.add(columnList);
      }
      slivers.add(_buildSliverList(section: i, list: allList));
      if (widget.sectionFooterBuilder != null) {
        slivers.add(_buildSectionFooter(
          section: i,
          child: widget.sectionFooterBuilder!(i),
        ));
      }
    }
    if (widget.gridFooterBuilder != null) {
      slivers.add(_tableViewFooterView(widget.gridFooterBuilder!()));
    }
    return slivers;
  }
}
