class KqPadDateLineEntity {
  ///标题
  String? title;

  ///事件
  String? event;

  KqPadDateLineEntity({
    this.title,
    this.event,
  });
}
