import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/ex/list_ex.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'entity/kq_pad_date_line_entity.dart';

class KqPadDateLine extends StatelessWidget {
  /// 时间轴数据
  final List<KqPadDateLineEntity> data;

  /// 内容显示布局
  final Function(int index, KqPadDateLineEntity entity)? buildEventWidget;

  final ScrollPhysics? physics;

  const KqPadDateLine({
    super.key,
    this.data = const [],
    this.buildEventWidget,
    this.physics,
  });

  @override
  Widget build(BuildContext context) {
    return data.isNullOrEmpty
        ? Container()
        : ListView.builder(
            physics: physics,
            itemCount: data.length,
            itemBuilder: (context, index1) {
              return IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          if (index1 > 0)
                            Container(
                                width: 1.dm,
                                height: 6.dm,
                                color: KqPadThemeColors.textLightBlue),
                          if (index1 > 0) SizedBox(height: 2.dm),
                          KqImage.assets(
                            url: Images.commonIcBuzhou,
                            package: KqPadGlobal.packageName,
                            width: 8.dm,
                            height: 8.dm,
                          ),
                          SizedBox(height: 2.dm),
                          Expanded(
                            child: Container(
                              width: 1.dm,
                              color: index1 < data.length - 1
                                  ? KqPadThemeColors.textLightBlue
                                  : Colors.transparent,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 6.dm),
                        padding: EdgeInsets.only(bottom: 6.dm),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              data[index1].title ?? "",
                              style: TextStyle(
                                color: KqPadThemeColors.text26,
                                fontSize: 7.dm,
                              ),
                            ),
                            buildEventWidget?.call(index1, data[index1]) ??
                                Container(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
  }
}
