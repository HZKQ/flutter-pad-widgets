import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_pad_widgets/theme/kq_theme_manager.dart';
import '../../resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';

/// 颜色类型
enum PadColorType {
  /// 填充绿色
  fillGreen,

  /// 填充白色
  fillWhite,

  /// 边框白
  lineWhite,

  /// 主题色
  fillMainColor
}

class KqPadTabView extends StatefulWidget {
  const KqPadTabView(
      {super.key,
      required this.titles,
      required this.colorTypes,
      this.fontSize,
      this.disables,
      this.distance,
      this.padding,
      this.bgColor = KqPadThemeColors.bgWhite,
      this.showBoxShadow = true,
      this.tabIndexBlock});
  final List<String>? titles;
  final List<PadColorType> colorTypes;
  //是否允许点击 默认：允许点击
  final List<bool>? disables;
  final Color? bgColor;
  final double? fontSize;

  final EdgeInsetsGeometry? padding;
  final bool showBoxShadow;

  ///间距
  final double? distance;
  final Function(int index, bool disable)? tabIndexBlock;
  @override
  State<KqPadTabView> createState() => _KqPadTabViewState();
}

class _KqPadTabViewState extends State<KqPadTabView> {
  //底部横线平分的
  Widget renderAverage(String title, int index) {
    Map<String, dynamic> colors =
        PadColorConfig.getSpecificColor(widget.colorTypes[index]);
    Color txtColor = colors[PadColorConfig.txtKey];
    Color bgColor = colors[PadColorConfig.bgKey];
    Color borderColor = colors[PadColorConfig.borderKey];
    bool disable = false;
    if (widget.disables != null && widget.disables!.length > index) {
      disable = widget.disables![index];
    }
    return Opacity(
      opacity: disable ? 0.45 : 1,
      child: KqInkWell(
        onTap: (() {
          if (widget.tabIndexBlock != null) {
            widget.tabIndexBlock!(index, disable);
          }
        }),
        backgroundColor: bgColor,
        borderWidth: 1.0,
        borderColor: borderColor,
        child: Container(
          height: 28.dm,
          alignment: Alignment.center,
          decoration: widget.showBoxShadow
              ? BoxDecoration(
                  color: bgColor,
                  boxShadow: [
                    BoxShadow(
                      color: KqPadThemeColors.shadowColor3,
                      offset: const Offset(0, 0),
                      blurRadius: 10.dm,
                      spreadRadius: -4.dm,
                    ),
                  ],
                )
              : null,
          child: Text(
            title,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style:
                TextStyle(fontSize: widget.fontSize ?? 8.dm, color: txtColor),
          ),
        ),
      ),
    );
  }

  List<Widget> renderList() {
    List<Widget> views = [];
    for (var i = 0; i < widget.titles!.length; i++) {
      views.add(Expanded(child: renderAverage(widget.titles![i], i)));
      if (widget.titles!.length != 1 && i != widget.titles!.length - 1) {
        views.add(SizedBox(
          width: widget.distance ?? 0,
        ));
      }
    }
    return views;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.bgColor,
      padding: widget.padding,
      child: Row(
        children: renderList(),
      ),
    );
  }
}

class PadColorConfig {
  static const String txtKey = 'txtKey';
  static const String borderKey = 'borderKey';
  static const String bgKey = 'bgKey';

  //组合颜色根据key
  static Map<String, dynamic> getSpecificColor(PadColorType type) {
    if (type == PadColorType.fillWhite) {
      return getFillWhite();
    } else if (type == PadColorType.lineWhite) {
      return getLineWhite();
    } else if (type == PadColorType.fillMainColor) {
      return getFillMainColor();
    }
    return getFillGreen();
  }

  //绿色填充
  static Map<String, dynamic> getFillGreen() {
    return {
      bgKey: KqPadThemeColors.db46,
      txtKey: KqPadThemeColors.textWhite,
      borderKey: KqPadThemeColors.bgTransparent
    };
  }

  //白色填充
  static Map<String, dynamic> getFillWhite() {
    return {
      bgKey: KqPadThemeColors.bgWhite,
      txtKey: KqPadThemeColors.text26,
      borderKey: KqPadThemeColors.bgTransparent
    };
  }

  //边框白色
  static Map<String, dynamic> getLineWhite() {
    return {
      bgKey: KqPadThemeColors.bgTransparent,
      txtKey: KqPadThemeColors.bgWhite,
      borderKey: KqPadThemeColors.bgWhite
    };
  }

  //主题色填充
  static Map<String, dynamic> getFillMainColor() {
    return {
      bgKey: KqPadThemeManager.instance.getConfig().commonConfig.mainColor,
      txtKey: KqPadThemeColors.textWhite,
      borderKey: KqPadThemeColors.bgTransparent
    };
  }
}
