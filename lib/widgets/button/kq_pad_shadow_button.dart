import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';

/// 带阴影按钮
class KqPadShadowButton extends StatelessWidget {
  /// 是否选中，选中时没有阴影，不选中时有阴影
  final bool selected;

  /// 中间的文本
  final String centerText;

  /// 中间文本的对齐方式，默认居左
  final TextAlign? textAlign;

  /// 完全自定义按钮左侧View，传了自动忽略[leftIcon]
  final Widget? leftWidget;

  /// 完全自定义按钮右侧View，传了自动忽略[rightIcon]
  final Widget? rightWidget;

  /// 按钮左侧图标，使用这个则默认左侧是Image，选中时会自动将Image变成白色
  final String? leftIcon;

  /// 按钮右侧View，使用这个则默认右侧是Image，选中时会自动将Image变成白色
  final String? rightIcon;

  /// 点击回调
  final VoidCallback? onTap;

  /// 水平padding，默认6.dm
  final double? paddingHorizontal;

  /// 垂直padding，默认11.dm
  final double? paddingVertical;

  const KqPadShadowButton(
      {Key? key,
      this.leftWidget,
      required this.selected,
      required this.centerText,
      this.leftIcon,
      this.rightIcon,
      this.rightWidget,
      this.textAlign,
      this.paddingHorizontal,
      this.paddingVertical,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KqInkWell(
      onTap: onTap,
      enableRipple: false,
      radius: 2.dm,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 100),
        padding: EdgeInsets.symmetric(
            horizontal: paddingHorizontal ?? 6.dm,
            vertical: paddingVertical ?? 11.dm),
        decoration: BoxDecoration(
          color: selected
              ? KqPadThemeColors.textLightBlue
              : KqPadThemeColors.bgWhite,
          borderRadius: BorderRadius.circular(2.dm),
          boxShadow: selected
              ? [
                  BoxShadow(
                    color: const Color(0xff0A84FF).withOpacity(0.5),
                    blurRadius: 5.dm,
                    spreadRadius: -1.dm,
                    offset: Offset(0, 1.dm),
                  ),
                ]
              : [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.25),
                    blurRadius: 4.dm,
                    spreadRadius: -2.dm,
                    offset: Offset(0, 1.dm),
                  ),
                ],
        ),
        child: Row(
          children: [
            if (leftWidget == null && leftIcon != null)
              KqImage.assets(
                width: 7.dm,
                height: 7.dm,
                url: leftIcon!,
                color: selected ? Colors.white : null,
              ),
            if (leftWidget != null) leftWidget!,
            if (leftWidget != null || leftIcon != null)
              SizedBox(
                width: 4.dm,
              ),
            Expanded(
                child: Text(
              centerText,
              textAlign: textAlign ?? TextAlign.start,
              style: TextStyle(
                  fontSize: 7.dm,
                  color: selected
                      ? KqPadThemeColors.textWhite
                      : KqPadThemeColors.text26),
            )),
            if (rightWidget != null || rightIcon != null)
              SizedBox(
                width: 4.dm,
              ),
            if (rightWidget == null && rightIcon != null)
              KqImage.assets(
                width: 7.dm,
                height: 7.dm,
                url: rightIcon!,
                color: selected ? Colors.white : null,
              ),
            if (rightWidget != null) rightWidget!,
          ],
        ),
      ),
    );
  }
}
