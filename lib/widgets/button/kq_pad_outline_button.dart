import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/utils/kq_multi_click_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';

import '../../resources/kq_pad_theme_colors.dart';
import '../../theme/kq_theme_manager.dart';

/// 列表通用操作栏按钮
///
/// @author 周卓
///
class KqPadOutlineButton extends StatelessWidget {
  /// 标题
  final String title;

  /// 边框颜色，默认跟随主题亮色
  final Color? borderColor;

  /// 文字颜色，默认跟随主题亮色
  final Color? textColor;

  /// 背景颜色，默认透明
  final Color? bgColor;

  /// 字体大小，默认8
  final double? fontSize;

  /// 圆角大小，默认1
  final double? radius;

  /// 边框宽度，默认1
  final double? borderWidth;

  /// 水平边距，默认6
  final double? horizontalPadding;

  /// 高度
  final double? height;

  /// 是否置灰
  final bool disabled;

  /// 左侧icon
  final Widget? leftIcon;

  /// 点击事件
  final Function()? onTap;

  const KqPadOutlineButton(
      {Key? key,
      required this.title,
      this.borderColor,
      this.textColor,
      this.bgColor,
      this.radius,
      this.height,
      this.borderWidth,
      this.fontSize,
      this.disabled = false,
      this.horizontalPadding,
      this.onTap,
      this.leftIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var borColor = borderColor ??
        (KqPadThemeManager.instance.getConfig().commonConfig.mainLightColor ??
            KqPadThemeColors.bgHtGreen);
    var txtColor = textColor ??
        (KqPadThemeManager.instance.getConfig().commonConfig.mainLightColor ??
            KqPadThemeColors.bgHtGreen);
    return KqInkWell(
        onTap: () {
          if (KqMultiClickUtil.isMultiClick()) {
            return;
          }
          onTap?.call();
        },
        child: Container(
          height: height ?? 16.dm,
          padding: EdgeInsets.symmetric(horizontal: horizontalPadding ?? 6.dm),
          decoration: BoxDecoration(
            color: bgColor,
            border: Border.all(color: borColor, width: borderWidth ?? 1),
            borderRadius: BorderRadius.circular(radius ?? 1.dm),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (leftIcon != null) leftIcon!,
              if (leftIcon != null)
                SizedBox(
                  width: 3.dm,
                ),
              Text(
                title,
                style: TextStyle(
                  color: txtColor,
                  fontSize: fontSize ?? 8.dm,
                ),
              ),
            ],
          ),
        ));
  }
}
