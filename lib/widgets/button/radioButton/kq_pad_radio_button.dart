import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';

enum ShowType {
  // 滚动显示
  scrollingDisplay,
  // 换行显示
  lineBreakDisplay,
}

class KqPadRadioButton<T extends IRadioEntity> extends StatefulWidget {
  ///排列方向，默认水平排列
  ///
  ///水平方向[RadioDirection.horizontal]
  ///
  ///垂直方向[RadioDirection.vertical]
  final RadioDirection direction;

  /// 数据，需继承[IRadioEntity]
  final List<T>? radioEntity;

  /// 点击回调
  ///
  /// 回调的下标[index]
  final Future<bool> Function(int index)? onChanged;

  ///文本宽度，不传代表自适应，传则代表固定指定宽度
  final double? textFixWidth;

  ///默认选择的index。
  ///可配合[needUpdateDefaultIndex]字段让[defaultIndex]每次都生效。
  final int defaultIndex;

  ///是否需要改变[defaultIndex]。
  ///如果设置成ture，则每次传入的[defaultIndex]都会生效。
  final bool needUpdateDefaultIndex;

  /// 标签间是否显示箭头
  final bool isShowArrow;

  /// 显示样式
  final ShowType showType;

  const KqPadRadioButton(
      {super.key,
      this.direction = RadioDirection.horizontal,
      this.radioEntity,
      this.onChanged,
      this.textFixWidth,
      this.defaultIndex = 0,
      this.needUpdateDefaultIndex = false,
      this.isShowArrow = false,
      this.showType = ShowType.scrollingDisplay});

  @override
  State<StatefulWidget> createState() => _KqPadRadioButtonState<T>();
}

class _KqPadRadioButtonState<T extends IRadioEntity>
    extends State<KqPadRadioButton<T>> {
  int _selectedValue = 0;

  @override
  void initState() {
    _selectedValue = widget.defaultIndex;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant KqPadRadioButton<T> oldWidget) {
    if (widget.needUpdateDefaultIndex) {
      _selectedValue = widget.defaultIndex;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.showType == ShowType.scrollingDisplay) {
      if (widget.direction == RadioDirection.horizontal) {
        return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: getList(context),
          ),
        );
      } else {
        return SingleChildScrollView(
          child: Column(
            children: getList(context),
          ),
        );
      }
    } else {
      return Wrap(
        runSpacing: 6.dm,
        spacing: 8.dm,
        children: getList(context),
      );
    }
  }

  List<Widget> getList(BuildContext context) {
    List<Widget> list = [];
    if (widget.radioEntity != null && widget.radioEntity!.isNotEmpty) {
      if (widget.showType == ShowType.scrollingDisplay) {
        for (int i = 0; i < widget.radioEntity!.length; i++) {
          T t = widget.radioEntity![i];
          list.add(
            KqInkWell(
              onTap: () async {
                if (t.isDelete != null && t.isDelete!) {
                } else {
                  if (widget.onChanged != null) {
                    if (await widget.onChanged!.call(i)) {
                      _selectedValue = i;
                      setState(() {});
                    }
                  } else {
                    _selectedValue = i;
                    setState(() {});
                  }
                }
              },
              child: Container(
                height: 16.dm,
                width: widget.textFixWidth,
                padding: widget.textFixWidth == null
                    ? EdgeInsets.symmetric(horizontal: 8.dm)
                    : null,
                alignment: Alignment.center,
                decoration: KqBoxDecoration(
                  gradient: (t.isDelete != null && t.isDelete!)
                      ? null
                      : _selectedValue == i
                          ? null
                          : const LinearGradient(
                              colors: [Color(0xFFE6E7ED), Color(0xFFF7F8FA)],
                              stops: [0, 1],
                            ),
                  color: (t.isDelete != null && t.isDelete!)
                      ? KqPadThemeColors.bgHeader
                      : _selectedValue == i
                          ? KqPadThemeColors.bgHtGreen
                          : null,
                  borderRadius: BorderRadius.circular(10.dm),
                  boxShadow: [
                    KqBoxShadow(
                      color: const Color(0xFFB1B5C6),
                      offset: Offset(0.5.dm, 0.5.dm),
                      blurRadius: 3.dm,
                      spreadRadius: 0,
                    ),
                    KqBoxShadow(
                      color: const Color(0xFFFAFAFC),
                      offset: Offset(-0.5.dm, -0.5.dm),
                      blurRadius: 4.dm,
                      spreadRadius: 0,
                    ),
                    KqBoxShadow(
                      color: const Color(0xFFEBECF0),
                      offset: Offset(0.5.dm, 0.5.dm),
                      blurRadius: 2.dm,
                      spreadRadius: 0,
                      inset: true,
                    ),
                  ],
                ),
                child: Text(
                  t.title ?? "",
                  style: (t.isDelete != null && t.isDelete!)
                      ? TextStyle(
                          fontSize: 8.dm,
                          color: KqPadThemeColors.text7326,
                          overflow: TextOverflow.ellipsis,
                          decoration: TextDecoration.lineThrough,
                          decorationColor: KqPadThemeColors.text7326, // 删除线的颜色
                          decorationStyle: TextDecorationStyle.solid, // 删除线的样式
                          decorationThickness: 2.0, // 删除线的粗细
                        )
                      : TextStyle(
                          fontSize: 8.dm,
                          color: _selectedValue == i
                              ? KqPadThemeColors.padLightBg
                              : KqPadThemeColors.text26,
                          overflow: TextOverflow.ellipsis),
                ),
              ),
            ),
          );
          if (i < widget.radioEntity!.length - 1) {
            list.add(SizedBox(
              width: 6.dm,
            ));
          }
        }
      } else {
        for (int i = 0; i < widget.radioEntity!.length; i++) {
          T t = widget.radioEntity![i];
          Size size = handleTxtWidth(t.title ?? '');
          list.add(KqInkWell(
            onTap: () async {
              if (t.isDelete != null && t.isDelete!) {
              } else {
                if (widget.onChanged != null) {
                  if (await widget.onChanged!.call(i)) {
                    _selectedValue = i;
                    setState(() {});
                  }
                } else {
                  _selectedValue = i;
                  setState(() {});
                }
              }
            },
            child: Container(
              height: 16.dm,
              width: size.width + 16.dm,
              alignment: Alignment.center,
              decoration: KqBoxDecoration(
                gradient: (t.isDelete != null && t.isDelete!)
                    ? null
                    : _selectedValue == i
                        ? null
                        : const LinearGradient(
                            colors: [Color(0xFFE6E7ED), Color(0xFFF7F8FA)],
                            stops: [0, 1],
                          ),
                color: (t.isDelete != null && t.isDelete!)
                    ? KqPadThemeColors.bgHeader
                    : _selectedValue == i
                        ? KqPadThemeColors.bgHtGreen
                        : null,
                borderRadius: BorderRadius.circular(10.dm),
                boxShadow: [
                  KqBoxShadow(
                    color: const Color(0xFFB1B5C6),
                    offset: Offset(0.5.dm, 0.5.dm),
                    blurRadius: 3.dm,
                    spreadRadius: 0,
                  ),
                  KqBoxShadow(
                    color: const Color(0xFFFAFAFC),
                    offset: Offset(-0.5.dm, -0.5.dm),
                    blurRadius: 4.dm,
                    spreadRadius: 0,
                  ),
                  KqBoxShadow(
                    color: const Color(0xFFEBECF0),
                    offset: Offset(0.5.dm, 0.5.dm),
                    blurRadius: 2.dm,
                    spreadRadius: 0,
                    inset: true,
                  ),
                ],
              ),
              child: Text(
                t.title ?? "",
                style: (t.isDelete != null && t.isDelete!)
                    ? TextStyle(
                        fontSize: 8.dm,
                        color: KqPadThemeColors.text7326,
                        overflow: TextOverflow.ellipsis,
                        decoration: TextDecoration.lineThrough,
                        decorationColor: KqPadThemeColors.text7326, // 删除线的颜色
                        decorationStyle: TextDecorationStyle.solid, // 删除线的样式
                        decorationThickness: 2.0, // 删除线的粗细
                      )
                    : TextStyle(
                        fontSize: 8.dm,
                        color: _selectedValue == i
                            ? KqPadThemeColors.padLightBg
                            : KqPadThemeColors.text26,
                        overflow: TextOverflow.ellipsis),
              ),
            ),
          ));
        }
      }
    }
    return list;
  }

  //计算宽度
  Size handleTxtWidth(String txt) {
    TextPainter painter = TextPainter(
        locale: Localizations.localeOf(context),
        maxLines: 1,
        textDirection: TextDirection.ltr,
        text: TextSpan(text: txt, style: TextStyle(fontSize: 14.sp)))
      ..layout(
        maxWidth: double.infinity,
      );
    return painter.size;
  }
}

enum RadioDirection { vertical, horizontal }

class IRadioEntity {
  final String? title;
  bool? isDelete;

  IRadioEntity(this.title, {this.isDelete});
}
