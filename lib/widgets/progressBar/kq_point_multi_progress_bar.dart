
import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';

/// 多进度点状进度条
/// 支持点高亮，显示文字。
class KqPointMultiProgressBar extends StatefulWidget {
  /// 最大进度，默认100
  final double max;

  /// 背景条高度，默认3.dm
  final double? barHeight;

  /// 渐变颜色值，从左到右
  final List<Color> gradientColors;

  /// 高亮点的颜色，默认[KqPadThemeColors.db46]
  final Color? highlightPointColor;

  /// 高亮点的高度
  final double? highlightPointHeight;

  /// 数据
  final List<KqPointMultiProgressData> data;

  const KqPointMultiProgressBar({
    Key? key,
    this.max = 100,
    this.barHeight,
    this.highlightPointColor,
    this.highlightPointHeight,
    required this.data,
    required this.gradientColors,
  }) : super(key: key);

  @override
  KqPointMultiProgressBarState createState() => KqPointMultiProgressBarState();
}

class KqPointMultiProgressBarState extends State<KqPointMultiProgressBar> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var widgetWidth = constraints.maxWidth;
        return Stack(
          clipBehavior: Clip.none,
          children: [
            gradientBg(),
            ...progressPointsAndTexts(widgetWidth),
          ],
        );
      },
    );
  }

  List<Widget> progressPointsAndTexts(double widgetWidth) {
    List<Widget> points = [];
    for (var entity in widget.data) {
      var values = _calculateLeft(entity, widgetWidth);
      var pointLeft = values[0];
      var textLeft = values[1];
      points.add(progressPoint(entity, pointLeft));
      points.add(progressText(entity, textLeft));
    }
    return points;
  }

  Widget progressPoint(KqPointMultiProgressData entity, double left) {
    double size = entity.highlight
        ? (widget.highlightPointHeight ?? 6.dm)
        : (widget.barHeight ?? 3.dm);
    double top = entity.highlight
        ? 0
        : (((widget.highlightPointHeight ?? 6.dm)) -
                (widget.barHeight ?? 3.dm)) /
            2;
    return Positioned(
        left: left -
            (entity.highlight
                ? (widget.highlightPointHeight ?? 6.dm) / 2
                : (widget.barHeight ?? 3.dm) / 2),
        top: top,
        child: Container(
          decoration: BoxDecoration(
            color: entity.highlight
                ? (widget.highlightPointColor ?? KqPadThemeColors.db46)
                : (entity.progressColor ?? KqPadThemeColors.bgWhite),
            borderRadius: BorderRadius.circular(100.dm),
          ),
          width: size,
          height: size,
        ));
  }

  Widget progressText(KqPointMultiProgressData entity, double left) {
    return Positioned(
        left: left,
        bottom: 0,
        child: Text(
          entity.highlightText ?? '',
          style: TextStyle(fontSize: 10.dm, color: KqPadThemeColors.text26),
        ));
  }

  Widget gradientBg() {
    var barHeight = widget.barHeight ?? 3.dm;
    var pointSize = widget.highlightPointHeight ?? 6.dm;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Expanded(
                child: Container(
              height: barHeight,
              margin: EdgeInsets.symmetric(
                vertical: (pointSize - barHeight) / 2,
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100.dm),
                  gradient: LinearGradient(
                    colors: widget.gradientColors,
                  )),
            )),
          ],
        ),
        // 占位用的文字
        Text(
          '80',
          style:
              TextStyle(fontSize: 10.dm, color: KqPadThemeColors.bgTransparent),
        ),
      ],
    );
  }

  List<double> _calculateLeft(
      KqPointMultiProgressData entity, double widgetWidth) {
    var centerWidth = widgetWidth;
    var x = entity.progress * 1.0 / (widget.max) * (centerWidth);
    double textWidth = entity.highlightText == null
        ? 0
        : measureTextWidth(entity.highlightText!,
            textStyle: TextStyle(
              fontSize: 10.dm,
            ));
    var textX = x - textWidth / 2;
    return [x, textX];
  }

  /// 画笔，用于测量
  TextPainter painter = TextPainter(textDirection: TextDirection.ltr);

  /// 获取文本宽度
  double measureTextWidth(String text, {TextStyle? textStyle}) {
    painter.text = TextSpan(
      text: text,
      style: textStyle,
    );

    painter.layout();
    return painter.width;
  }
}

class KqPointMultiProgressData {
  /// 进度值
  final double progress;

  /// 是否高亮
  final bool highlight;

  /// 进度值颜色，默认白色
  final Color? progressColor;

  /// 高亮的文字
  final String? highlightText;

  KqPointMultiProgressData({
    this.progress = 0,
    this.highlight = false,
    this.progressColor,
    this.highlightText,
  });
}
