import 'base/pad_base_chart.dart';

/// 图表
///
/// 目前提供以下代理：
///
/// [KqLineChartDelegate]
///
/// [KqBarChartDelegate]
///
/// [KqHorizontalBarChartDelegate]
///
/// [KqCombinedChartDelegate]
class KqPadChart extends PadBaseChart {
  const KqPadChart({super.key, super.padding, required super.delegate});
}
