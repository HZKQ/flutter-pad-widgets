import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/widgets/gesture/kq_gesture_detector.dart';
import 'package:kq_flutter_pad_widgets/resources/l10n/kq_pad_intl.dart';

abstract class PadBaseChartDelegate<D> {
  PadBaseChartDelegate(
      {this.animDuration,
      this.data,
      this.gestureHandler,
      this.emptyWidgetBuilder,
      this.isDataEmpty,
      this.canScroll = true,
      this.scrollWidth = 800});

  /// 动画时间，设置此值则启用动画绘制
  final Duration? animDuration;

  final D? data;

  final bool Function(D? data)? isDataEmpty;

  /// 当[isEmptyData]返回true时显示的内容
  final Widget Function()? emptyWidgetBuilder;

  final PadBaseChartGestureHandlerMixin? gestureHandler;

  final bool canScroll;

  final double scrollWidth;

  PadChartStateMixin? _chartState;

  /// 此代理关联的[State]
  PadChartStateMixin? get chartState => _chartState;

  Size _size = Size.zero;

  /// X轴图表组件占用大小
  Size get size => _size;

  // Y轴图表组件占用大小
  Size _ySize = Size.zero;
  Size get ySize => _ySize;

  bool get isEmptyData;

  void onDraw(Canvas canvas, double animProgress, bool isDrawX, bool isDrawY,
      bool canScroll);

  /// 关联上Chart，此时[State.initState]已执行
  @mustCallSuper
  void attachChart(PadChartStateMixin state) {
    _chartState = state;
    gestureHandler?.attachChart(this);
  }

  @mustCallSuper
  void didUpdateWidget(covariant PadBaseChart oldWidget) {}

  @mustCallSuper
  double calculateYAxisSize(covariant PadBaseChartDelegate delegate) {
    return 0;
  }

  /// 界面销毁
  @mustCallSuper
  void dispose() {
    _chartState = null;
    gestureHandler?.dispose();
  }

  /// 刷新界面数据
  void update() {
    _chartState?.update();
  }
}

class _PadChartGestureHandlerWrapper<C extends PadBaseChartDelegate> {
  PadBaseChartDelegate? _delegate;
  DragDownDetails? _dragDownDetails;
  bool _isMove = false;

  void attachChart(C delegate) {
    _delegate?.gestureHandler?.attachChart(delegate);
  }

  void dispose() {
    _delegate?.gestureHandler?.dispose();
  }

  void onTapDown(DragDownDetails details) {
    _isMove = false;
    var padding = _delegate?.chartState?.widget.padding;
    if (padding == null) {
      DragDownDetails detail = details;
      if (_delegate != null &&
          _delegate!.canScroll &&
          _delegate!.chartState != null) {
        detail = DragDownDetails(
            globalPosition: details.globalPosition,
            localPosition: details.localPosition == details.globalPosition
                ? details.localPosition
                : details.localPosition.translate(
                    _delegate!.chartState!.currentOffset -
                        _delegate!._ySize.width,
                    0));
      }
      _dragDownDetails = detail;
      _delegate?.gestureHandler?.onTapDown(detail);
    } else {
      var wrapperDetails = DragDownDetails(
          globalPosition: details.globalPosition,
          localPosition: details.localPosition == details.globalPosition
              ? details.localPosition
              : details.localPosition.translate(-padding.left, -padding.top));
      _dragDownDetails = wrapperDetails;
      _delegate?.gestureHandler?.onTapDown(wrapperDetails);
    }
  }

  void onTapUp() {
    _delegate?.gestureHandler?.onTapUp(_dragDownDetails ?? DragDownDetails());
  }

  void onDragEnd(DragEndDetails details) {
    if (!_isMove) {
      onTapUp();
      return;
    }

    _delegate?.gestureHandler?.onDragEnd(details);
  }

  void onDragStart(DragStartDetails details) {
    var padding = _delegate?.chartState?.widget.padding;
    if (padding == null) {
      _delegate?.gestureHandler?.onDragStart(details);
    } else {
      var wrapperDetails = DragStartDetails(
          sourceTimeStamp: details.sourceTimeStamp,
          globalPosition: details.globalPosition,
          localPosition: details.localPosition == details.globalPosition
              ? details.localPosition
              : details.localPosition.translate(-padding.left, -padding.top));
      _delegate?.gestureHandler?.onDragStart(wrapperDetails);
    }
  }

  void onDragUpdate(DragUpdateDetails details) {
    if (details.delta.distanceSquared <= 0) {
      return;
    }

    _isMove = true;
    var padding = _delegate?.chartState?.widget.padding;
    if (padding == null) {
      _delegate?.gestureHandler?.onDragUpdate(details);
    } else {
      var wrapperDetails = DragUpdateDetails(
          sourceTimeStamp: details.sourceTimeStamp,
          delta: details.delta,
          primaryDelta: details.primaryDelta,
          globalPosition: details.globalPosition,
          localPosition: details.localPosition == details.globalPosition
              ? details.localPosition
              : details.localPosition.translate(-padding.left, -padding.top));
      _delegate?.gestureHandler?.onDragUpdate(wrapperDetails);
    }
  }
}

/// 手势事件处理
mixin PadBaseChartGestureHandlerMixin<C extends PadBaseChartDelegate> {
  /// 是否可点击
  bool get tapEnable;

  /// 是否可滑动、拖拽,旋转等发生位移的手势
  bool get dragEnable;

  void attachChart(C delegate);

  void dispose();

  /// 手指按下，此事件发生在手指首次触摸屏幕
  void onTapDown(DragDownDetails details);

  /// 手指抬起，此事件发生在手指首次触摸屏幕后未发生滑动后抬起
  void onTapUp(DragDownDetails details);

  /// 拖拽开始，此事件发生在手指按下首次滑动
  void onDragStart(DragStartDetails details);

  /// 拖拽更新，此事件发生在手指持续滑动过程
  void onDragUpdate(DragUpdateDetails details);

  /// 拖拽结束，此事件发生在手指持续滑动后抬起
  void onDragEnd(DragEndDetails details);
}

/// 图表
class PadBaseChart extends StatefulWidget {
  const PadBaseChart({super.key, this.padding, required this.delegate});

  final PadBaseChartDelegate delegate;
  final EdgeInsets? padding;

  @override
  State<StatefulWidget> createState() => KqPadChartState();
}

/// 图表关联的[State]对外暴露内容
mixin PadChartStateMixin {
  /// 更新数据
  update();

  /// 获取state关联的Widget
  PadBaseChart get widget;

  /// 用于创建动画
  TickerProvider get vsync;

  /// 用于创建动画
  double get currentOffset;
}

/// 科强表格[State]基类
class KqPadChartState extends State<PadBaseChart>
    with TickerProviderStateMixin, PadChartStateMixin {
  /// 控制绘制动画
  AnimationController? _animationController;
  final _PadChartGestureHandlerWrapper _gestureHandlerWrapper =
      _PadChartGestureHandlerWrapper();
  Duration? _animDuration;
  final ScrollController _scrollController = ScrollController();

  // 用于存储当前的滚动偏移量
  double currentOffsetX = 0.0;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(vsync: this);
    _animDuration = widget.delegate.animDuration;
    _gestureHandlerWrapper._delegate = widget.delegate;
    widget.delegate.attachChart(this);
    widget.delegate._ySize = Size(
        widget.delegate.calculateYAxisSize(widget.delegate),
        widget.delegate._ySize.height);

    _scrollController.addListener(() {
      // 获取当前的滚动偏移量
      currentOffsetX = _scrollController.offset;
    });
  }

  @override
  void dispose() {
    _animationController?.dispose();
    widget.delegate.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant PadBaseChart oldWidget) {
    super.didUpdateWidget(oldWidget);
    _animDuration = widget.delegate.animDuration;
    widget.delegate.attachChart(this);
    _gestureHandlerWrapper._delegate = widget.delegate;
    widget.delegate.didUpdateWidget(oldWidget);
    widget.delegate._ySize = Size(
        widget.delegate.calculateYAxisSize(oldWidget.delegate),
        widget.delegate._ySize.height);
  }

  @override
  Widget build(BuildContext context) {
    _animationController?.stop(canceled: true);
    var builder = widget.delegate.emptyWidgetBuilder;
    if (widget.delegate.isEmptyData && builder != null) {
      return _mapChild(builder.call());
    }

    var gestureHandler = widget.delegate.gestureHandler;
    if (gestureHandler == null ||
        (!gestureHandler.tapEnable && !gestureHandler.dragEnable)) {
      return _mapChild(SizedBox.expand(child: _buildContent(context)));
    }

    if (gestureHandler.tapEnable && !gestureHandler.dragEnable) {
      return GestureDetector(
        onTapDown: (details) {
          _gestureHandlerWrapper.onTapDown(DragDownDetails(
              globalPosition: details.globalPosition,
              localPosition: details.localPosition));
        },
        onTapUp: (details) {
          _gestureHandlerWrapper.onTapUp();
        },
        child: _mapChild(SizedBox.expand(child: _buildContent(context))),
      );
    } else {
      // 当前图表如果被包裹在滑动组件中，如果未触发滑动且手指抬起，则直接触发onDragCancel。
      // 当前图表如果未包裹在滑动列表中，如果未触发滑动且手指抬起，则触发onDragEnd。
      return KqGestureDetector(
        onDragDown: _gestureHandlerWrapper.onTapDown,
        onDragStart: gestureHandler.dragEnable
            ? _gestureHandlerWrapper.onDragStart
            : null,
        onDragUpdate: gestureHandler.dragEnable
            ? _gestureHandlerWrapper.onDragUpdate
            : null,
        onDragEnd:
            gestureHandler.dragEnable ? _gestureHandlerWrapper.onDragEnd : null,
        onDragCancel:
            gestureHandler.tapEnable ? _gestureHandlerWrapper.onTapUp : null,
        child: _mapChild(SizedBox.expand(child: _buildContent(context))),
      );
    }
  }

  Widget _mapChild(Widget child) {
    if (widget.padding != null) {
      return Padding(padding: widget.padding!, child: child);
    }
    return child;
  }

  Widget _buildContent(BuildContext context) {
    return _animDuration == null || widget.delegate.isEmptyData
        ? buildChild(context, 1)
        : _withAnim(_animDuration!);
  }

  Widget _withAnim(Duration duration) {
    _animationController?.duration = duration;
    var widget = AnimatedBuilder(
      animation: _animationController!,
      builder: (context, child) {
        return buildChild(context, _animationController?.value ?? 1);
      },
    );
    _animationController?.forward(from: 0);
    return widget;
  }

  @protected
  Widget buildChild(BuildContext context, double animProgress) {
    if (!widget.delegate.canScroll) {
      return CustomPaint(
          painter: _PadChartPainter(state: this, animProgress: animProgress));
    }
    return LayoutBuilder(
      builder: (context, constraints) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            if (!widget.delegate.canScroll)
              Expanded(
                  child: CustomPaint(
                      painter: _PadChartPainter(
                          state: this, animProgress: animProgress))),
            Expanded(
                child: Stack(
              children: [
                /// 画Y轴部分
                Positioned(
                    left: widget.delegate._ySize.width,
                    child: SizedBox(
                      width: constraints.maxWidth,
                      height: constraints.maxHeight,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        controller: _scrollController,
                        physics: const ClampingScrollPhysics(),
                        child: SizedBox(
                          width: widget.delegate.scrollWidth,
                          height: constraints.maxHeight,
                          // color: Colors.blue,
                          child: CustomPaint(
                              painter: _PadChartPainter(
                                  state: this, animProgress: animProgress)),
                        ),
                      ),
                    )),
                SizedBox(
                  width: widget.delegate._ySize.width,
                  height: constraints.maxHeight,
                  // color: Colors.white,
                  child: CustomPaint(
                      painter: _PadYAxisChartPainter(
                          state: this, animProgress: animProgress)),
                ),
              ],
            ))
          ],
        );
      },
    );
  }

  @override
  void update() {
    setState(() {
      _animDuration = null;
    });
  }

  @override
  TickerProvider get vsync => this;

  @override
  double get currentOffset => currentOffsetX;
}

/// 绘制Y轴图表器
class _PadYAxisChartPainter extends CustomPainter {
  const _PadYAxisChartPainter({required this.state, this.animProgress = 1})
      : assert(animProgress >= 0 && animProgress <= 1);

  /// 绘制器所依赖的饼状图[KqBasePieChartState]
  final KqPadChartState state;

  /// 动画进度，用于根据动画进行进行绘制
  final double animProgress;
  @override
  void paint(Canvas canvas, Size size) {
    state.widget.delegate._ySize = size;
    if (size.isEmpty) {
      return;
    }
    state.widget.delegate.onDraw(
        canvas, animProgress, false, true, state.widget.delegate.canScroll);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

/// 图表绘制器
class _PadChartPainter extends CustomPainter {
  const _PadChartPainter({required this.state, this.animProgress = 1})
      : assert(animProgress >= 0 && animProgress <= 1);

  /// 绘制器所依赖的饼状图[KqBasePieChartState]
  final KqPadChartState state;

  /// 动画进度，用于根据动画进行进行绘制
  final double animProgress;

  @override
  void paint(Canvas canvas, Size size) {
    state.widget.delegate._size = size;
    if (size.isEmpty) {
      return;
    }

    state.widget.delegate.onDraw(
        canvas,
        animProgress,
        true,
        state.widget.delegate.canScroll ? false : true,
        state.widget.delegate.canScroll);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

/// 获取默认的空数据界面
Widget getDefEmptyView({double fontSize = 12, Color color = Colors.black}) =>
    Align(
      alignment: Alignment.center,
      child: Text(
        KqPadIntl.currentResource.noDataTip,
        style: TextStyle(fontSize: fontSize, color: color),
      ),
    );
