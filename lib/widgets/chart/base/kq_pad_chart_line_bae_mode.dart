import 'dart:ui';

class KqPadChartLineBaseModel {
  ///线颜色数组与chartLineArray一一对应，
  List<Color>? lineColors;

  ///线颜色数组与chartRightArray一一对应，
  List<Color>? rightLineColors;

  ///遮罩颜色数组与chartLineArray一一对应，
  List<List<Color>>? shaderColors;

  ///遮罩颜色数组与chartLineArray一一对应，
  List<List<Color>>? rightShaderColors;

  KqPadChartLineBaseModel(
      {this.lineColors,
      this.rightLineColors,
      this.shaderColors,
      this.rightShaderColors});

  ///实际高度
  double yHeight = 0;

  ///实际宽度
  double xWidth = 0;

  int maxCross = 0;

  ///用于充当分母的最大值
  double maxHeight = 0;

  ///当存在两条纵坐标时候用于充当右纵坐标的分母
  double maxRightHeight = 0;

  ///平均线坐标数组
  List<double> avgYPointList = [];

  ///横坐标数组
  List<double> xList = [];

  ///横坐标内容
  List xValue = [];

  ///纵坐标数组
  List yList = [];

  ///纵坐标内容，存在左右纵坐标轴时为左边的纵坐标
  List yValue = [];

  ///右纵坐标内容
  List yRightValue = [];

  ///横坐标间距
  double crossSpacing = 0;

  ///横坐标显示内容个数不为0时，使用传入需要显示内容点的对应下标
  List<int> crossShowList = [];
}
