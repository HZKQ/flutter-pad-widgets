import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';

import '../kq_pda_chart_line_model.dart';

class KqPadChartModel {
  ///横坐标文字颜色
  Color? xColor;

  ///纵坐标文字颜色
  Color? yColor;

  ///横坐标文字大小,默认6
  double? xFont;

  ///纵坐标文字大小,默认6
  double? yFont;

  ///x距离左边距距离,默认40
  double? xLeft;

  ///横坐标起始点,默认0
  double initialPointX;

  ///横坐标终端距离画布边距的距离，默认0
  double endPointXWidth;

  ///x距离右边距距离,默认8
  double? xRight;

  ///y距离Y轴顶部距距离,默认16
  double? yTop;

  ///Y轴顶部距离上边距的距离,默认若不存在单位且isAvgValue=false时默认为0，否则默认10
  double? yAxisTop;

  ///y距离下边距距离，默认30
  double? yBottom;

  ///y轴右边纵坐标数组
  List<double>? rightYAxisList;

  ///y轴左边纵轴表数组
  List<double>? leftYAxisList;

  ///坐标线颜色
  Color? axisColor;

  ///是否显示分割线
  bool isDivide;

  ///分割线颜色
  Color? dividerColor;

  ///分割线是否为实现，默认为true
  bool hintLineSolid;

  ///单位,不传代表不显示
  String? unit;

  ///单位样式
  TextStyle? unitStyle;

  ///右边纵坐标单位，不传代表不显示
  String? rightUnit;

  ///单位样式
  TextStyle? rightUnitStyle;

  ///是否显示平均线
  bool isAvg;

  ///是否显示平均线的值
  bool isAvgValue;

  ///平均线颜色与折线柱状数组一一对应,若不传则默认为线的颜色
  List<Color>? avgColors;

  ///平均线数值与折线柱状数组一一对应,若不传则内部自己计算
  List<String>? avgValues;

  ///平均线是否是实线，默认false
  bool hintAvgLineSolid;

  ///纵坐标最大值，默认100
  double maxY;

  ///纵个数，默认为6
  int rowCount;

  ///横坐标显示内容个数，不传则为内容的个数，传了则固定为几个，若没传crossShowList则间隔数平均
  int crossCount;

  ///横坐标显示内容个数不为0时，使用传入需要显示内容点的对应下标
  List<int>? crossShowList;

  ///是否显示全部的内容文案，默认为false,设为true时isShowClickValue，isShowSlideValue将均无效
  bool isShowValue;

  ///点击时是否显示对应的文案，默认为false
  bool isShowClickValue;

  /// 折线上的点的点击事件
  Function(
          bool isLeft, int lineIndex, int pointIndex, KqPdaChartLineValue item)?
      onLineChartPointTap;

  ///滑动经过对应点时是否显示对应点的文案，默认为false
  bool isShowSlideValue;

  ///画布区域高
  final double sizeHeight;

  ///画布区域宽
  final double sizeWidth;

  ///是否固定最大高度,
  bool isFixMaxY;

  ///动画起始，默认为0,
  double animationTime;

  ///动画时间,默认为300毫秒
  Duration duration;

  ///动画是否结束
  bool isAnimationEnd = false;

  ///是否增加覆盖层默认false
  bool isAddMask;

  ///是否曲线绘制默认为false
  bool isCurve;

  ///是否显示内容坐标点,默认为false
  bool isShowPoint;

  ///内容坐标点的半径
  double? pointRadius;

  ///x轴文案倾斜的角度
  double? angle;

  ///Y轴文案距离Y轴的距离
  double? yLabelDistance;

  ///右边Y轴文案距离右边Y轴的距离
  double? yRightLabelDistance;

  ///X轴文案距离X轴的距离
  double? xLabelDistance;

  ///倾斜时x轴文案的偏移
  double? xAngleDistance;

  KqPadChartModel({
    this.angle,
    this.leftYAxisList,
    this.rightYAxisList,
    this.xColor,
    this.xFont,
    this.yColor,
    this.yFont,
    this.xLeft,
    this.unit,
    this.rightUnit,
    this.unitStyle,
    this.rightUnitStyle,
    this.xRight,
    this.yTop,
    this.yBottom,
    this.isDivide = true,
    this.hintLineSolid = true,
    this.axisColor,
    this.dividerColor,
    this.avgColors,
    this.avgValues,
    this.yAxisTop,
    this.maxY = 100,
    this.rowCount = 6,
    this.crossCount = 0,
    this.isAvgValue = false,
    this.isAvg = false,
    this.crossShowList,
    this.duration = const Duration(milliseconds: 300),
    required this.isFixMaxY,
    required this.sizeHeight,
    required this.sizeWidth,
    this.isShowValue = false,
    this.isShowSlideValue = false,
    this.isShowClickValue = false,
    this.initialPointX = 0,
    this.animationTime = 0,
    this.endPointXWidth = 0,
    this.isAddMask = false,
    this.isCurve = false,
    this.pointRadius,
    this.isShowPoint = false,
    this.hintAvgLineSolid = false,
    this.onLineChartPointTap,
    this.yRightLabelDistance,
    this.yLabelDistance,
    this.xLabelDistance,
    this.xAngleDistance,
  });

  void initData() {
    xFont = xFont ?? 6.dm;
    yFont = yFont ?? 6.dm;
    yTop = yTop ?? 16.dm;
    yBottom = yBottom ?? 30.dm;
    xLeft = xLeft ?? 40.dm;
    xRight = xRight ?? 8.dm;
    yAxisTop = yAxisTop ?? ((unit == null && isAvgValue == false) ? 0 : 10.dm);
    pointRadius = pointRadius ?? 2.dm;
    avgValues = avgValues ?? [];
  }
}
