import 'package:flutter/material.dart';
import 'package:kq_flutter_pad_widgets/widgets/coordMap/kq_pad_coord_map_painter.dart';
import 'package:kq_flutter_pad_widgets/widgets/coordMap/kq_pad_coord_map_entity.dart';

class KqPadCoordMap extends StatefulWidget {
  final double height;

  /// 选中的位置
  final int? selectedIndex;

  /// 选中的折线图颜色
  final Color? lineSelectedColor;

  /// 默认的折线图颜色
  final Color? lineNormalColor;

  final List<KqPadCoordMapEntity> entitys;

  /// 绘制刻度文本时，外部自定义处理
  final String Function(double text)? onDrawMarkText;

  const KqPadCoordMap(
      {required this.height,
      required this.entitys,
      this.selectedIndex,
      this.lineSelectedColor,
      this.lineNormalColor,
      this.onDrawMarkText,
      super.key});

  @override
  State<KqPadCoordMap> createState() => _KqPadCoordMapState();
}

class _KqPadCoordMapState extends State<KqPadCoordMap> {
  final GlobalKey _wrapKey = GlobalKey();
  double width = 300;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final RenderBox? superView =
          _wrapKey.currentContext!.findRenderObject() as RenderBox?;
      var size = superView?.size;
      setState(() {
        width = size?.width ?? 0;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      key: _wrapKey,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Container(
        //   height: width,
        //   color: KqPadThemeColors.bgBlack75,
        // ),
        CustomPaint(
          size: Size(width, widget.height),
          painter: KqPadCoordMapPainter(
              entitys: widget.entitys,
              selectedIndex: widget.selectedIndex,
              lineNormalColor: widget.lineNormalColor,
              lineSelectedColor: widget.lineSelectedColor,
              onDrawMarkText: widget.onDrawMarkText),
        )
      ],
    );
  }
}
