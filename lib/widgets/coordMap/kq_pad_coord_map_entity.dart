
class KqPadCoordMapEntity {
  String? typeName;
  String maxValue;
  String minValue;
  List<String> points;

  KqPadCoordMapEntity(
      {this.typeName,
      required this.maxValue,
      required this.minValue,
      required this.points});
}
