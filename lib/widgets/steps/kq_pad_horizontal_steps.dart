import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import '../../config/kq_pad_global.dart';
import '../../resources/kq_pad_theme_colors.dart';
import '../divider/kq_pad_divider.dart';

final double _kItemSidePadding = 4.dm;
final double _kItemFontSize = 8.dm;
final double _kIconSize = 8.dm;

/// 描述: 横向步骤条,是一种常见的导航形式，它具有导航通用的属性：告知用户”我在哪/我能去哪“，
/// 步骤数目就相当于告知用户--能去哪或者说流程将要经历什么。
/// 通用组件步骤条分为三个状态：完成态/进行态/等待态，三种状态在样式上均加以区分
/// 注意事项：横向步骤条内的步骤总数最多只支持5个
class KqPadHorizontalSteps extends StatefulWidget {
  /// The steps of the stepper whose titles, subtitles, icons always get shown.
  ///
  //当前这个板块的高
  final double? height;

  /// 控制类
  final KqPadStepsController? controller;

  /// 自定义正在进行状态的icon
  final Widget? doingIcon;

  /// 自定义已完成状态的icon
  final Widget? completedIcon;

  //点击了每个块
  final Function(int index, KqPadStep step)? callBack;

  const KqPadHorizontalSteps(
      {Key? key,
      this.height,
      required this.controller,
      this.doingIcon,
      this.completedIcon,
      this.callBack})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => KqPadHorizontalStepsState();
}

class KqPadHorizontalStepsState extends State<KqPadHorizontalSteps> {
  Color? get _primary {
    return KqPadThemeColors.db46;
  }

  int get _currentIndex {
    return widget.controller?.currentIndex ?? 0;
  }

  Color _getStepContentTextColor(int index) {
    if (_currentIndex == index) {
      return _primary!;
    }
    return KqPadThemeColors.text26;
  }

  void _handleStepStateListenerTick() {
    setState(() {});
  }

  void _initController() {
    widget.controller?.setMaxCount(widget.controller!.steps.length);
    widget.controller?.addListener(_handleStepStateListenerTick);
  }

  @override
  void initState() {
    super.initState();
    _initController();
  }

  @override
  void dispose() {
    widget.controller?.removeListener(_handleStepStateListenerTick);
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant KqPadHorizontalSteps oldWidget) {
    super.didUpdateWidget(oldWidget);
    final bool isControllerDiff = oldWidget.controller != null &&
        widget.controller != oldWidget.controller;
    final bool isCountDiff =
        widget.controller!.steps.length != oldWidget.controller!.steps.length;
    if (isControllerDiff || isCountDiff) {
      oldWidget.controller?.removeListener(_handleStepStateListenerTick);
      _initController();
    }
  }

  @override
  Widget build(BuildContext context) {
    /// 单独一个widget组件，用于返回需要生成的内容widget
    Widget content;
    final List<KqPadStep> steps = widget.controller!.steps;
    content = Container(
      height: widget.height ?? 30.dm,
      alignment: Alignment.center,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: IntrinsicHeight(
          child: Row(
            children: List<Widget>.generate(steps.length, (index) {
              return _applyStepItem(steps[index], index);
            }),
          ),
        ),
      ),
    );
    return content;
  }

  Widget _applyStepItem(KqPadStep step, int index) {
    return KqInkWell(
      enableRipple: widget.callBack != null,
      onTap: () {
        if (widget.callBack != null) {
          widget.callBack!(index, step);
        }
      },
      child: _applyStepAndLine(step, index),
    );
  }

  Widget _applyStepAndLine(KqPadStep step, int index) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        index == 0 ? const SizedBox.shrink() : _applyLineItem(index, true),
        _applyStepIcon(step, index),
        _applyStepContent(step, index),
        index == widget.controller!.steps.length - 1
            ? const SizedBox.shrink()
            : _applyLineItem(index, false),
      ],
    );
  }

  Widget _applyStepIcon(KqPadStep step, int index) {
    Widget icon;
    if (widget.controller?.isCompleted == true) {
      return _getCompletedIcon(step);
    }
    if (step.state != null) {
      switch (step.state) {
        case KqPadStepState.indexed:
          icon = _getIndexIcon(step, index);
          break;
        case KqPadStepState.complete:
          icon = _getCompletedIcon(step);
          break;
        case KqPadStepState.doing:
          icon = _getDoingIcon(step);
          break;
        default:
          icon = _getDoingIcon(step);
          break;
      }
    } else {
      if (index < _currentIndex) {
        // 当前index小于指定的活跃index
        icon = _getCompletedIcon(step);
      } else if (index == _currentIndex) {
        icon = _getDoingIcon(step);
      } else {
        icon = _getIndexIcon(step, index);
      }
    }
    return icon;
  }

  Widget _applyLineItem(int index, bool isLeft) {
    return Container(
        width: 32.dm,
        alignment: Alignment.center,
        child: KqPadDivider(
          height: 1,
          color: _getLineColor(index, isLeft),
          leftMargin: isLeft ? 0 : _kItemSidePadding,
          rightMargin: isLeft ? _kItemSidePadding : 0,
        ));
  }

  Color _getLineColor(int index, bool isLeft) {
    // if (index < _currentIndex) {
    //   return _primary!;
    // } else if (_currentIndex == index && isLeft) {
    //   return _primary!;
    // }
    return const Color(0xffD9D9D9);
  }

  Widget _getIndexIcon(KqPadStep step, int index) {
    Widget? indexedIcon = step.indexedIcon;
    if (indexedIcon != null) {
      return indexedIcon;
    }
    Widget icon = KqImage.assets(
      fit: BoxFit.cover,
      url: Images.commonIcRadio16UnselectedEnabled,
      package: KqPadGlobal.packageName,
      height: _kIconSize,
      width: _kIconSize,
    );
    return icon;
  }

  Widget _applyStepContent(KqPadStep step, int index) {
    return Container(
        margin: EdgeInsets.only(left: _kItemSidePadding),
        child: Text(
          step.stepContentText ?? '',
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontSize: _kItemFontSize,
            color: _getStepContentTextColor(index),
          ),
        ));
  }

  Widget _getCompletedIcon(KqPadStep step) {
    Widget? completedIcon = step.completedIcon;
    if (completedIcon != null) {
      /// 如果Step中自定义completedIcon不为空，则使用自定义的icon
      return completedIcon;
    }
    completedIcon = widget.completedIcon;
    if (completedIcon != null) {
      /// 如果自定义completedIcon不为空，则使用自定义的icon
      return completedIcon;
    }

    /// 使用组件默认的icon
    return KqImage.assets(
      url: Images.commonIcCheckboxes16SelectedEnabledLine,
      package: KqPadGlobal.packageName,
      width: _kIconSize,
      height: _kIconSize,
    );
  }

  Widget _getDoingIcon(KqPadStep step) {
    Widget? doingIcon = step.doingIcon;
    if (doingIcon != null) {
      /// 如果Step中自定义doingIcon不为空，则使用自定义的icon
      return doingIcon;
    }
    doingIcon = widget.doingIcon;
    if (doingIcon != null) {
      /// 如果自定义doingIcon不为空，则使用自定义的icon
      return doingIcon;
    }
    // 使用组件默认的icon
    return KqImage.assets(
      url: Images.commonIcRadio16SelectedEnabled,
      package: KqPadGlobal.packageName,
      width: _kIconSize,
      height: _kIconSize,
    );
  }
}

enum KqPadStepState {
  /// A step that displays its index in its circle.
  indexed,

  /// A step that displays a doing icon in its circle.
  doing,

  /// A step that displays a completed icon in its circle.
  complete
}

class KqPadStep {
  /// Creates a step for a [Stepper].
  ///
  /// The [stepContent], [doingIcon] arguments can be null.
  const KqPadStep({
    this.stepContent,
    this.indexedIcon,
    this.doingIcon,
    this.stepContentText,
    this.completedIcon,
    this.state,
    this.extraData,
  });

  /// The String title of the step that typically describes it.
  final String? stepContentText;

  /// The title of the step that typically describes it.
  final Widget? stepContent;

  ///  The completedIcon of the step
  final Widget? indexedIcon;

  /// The doingIcon of the step
  final Widget? doingIcon;

  /// The completedIcon of the step
  final Widget? completedIcon;

  /// The state of the step which determines the styling of its components
  /// and whether steps are interactive.
  final KqPadStepState? state;

  /// 自定义内容，备用
  final dynamic extraData;
}

class KqPadStepsController with ChangeNotifier {
  /// 指示当前进行态的步骤
  int currentIndex;

  /// 整个流程是否完成
  bool isCompleted;

  /// 最大个数（最多只支持5个）
  int _maxCount = 0;

  /// 步骤条中元素的列表
  late List<KqPadStep> steps;

  KqPadStepsController(
      {this.currentIndex = 0, this.isCompleted = false, required this.steps});

  /// 只有在当前包内调用，不开放给外部调用
  void setMaxCount(int _maxCount) {
    this._maxCount = _maxCount;
  }

  /// 设置当前步骤条的 index,从 0 开始。
  void setCurrentIndex(int currentIndex) {
    if (this.currentIndex == currentIndex || currentIndex > _maxCount) return;
    isCompleted = currentIndex == _maxCount;
    this.currentIndex = currentIndex;
    notifyListeners();
  }

  /// 整个链路完成
  void setCompleted() {
    setCurrentIndex(_maxCount);
  }

  /// 向前一步
  void forwardStep() {
    if (currentIndex < _maxCount) {
      setCurrentIndex(currentIndex + 1);
    }
  }

  /// 向后一步
  void backStep() {
    final int backIndex = currentIndex <= 0 ? 0 : currentIndex - 1;
    setCurrentIndex(backIndex);
  }

  //赋值数据
  void setUpdate(List<KqPadStep> kqSteps) {
    steps = kqSteps;
    setMaxCount(steps.length);
  }
}
