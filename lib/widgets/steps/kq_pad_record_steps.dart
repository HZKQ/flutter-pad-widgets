import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/image/kq_image.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/theme/kq_theme_manager.dart';
import '../../config/kq_pad_global.dart';
import '../../resources/kq_pad_theme_colors.dart';

final double _kItemSidePadding = 4.dm;
final double _kItemFontSize = 8.dm;

class KqPadRecordSteps<T extends KqPadBaseRecordEntity> extends StatefulWidget {
  /// 指示当前进行态的步骤
  final int? currentIndex;

  /// 步骤条中元素的列表
  final List<T> data;

  //点击了每个块
  final Function(T object, int index)? callBack;

  const KqPadRecordSteps(
      {Key? key, required this.data, this.currentIndex, this.callBack})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => KqPadRecordStepsState();
}

class KqPadRecordStepsState<T extends KqPadBaseRecordEntity>
    extends State<KqPadRecordSteps<T>> {
  int? _currentIndex;

  @override
  void initState() {
    super.initState();
    _currentIndex = widget.currentIndex;
  }

  @override
  void didUpdateWidget(covariant KqPadRecordSteps<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.currentIndex != oldWidget.currentIndex) {
      setState(() {
        _currentIndex = widget.currentIndex;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    /// 单独一个widget组件，用于返回需要生成的内容widget
    Widget content;
    final List<T> steps = widget.data;
    List<Widget> widgets = [];
    for (var i = 0; i < steps.length; i++) {
      T object = steps[i];
      if (i == 0) {
        widgets.add(const SizedBox.shrink());
      } else {
        widgets.add(_applyLine(i));
      }
      widgets.add(_applyContent(object, i));
    }
    content = Container(
      alignment: Alignment.centerLeft,
      child: IntrinsicWidth(
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: widgets,
        ),
      ),
    );
    return content;
  }

  Widget _applyLine(int index) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.dm),
      child: KqImage.assets(
        url: Images.commonIcJiantouStep,
        width: 9.dm,
        height: 4.dm,
        package: KqPadGlobal.packageName,
      ),
    );
  }

  Widget _applyContent(T object, int index) {
    return KqInkWell(
        // enableRipple: widget.callBack != null,
        onTap: () {
          //点击
          if (widget.callBack != null) {
            widget.callBack!(object, index);
          }
          setState(() {
            _currentIndex = index;
          });
        },
        child: Stack(
          children: [
            Container(
                padding: EdgeInsets.symmetric(
                    vertical: 4.dm,
                    horizontal: _currentIndex != null && _currentIndex == index
                        ? _kItemSidePadding
                        : 0),
                decoration: _getDecoration(index),
                child: Text(
                  '${index + 1}.${object.defectName ?? ''}',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: _kItemFontSize,
                    color: _getStepContentTextColor(object, index),
                  ),
                )),
            //步骤状态
            if (_currentIndex != null &&
                _currentIndex != index &&
                object.enabled != 1)
              Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  top: 0,
                  child: Center(
                    child: Container(
                      height: 0.5,
                      color: KqPadThemeColors.text59,
                    ),
                  ))
          ],
        ));
  }

  //文本的显示状态
  Decoration? _getDecoration(int index) {
    if (_currentIndex != null && _currentIndex == index) {
      //当前选中的
      return BoxDecoration(
        color: KqPadThemeManager.instance.getConfig().commonConfig.mainColor ??
            KqPadThemeColors.db46,
        border: Border.all(color: KqPadThemeColors.db46, width: 1),
        borderRadius: BorderRadius.circular(2.dm),
      );
    }
    //默认的 要不然点击的时候 存在闪退
    return BoxDecoration(
      color: Colors.transparent,
      border: Border.all(color: Colors.transparent, width: 1),
      borderRadius: BorderRadius.circular(2.dm),
    );
  }

  Color _getStepContentTextColor(T object, int index) {
    if (_currentIndex != null &&
        _currentIndex != index &&
        object.enabled != 1) {
      //作废的 打岔
      return KqPadThemeColors.text59;
    }
    if (_currentIndex == index) {
      return KqPadThemeColors.textWhite;
    }
    //默认的
    return KqPadThemeColors.text26;
  }
}

class KqPadBaseRecordEntity {
  /// 缺陷修复参数id
  int? fixedParametersId;

  /// 标签名称
  String? defectName;

  /// 0：未激活 1:有效
  int? enabled;

  KqPadBaseRecordEntity({
    this.fixedParametersId,
    this.defectName,
    this.enabled,
  });
}
