import 'package:flutter/material.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/utils/ex/kq_ex.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';

class KqPadVerticalSteps extends StatefulWidget {
  /// 总步骤数量
  final int totalStepNum;

  /// 标题
  final List<String>? titles;

  /// 标题
  final List<String>? contents;

  /// 标题
  final Widget Function(int index)? titleCallback;

  /// 内容
  final Widget Function(int index)? contentCallback;

  const KqPadVerticalSteps(
      {required this.totalStepNum,
      this.titles,
      this.contents,
      this.titleCallback,
      this.contentCallback,
      super.key});

  @override
  State<KqPadVerticalSteps> createState() => _KqPadVerticalStepsState();
}

class _KqPadVerticalStepsState extends State<KqPadVerticalSteps> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: _renderCell(),
    );
  }

  List<Widget> _renderCell() {
    List<Widget> list = [];

    for (var i = 0; i < widget.totalStepNum; i++) {
      String? title;
      if (widget.titles.isNotNullOrEmpty && widget.titles!.length > i) {
        title = widget.titles![i];
      }

      String? content;
      if (widget.contents.isNotNullOrEmpty && widget.contents!.length > i) {
        content = widget.contents![i];
      }
      list.add(Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            children: [
              Image.asset(
                Images.commonIcBuzhou,
                width: 8.dm,
                height: 8.dm,
                package: KqPadGlobal.packageName,
              ),
              SizedBox(width: 6.dm),
              if (title != null)
                Expanded(
                    child: Text(
                  title,
                  style:
                      TextStyle(fontSize: 7.dm, color: KqPadThemeColors.text26),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                )),
              if (title == null) _renderTitle(i)
            ],
          ),
          SizedBox(
            height: 2.dm,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 3.5.dm,
              ),
              Column(
                children: [
                  Visibility(
                    visible: content.isNotNullOrEmpty ||
                        widget.contentCallback != null,
                    child: SizedBox(
                      height: 2.dm,
                    ),
                  ),
                  Visibility(
                      visible: i != widget.totalStepNum - 1,
                      child: Container(
                        width: 1.dm,
                        height: 25.dm,
                        color: KqPadThemeColors.db46,
                      ))
                ],
              ),
              SizedBox(
                width: 9.5.dm,
              ),
              if (content != null)
                Expanded(
                    child: Text(
                  content,
                  style:
                      TextStyle(fontSize: 7.dm, color: KqPadThemeColors.text26),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                )),
              if (content == null) _renderContent(i)
            ],
          )
        ],
      ));
    }

    return list;
  }

  Widget _renderTitle(int index) {
    if (widget.titleCallback != null) {
      return Expanded(child: widget.titleCallback!(index));
    }
    return Container();
  }

  Widget _renderContent(int index) {
    if (widget.contentCallback != null) {
      return Expanded(child: widget.contentCallback!(index));
    }
    return Container();
  }
}
