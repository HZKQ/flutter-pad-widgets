import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';

import '../../resources/kq_pad_theme_colors.dart';


/// 数字角标
///
/// @author 周卓
///
class KqPadNumberTagView extends StatelessWidget {
  /// 数字值
  final int number;

  /// 最大值，默认0，0代表不限制，超过最大值则显示"xx+"
  final int max;

  /// number=0时是否自动变透明
  final bool hideWhenZero;

  /// 背景颜色，默认红色
  final Color backgroundColor;

  /// 文字颜色，默认白色
  final Color textColor;

  /// 边框颜色，默认无边框
  final Color borderColor;

  /// 边框宽度
  final double borderWidth;

  /// 最小宽高
  final double? minSize;

  /// 文字大小
  final double? fontSize;

  const KqPadNumberTagView(
      {Key? key,
      required this.number,
      this.max = 0,
      this.borderWidth = 0,
      this.borderColor = KqPadThemeColors.bgTransparent,
      this.hideWhenZero = true,
      this.backgroundColor = KqPadThemeColors.textRed,
      this.textColor = KqPadThemeColors.textWhite,
      this.minSize,
      this.fontSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String value;
    if (max > 0) {
      if (number > max) {
        value = '$max+';
      } else {
        value = number.toString();
      }
    } else {
      value = number.toString();
    }

    double? width;
    double? height;

    if (number.abs() < 10) {
      width = minSize ?? 24.dm;
      height = minSize ?? 24.dm;
    } else {
      height = minSize ?? 24.dm;
    }

    return Visibility(
      visible: !(hideWhenZero && number == 0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(height)),
            border: borderWidth == 0
                ? null
                : Border.all(
                    width: borderWidth,
                    color: borderColor == KqPadThemeColors.bgTransparent
                        ? backgroundColor
                        : borderColor),
            color: backgroundColor),
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 3.dm),
        width: width,
        height: height,
        child: Text(
          value,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: fontSize ?? 16.dm, color: textColor),
        ),
      ),
    );
  }
}
