import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/widgets/rangInput/kq_pad_range_input_entity.dart';
import 'package:kq_flutter_pad_widgets/widgets/textFiled/kq_pad_textfiled.dart';
import 'package:kq_flutter_core_widget/utils/text_input_format_utils.dart';

class KqPadRangeInput extends StatefulWidget {
  final KqPadRangeInputEntity entity;

  const KqPadRangeInput({required this.entity, super.key});

  @override
  State<KqPadRangeInput> createState() => _KqPadRangeInputState();
}

class _KqPadRangeInputState extends State<KqPadRangeInput> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      // 适配的最小宽度，小于这个宽度，等比缩放
      final fitMinWidth = 292.dm;
      bool isFit = constraints.maxWidth > fitMinWidth;
      double ratio = isFit ? 1 : (constraints.maxWidth / fitMinWidth);
      return Row(
        children: [
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                widget.entity.title,
                style: TextStyle(
                    fontSize: 8.dm * ratio, color: KqPadThemeColors.text26),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
          ),
          SizedBox(
            width: 8.dm * ratio,
          ),
          Container(
            height: 20.dm * ratio,
            width: 80.dm * ratio,
            decoration: KqBoxDecoration(
              color: KqPadThemeColors.bgWhite,
              borderRadius: BorderRadius.all(Radius.circular(1.dm * ratio)),
              boxShadow: [
                KqBoxShadow(
                    offset: Offset(0.5.dm * ratio, 0.5.dm * ratio),
                    blurRadius: 0,
                    spreadRadius: 0,
                    color: const Color(0x80FFFFFF)),
                KqBoxShadow(
                    offset: Offset(0, 0.5.dm * ratio),
                    blurRadius: 3.dm * ratio,
                    spreadRadius: 0,
                    inset: true,
                    color: const Color(0xFFB2B7CE)),
                KqBoxShadow(
                    offset: Offset(0.5.dm * ratio, 0.5.dm * ratio),
                    blurRadius: 0.5.dm * ratio,
                    spreadRadius: 0,
                    inset: true,
                    color: const Color(0xFFC9CCD9)),
              ],
            ),
            child: KqPadTextFiled(
              newValue: widget.entity.minValue ?? '',
              fontSize: 8.dm * ratio,
              inputType: TextInputType.number,
              textAlign: TextAlign.center,
              showShadow: false,
              inputFormatters: TextInputFormatUtils.decimal(
                  integerLength: 6, decimalLength: 4),
              onFocusChanged: (focus) {
                if (widget.entity.hasLeftFocus == true && !focus) {
                  widget.entity.hasLeftFocus = false;
                  widget.entity.onFocusChanged?.call(false);
                } else if (widget.entity.hasLeftFocus != true && focus) {
                  widget.entity.hasLeftFocus = true;
                  widget.entity.onFocusChanged?.call(true);
                }
              },
              onChanged: (value) {
                widget.entity.minValue = value;
                widget.entity.onSubmittedForMin?.call(widget.entity);
              },
            ),
          ),
          Container(
            width: 16.dm * ratio,
            height: 1,
            color: KqPadThemeColors.borderLine,
            margin: EdgeInsets.only(
              left: 4.dm * ratio,
              right: 4.dm * ratio,
            ),
          ),
          Container(
            height: 20.dm * ratio,
            width: 80.dm * ratio,
            decoration: KqBoxDecoration(
              color: KqPadThemeColors.bgWhite,
              borderRadius: BorderRadius.all(Radius.circular(1.dm * ratio)),
              boxShadow: [
                KqBoxShadow(
                    offset: Offset(0.5.dm * ratio, 0.5.dm * ratio),
                    blurRadius: 0,
                    spreadRadius: 0,
                    color: const Color(0x80FFFFFF)),
                KqBoxShadow(
                    offset: Offset(0, 0.5.dm * ratio),
                    blurRadius: 3.dm * ratio,
                    spreadRadius: 0,
                    inset: true,
                    color: const Color(0xFFB2B7CE)),
                KqBoxShadow(
                    offset: Offset(0.5.dm * ratio, 0.5.dm * ratio),
                    blurRadius: 0.5.dm * ratio,
                    spreadRadius: 0,
                    inset: true,
                    color: const Color(0xFFC9CCD9)),
              ],
            ),
            child: KqPadTextFiled(
              newValue: widget.entity.maxValue ?? '',
              fontSize: 8.dm * ratio,
              inputType: TextInputType.number,
              textAlign: TextAlign.center,
              showShadow: false,
              inputFormatters: TextInputFormatUtils.decimal(
                  integerLength: 6, decimalLength: 4),
              onFocusChanged: (focus) {
                if (widget.entity.hasRightFocus == true && !focus) {
                  widget.entity.hasRightFocus = false;
                  widget.entity.onFocusChanged?.call(false);
                } else if (widget.entity.hasRightFocus != true && focus) {
                  widget.entity.hasRightFocus = true;
                  widget.entity.onFocusChanged?.call(true);
                }
              },
              onChanged: (value) {
                widget.entity.maxValue = value;
                widget.entity.onSubmittedForMax?.call(widget.entity);
              },
            ),
          ),
          SizedBox(
            width: 8.dm * ratio,
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                widget.entity.unit ?? '',
                style: TextStyle(
                    fontSize: 8.dm * ratio, color: KqPadThemeColors.text26),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
          ),
        ],
      );
    });
  }
}
