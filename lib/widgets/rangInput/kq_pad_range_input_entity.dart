import 'package:flutter/material.dart';
import 'package:kq_flutter_pad_widgets/widgets/rangInput/kq_pad_range_input.dart';

class KqPadRangeInputEntity {
  /// 标题
  final String title;

  /// 单位
  String? unit;

  /// 最小值
  String? minValue;

  /// 最大值
  String? maxValue;

  /// 是否有左焦点
  bool? hasLeftFocus;

  /// 是否有右焦点
  bool? hasRightFocus;

  /// 焦点变化（获取和失去的时候才返回）
  final void Function(bool focus)? onFocusChanged;

  /// 最小值回调
  final void Function(KqPadRangeInputEntity entity)? onSubmittedForMin;

  /// 最大值回调
  final void Function(KqPadRangeInputEntity entity)? onSubmittedForMax;

  KqPadRangeInputEntity(
      {required this.title,
      this.unit,
      this.minValue,
      this.maxValue,
      this.onSubmittedForMin,
      this.onSubmittedForMax,
      this.onFocusChanged});

  static Widget renderRangInputWidget(KqPadRangeInputEntity entity) {
    return KqPadRangeInput(entity: entity);
  }

  static List<Widget> renderRangInputWidgets(
      List<KqPadRangeInputEntity> entitys) {
    List<Widget> inputs = [];
    for (var element in entitys) {
      inputs.add(KqPadRangeInput(entity: element));
    }
    return inputs;
  }
}
