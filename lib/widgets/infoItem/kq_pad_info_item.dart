import 'package:flutter/material.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';

class KqPadInfoItem extends StatefulWidget {
  /// 标题
  final String title;

  /// 值
  final String? value;

  /// value的字体
  final String? valueFontFamily;

  /// 单位
  final String? unit;

  /// 高
  final double? height;

  /// 圆角
  final double? radius;

  /// 中间间距
  final double? distance;

  final double? valueFontSize;

  final double? titleFontSize;

  const KqPadInfoItem(
      {required this.title,
      this.value,
      this.valueFontFamily,
      this.unit,
      this.height,
      this.radius,
      this.distance,
      this.valueFontSize,
      this.titleFontSize,
      super.key});

  @override
  State<KqPadInfoItem> createState() => _KqPadInfoItemState();
}

class _KqPadInfoItemState extends State<KqPadInfoItem> {
  @override
  Widget build(BuildContext context) {
    String title = widget.title;
    if (widget.unit != null && widget.unit != '') {
      title = '$title(${widget.unit})';
    }
    return Expanded(
        child: Container(
      height: widget.height ?? 36.dm,
      decoration: BoxDecoration(
          color: KqPadThemeColors.bgBlack04,
          borderRadius: BorderRadius.circular(widget.radius ?? 3.dm)),
      child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 12.dm,
            ),
            Text(
              title,
              style: TextStyle(
                  fontSize: widget.titleFontSize ?? 8.dm,
                  color: KqPadThemeColors.text26),
            ),
            SizedBox(
              width: widget.distance ?? 12.dm,
            ),
            if (widget.value != null && widget.value!.isNotEmpty)
              Expanded(
                  child: Text(
                widget.value ?? '',
                style: TextStyle(
                    fontSize: widget.valueFontSize ?? 12.dm,
                    color: KqPadThemeColors.text26,
                    fontFamily: widget.valueFontFamily),
                textAlign: TextAlign.right,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              )),
            SizedBox(
              width: 12.dm,
            ),
          ],
        ),
      ),
    ));
  }
}
