import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'kq_flutter_pad_widgets_platform_interface.dart';

/// An implementation of [KqFlutterPadWidgetsPlatform] that uses method channels.
class MethodChannelKqFlutterPadWidgets extends KqFlutterPadWidgetsPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('kq_flutter_pad_widgets');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
