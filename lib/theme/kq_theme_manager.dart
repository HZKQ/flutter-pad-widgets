import 'package:kq_flutter_core_widget/config/kq_core_global.dart';

import '../config/kq_pad_global.dart';
import 'configs/button/kq_button_theme_config.dart';
import 'configs/common/kq_common_theme_config.dart';
import 'configs/form/kq_form_theme_config.dart';
import 'configs/kq_theme_config.dart';
import 'default/kq_default_config_utils.dart';

/// 主题配置管理类
class KqPadThemeManager {
  KqPadThemeManager._() {
    _checkAndInitKqConfig();
  }

  static final KqPadThemeManager _instance = KqPadThemeManager._();

  static KqPadThemeManager get instance {
    return _instance;
  }

  /// 主题配置Map，用于多个主题切换
  Map<String, KqPadThemeConfig> globalConfig = <String, KqPadThemeConfig>{};

  /// 检查是否有默认配置
  bool isKqPadThemeConfig() => globalConfig[kqPadThemeConfigId] != null;

  void _checkAndInitKqConfig() {
    if (!isKqPadThemeConfig()) {
      globalConfig[kqPadThemeConfigId] =
          KqPadDefaultConfigUtils.defaultAllConfig;
    }
  }

  /// 注册主题配置，不传configId，则修改默认主题，多个主题可以用多个configId。
  void register(KqPadThemeConfig? allThemeConfig,
      {String configId = kqPadThemeConfigId}) {
    if (allThemeConfig != null) {
      globalConfig[configId] = allThemeConfig;
    }
  }

  /// 获取当前的主题的配置
  KqPadThemeConfig getConfig() {
    KqPadThemeConfig? allThemeConfig =
        globalConfig[KqPadGlobal.themeConfigId] ??
            globalConfig[kqPadThemeConfigId];
    assert(allThemeConfig != null, 'No suitable config found.');
    return allThemeConfig!;
  }

  /// 获取当前主题的通用配置
  static KqPadCommonThemeConfig getCommonConfig() {
    return KqPadThemeManager.instance.getConfig().commonConfig;
  }

  /// 获取当前主题的按钮配置
  static KqPadButtonThemeConfig getButtonConfig() {
    return KqPadThemeManager.instance.getConfig().buttonThemeConfig;
  }

  /// 获取当前主题的表单配置
  static KqPadFormThemeConfig getFormConfig() {
    return KqPadThemeManager.instance.getConfig().formConfig;
  }
}
