import 'dart:ui';

/// 按钮主题配置
class KqPadButtonThemeConfig {
  /// [KqHeadButton]的弹出菜单的文字大小
  final double? headButtonMenuFontSize;

  /// [KqHeadButton]的弹出菜单的图标颜色，如果无权限时，取该颜色的25%透明度
  final Color? headButtonMenuIconColor;

  /// [KqHeadButton]的弹出菜单的图标颜色-无权限
  final Color? headButtonMenuIconColorNoPermission;

  /// [KqHeadButton]的弹出菜单的文字颜色，如果无权限时，取该颜色的25%透明度
  final Color? headButtonMenuTextColor;

  /// [KqHeadButton]的弹出菜单的文字颜色-无权限
  final Color? headButtonMenuTextColorNoPermission;

  KqPadButtonThemeConfig(
      {this.headButtonMenuIconColor,
      this.headButtonMenuTextColor,
      this.headButtonMenuFontSize,
      this.headButtonMenuIconColorNoPermission,
      this.headButtonMenuTextColorNoPermission});
}
