import 'dart:ui';

/// 通用主题配置
class KqPadCommonThemeConfig {
  /// 主题色，默认海天蓝，标题背景跟随这个
  final Color? mainColor;

  /// 主题亮色，比如天蓝
  final Color? mainLightColor;

  /// 背景色，一般是灰色
  final Color? bgColor;

  /// 背景色，一般白色
  final Color? itemBgColor;

  /// 标题文字颜色，默认白色
  final Color? titleTextColor;

  /// 分割线颜色
  final Color? lineColor;

  /// 输入框文字颜色
  final Color? inputColor;

  /// Cell标题文字颜色
  final Color? itemTitleColor;

  /// Cell内容文字颜色
  final Color? itemContentColor;

  /// 提示文字颜色
  final Color? placeHolderColor;

  /// 右边按钮图片颜色
  final Color? rightBtnColor;

  KqPadCommonThemeConfig(
      {this.mainColor,
      this.mainLightColor,
      this.bgColor,
      this.itemBgColor,
      this.titleTextColor,
      this.lineColor,
      this.inputColor,
      this.itemTitleColor,
      this.itemContentColor,
      this.placeHolderColor,
      this.rightBtnColor});
}
