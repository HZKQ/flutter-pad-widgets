import '../default/kq_default_config_utils.dart';
import 'button/kq_button_theme_config.dart';
import 'common/kq_common_theme_config.dart';
import 'form/kq_form_theme_config.dart';

/// 主题配置类
class KqPadThemeConfig {
  final KqPadCommonThemeConfig? _commonThemeConfig;
  final KqPadFormThemeConfig? _formThemeConfig;
  final KqPadButtonThemeConfig? _buttonThemeConfig;

  KqPadThemeConfig({
    KqPadCommonThemeConfig? commonThemeConfig,
    KqPadFormThemeConfig? formConfig,
    KqPadButtonThemeConfig? buttonThemeConfig,
  })  : _formThemeConfig = formConfig,
        _commonThemeConfig = commonThemeConfig,
        _buttonThemeConfig = buttonThemeConfig;

  /// 通用配置
  KqPadCommonThemeConfig get commonConfig =>
      _commonThemeConfig ?? KqPadDefaultConfigUtils.defaultCommonConfig;

  /// 表单配置
  KqPadFormThemeConfig get formConfig =>
      _formThemeConfig ?? KqPadDefaultConfigUtils.defaultFormConfig;

  /// 按钮配置
  KqPadButtonThemeConfig get buttonThemeConfig =>
      _buttonThemeConfig ?? KqPadDefaultConfigUtils.defaultButtonConfig;
}
