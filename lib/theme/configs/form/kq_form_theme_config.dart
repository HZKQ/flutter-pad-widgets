/// 表单配置类
class KqPadFormThemeConfig {
  /// 是否必填字体大小
  final double? mustInputFontSize;

  /// 标题字体大小
  final double? titleFontSize;

  /// 内容字体大小
  final double? contentFontSize;

  /// 单位字体大小
  final double? unitFontSize;

  /// 头部字体大小
  final double? headerFontSize;

  /// 标题最大宽度
  final double? titleMaxWidth;

  /// 水平方向padding
  final double? paddingHorizontal;

  final bool? isHideInputBox;

  final double? rightMargin;

  KqPadFormThemeConfig(
      {this.mustInputFontSize,
      this.titleFontSize,
      this.contentFontSize,
      this.unitFontSize,
      this.headerFontSize,
      this.titleMaxWidth,
      this.paddingHorizontal,
      this.isHideInputBox,
      this.rightMargin});
}
