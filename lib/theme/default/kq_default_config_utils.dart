import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';

import '../../resources/kq_pad_theme_colors.dart';
import '../configs/button/kq_button_theme_config.dart';
import '../configs/common/kq_common_theme_config.dart';
import '../configs/form/kq_form_theme_config.dart';
import '../configs/kq_theme_config.dart';

/// 默认主题
class KqPadDefaultConfigUtils {
  /// 默认所有主题配置
  static KqPadThemeConfig get defaultAllConfig => KqPadThemeConfig(
        formConfig: defaultFormConfig,
        commonThemeConfig: defaultCommonConfig,
        buttonThemeConfig: defaultButtonConfig,
      );

  /// 默认通用主题
  static KqPadCommonThemeConfig get defaultCommonConfig =>
      KqPadCommonThemeConfig(
          mainColor: KqPadThemeColors.bgBlue,
          mainLightColor: KqPadThemeColors.textLightBlue,
          bgColor: KqPadThemeColors.bgD8,
          itemBgColor: KqPadThemeColors.bgWhite,
          titleTextColor: KqPadThemeColors.textWhite,
          lineColor: KqPadThemeColors.lineColor,
          inputColor: KqPadThemeColors.text59,
          itemTitleColor: KqPadThemeColors.text26,
          itemContentColor: KqPadThemeColors.text59,
          placeHolderColor: KqPadThemeColors.textBF);

  /// 默认表单主题
  static KqPadFormThemeConfig get defaultFormConfig => KqPadFormThemeConfig(
      mustInputFontSize: 7.dm,
      titleFontSize: 7.dm,
      contentFontSize: 7.dm,
      unitFontSize: 7.dm,
      headerFontSize: 7.dm,
      titleMaxWidth: 60.dm,
      paddingHorizontal: 8.dm,
      isHideInputBox: false);

  /// 默认按钮主题
  static KqPadButtonThemeConfig get defaultButtonConfig =>
      KqPadButtonThemeConfig(
          headButtonMenuIconColor: KqPadThemeColors.text26,
          headButtonMenuTextColor: KqPadThemeColors.text26,
          headButtonMenuIconColorNoPermission: KqPadThemeColors.textCCC,
          headButtonMenuTextColorNoPermission: KqPadThemeColors.textCCC,
          headButtonMenuFontSize: 24.dm);
}
