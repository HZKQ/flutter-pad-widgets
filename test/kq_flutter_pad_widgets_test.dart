import 'package:flutter_test/flutter_test.dart';
import 'package:kq_flutter_pad_widgets/kq_flutter_pad_widgets.dart';
import 'package:kq_flutter_pad_widgets/kq_flutter_pad_widgets_platform_interface.dart';
import 'package:kq_flutter_pad_widgets/kq_flutter_pad_widgets_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockKqFlutterPadWidgetsPlatform
    with MockPlatformInterfaceMixin
    implements KqFlutterPadWidgetsPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final KqFlutterPadWidgetsPlatform initialPlatform = KqFlutterPadWidgetsPlatform.instance;

  test('$MethodChannelKqFlutterPadWidgets is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelKqFlutterPadWidgets>());
  });

  test('getPlatformVersion', () async {
    KqFlutterPadWidgets kqFlutterPadWidgetsPlugin = KqFlutterPadWidgets();
    MockKqFlutterPadWidgetsPlatform fakePlatform = MockKqFlutterPadWidgetsPlatform();
    KqFlutterPadWidgetsPlatform.instance = fakePlatform;

    expect(await kqFlutterPadWidgetsPlugin.getPlatformVersion(), '42');
  });
}
