import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kq_flutter_pad_widgets/kq_flutter_pad_widgets_method_channel.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  MethodChannelKqFlutterPadWidgets platform = MethodChannelKqFlutterPadWidgets();
  const MethodChannel channel = MethodChannel('kq_flutter_pad_widgets');

  setUp(() {
    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger.setMockMethodCallHandler(
      channel,
      (MethodCall methodCall) async {
        return '42';
      },
    );
  });

  tearDown(() {
    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger.setMockMethodCallHandler(channel, null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
