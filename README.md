# kq_flutter_pad_widgets

科强Pad端Flutter组件库

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter development, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

注意：pad组件需要和APP混用，所以组件名称需要统一一个前缀-"KqPad"