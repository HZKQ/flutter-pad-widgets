#ifndef FLUTTER_PLUGIN_KQ_FLUTTER_PAD_WIDGETS_PLUGIN_H_
#define FLUTTER_PLUGIN_KQ_FLUTTER_PAD_WIDGETS_PLUGIN_H_

#include <flutter/method_channel.h>
#include <flutter/plugin_registrar_windows.h>

#include <memory>

namespace kq_flutter_pad_widgets {

class KqFlutterPadWidgetsPlugin : public flutter::Plugin {
 public:
  static void RegisterWithRegistrar(flutter::PluginRegistrarWindows *registrar);

  KqFlutterPadWidgetsPlugin();

  virtual ~KqFlutterPadWidgetsPlugin();

  // Disallow copy and assign.
  KqFlutterPadWidgetsPlugin(const KqFlutterPadWidgetsPlugin&) = delete;
  KqFlutterPadWidgetsPlugin& operator=(const KqFlutterPadWidgetsPlugin&) = delete;

  // Called when a method is called on this plugin's channel from Dart.
  void HandleMethodCall(
      const flutter::MethodCall<flutter::EncodableValue> &method_call,
      std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result);
};

}  // namespace kq_flutter_pad_widgets

#endif  // FLUTTER_PLUGIN_KQ_FLUTTER_PAD_WIDGETS_PLUGIN_H_
