#include "include/kq_flutter_pad_widgets/kq_flutter_pad_widgets_plugin_c_api.h"

#include <flutter/plugin_registrar_windows.h>

#include "kq_flutter_pad_widgets_plugin.h"

void KqFlutterPadWidgetsPluginCApiRegisterWithRegistrar(
    FlutterDesktopPluginRegistrarRef registrar) {
  kq_flutter_pad_widgets::KqFlutterPadWidgetsPlugin::RegisterWithRegistrar(
      flutter::PluginRegistrarManager::GetInstance()
          ->GetRegistrar<flutter::PluginRegistrarWindows>(registrar));
}
