import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:kq_flutter_core_widget/config/kq_core_global.dart';
import 'package:kq_flutter_core_widget/resources/l10n/kq_core_s.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/resources/l10n/kq_pad_intl.dart';
import 'package:kq_flutter_pad_widgets/resources/l10n/kq_pad_strings.dart';
import 'package:kq_flutter_pad_widgets/theme/configs/common/kq_common_theme_config.dart';
import 'package:kq_flutter_pad_widgets/theme/configs/form/kq_form_theme_config.dart';
import 'package:kq_flutter_pad_widgets/theme/configs/kq_theme_config.dart';
import 'package:kq_flutter_pad_widgets/theme/kq_theme_manager.dart';
import 'package:kq_flutter_pad_widgets_example/router/route_map.dart';

import 'pages/home_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KqScreenUtilInit(
      //初始化屏幕适配框架
      designSize: const Size(414, 736),
      minTextAdapt: true,
      builder: _buildApp,
      child: const HomePage(),
    );
  }

  Widget _buildApp(BuildContext context, Widget? widget) {
    KqPadThemeManager.instance.register(KqPadThemeConfig(
        commonThemeConfig: KqPadCommonThemeConfig(
          mainLightColor: KqPadThemeColors.bgHtGreen,
          mainColor: KqPadThemeColors.bgHtGreen,
        ),
        formConfig: KqPadFormThemeConfig(
            titleFontSize: 7.dm, contentFontSize: 7.dm, unitFontSize: 7.dm)));

    return GetMaterialApp(
      title: '小诸葛',
      navigatorKey: KqCoreGlobal.navigatorKey,
      defaultTransition: Transition.rightToLeft,
      getPages: RouteMap.getPages,
      transitionDuration: const Duration(milliseconds: 150),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        KqPadLocalizationDelegate.delegate,
        KqCoreLocalizationDelegate.delegate,
      ],
      supportedLocales: [KqPadStringZh.locale],
      //设置默认语言
      fallbackLocale: const Locale('zh'),
      //配置错误的情况，使用的语言
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: KqPadThemeColors.bgF5,
        useMaterial3: false,
      ),
      builder: (context, child) {
        _initWithContext(context);
        // return EasyLoading.init()(context, child);
        return MediaQuery(
          //设置全局的文字的textScaleFactor为1.0，文字不再随系统设置改变
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: EasyLoading.init()(context, child),
        );
      },
    );
  }

  void _initWithContext(BuildContext context) {
    // 全局字符串处理器
    // KqPadGlobal.globalString = () {
    //   return KqAppWidgetString();
    // };
  }
}
