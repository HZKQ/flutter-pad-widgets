import 'package:get/get.dart';
import 'package:kq_flutter_pad_widgets_example/pages/demo/kq_pad_chart_demo_page.dart';
import 'package:kq_flutter_pad_widgets_example/pages/home_page.dart';

class RouteMap {
  /// 路由配置
  static List<GetPage> getPages = [
    GetPage(name: '/', page: () => const HomePage()),
    GetPage(
        name: '/KqPadChartDemoPage', page: () => const KqPadChartDemoPage()),
  ];

  static goChartDemoPage() {
    Get.toNamed(
      '/KqPadChartDemoPage',
    );
  }

  static goDestureDemoPage() {
    Get.toNamed(
      '/KqPadGestureDemoPage',
    );
  }
}
