import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:kq_flutter_core_widget/utils/kq_screen_util.dart';
import 'package:kq_flutter_core_widget/utils/text_input_format_utils.dart';
import 'package:kq_flutter_core_widget/utils/toast_util.dart';
import 'package:kq_flutter_core_widget/widgets/button/kq_ink_well.dart';
import 'package:kq_flutter_core_widget/widgets/imageBox/kq_image_box.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_decoration.dart';
import 'package:kq_flutter_core_widget/widgets/shadow/kq_box_shadow.dart';
import 'package:kq_flutter_pad_widgets/config/kq_pad_global.dart';
import 'package:kq_flutter_pad_widgets/resources/images.dart';
import 'package:kq_flutter_pad_widgets/resources/kq_pad_theme_colors.dart';
import 'package:kq_flutter_pad_widgets/theme/configs/common/kq_common_theme_config.dart';
import 'package:kq_flutter_pad_widgets/theme/configs/form/kq_form_theme_config.dart';
import 'package:kq_flutter_pad_widgets/theme/configs/kq_theme_config.dart';
import 'package:kq_flutter_pad_widgets/theme/kq_theme_manager.dart';
import 'package:kq_flutter_pad_widgets/utils/kq_pad_form_util.dart';
import 'package:kq_flutter_pad_widgets/widgets/ImmTemperatureView/kq_pad_imm_temperature_view.dart';
import 'package:kq_flutter_pad_widgets/widgets/ImmTemperatureViewExpo/kq_pad_imm_temperature_view_expo.dart';
import 'package:kq_flutter_pad_widgets/widgets/ImmTemperatureViewExpo/kq_pad_temperature_complete_expo.dart';
import 'package:kq_flutter_pad_widgets/widgets/burst/kq_pad_burst.dart';
import 'package:kq_flutter_pad_widgets/widgets/button/kq_pad_shadow_button.dart';
import 'package:kq_flutter_pad_widgets/widgets/button/radioButton/kq_pad_radio_button.dart';
import 'package:kq_flutter_pad_widgets/widgets/chart/base/kq_pad_chart_line_bae_mode.dart';
import 'package:kq_flutter_pad_widgets/widgets/chart/base/kq_pad_chart_model.dart';
import 'package:kq_flutter_pad_widgets/widgets/chart/kq_pda_chart_line.dart';
import 'package:kq_flutter_pad_widgets/widgets/chart/kq_pda_chart_line_model.dart';
import 'package:kq_flutter_pad_widgets/widgets/coordMap/kq_pad_coord_map.dart';
import 'package:kq_flutter_pad_widgets/widgets/coordMap/kq_pad_coord_map_entity.dart';
import 'package:kq_flutter_pad_widgets/widgets/dashedBox/kq_dashed_box.dart';
import 'package:kq_flutter_pad_widgets/widgets/dialog/kq_pad_dialog_util.dart';
import 'package:kq_flutter_pad_widgets/widgets/dialog/kq_pad_two_pulldown_dialog.dart';
import 'package:kq_flutter_pad_widgets/widgets/dragView/kq_pad_delete_and_drag_list_view.dart';
import 'package:kq_flutter_pad_widgets/widgets/filter/kq_filter_check_view.dart';
import 'package:kq_flutter_pad_widgets/widgets/form/entity/kq_form_entity.dart';
import 'package:kq_flutter_pad_widgets/widgets/form/kq_radio_item_view.dart';
import 'package:kq_flutter_pad_widgets/widgets/gradient/kq_%20gradient_bar_view.dart';
import 'package:kq_flutter_pad_widgets/widgets/infoItem/kq_pad_info_item.dart';
import 'package:kq_flutter_pad_widgets/widgets/progressBar/kq_point_multi_progress_bar.dart';
import 'package:kq_flutter_pad_widgets/widgets/pullRefresh/kq_pad_refresh_header.dart';
import 'package:kq_flutter_pad_widgets/widgets/sectionList/kq_pad_section_list_view.dart';
import 'package:kq_flutter_pad_widgets/widgets/searchBar/kq_pad_search_bar.dart';
import 'package:kq_flutter_pad_widgets/widgets/slider/kq_gradient_slider_bar.dart';
import 'package:kq_flutter_pad_widgets/widgets/slider/kq_pad_slider.dart';
import 'package:kq_flutter_pad_widgets/widgets/slider/rangeSlider/kq_pad_range_slider.dart';
import 'package:kq_flutter_pad_widgets/widgets/steps/kq_pad_horizontal_steps.dart';
import 'package:kq_flutter_pad_widgets/widgets/steps/kq_pad_record_steps.dart';
import 'package:kq_flutter_pad_widgets/widgets/tabbar/kq_pad_shadow_tab_bar.dart';
import 'package:kq_flutter_pad_widgets/widgets/tabbar/kq_tab_bar.dart';
import 'package:kq_flutter_pad_widgets/widgets/textFiled/kq_pad_textfiled.dart';
import 'package:kq_flutter_pad_widgets/widgets/timeLine/entity/kq_pad_date_line_entity.dart';
import 'package:kq_flutter_pad_widgets/widgets/timeLine/kq_pad_date_line.dart';
import 'package:kq_flutter_pad_widgets/widgets/verticalSteps/kq_pad_vertical_steps.dart';
import 'package:kq_flutter_pad_widgets/widgets/eventTimeLine/entity/kq_event_time_line_entity.dart';
import 'package:kq_flutter_pad_widgets/widgets/eventTimeLine/kq_event_time_line.dart';
import 'package:kq_flutter_pad_widgets_example/router/route_map.dart';

class CustomMold {
  String? name;
  String? content;
  String? id;

  CustomMold({this.name, this.content, this.id});
}

///
class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  bool selected = false;
  int selectIndex = 1;

  int tabIndex = 0;

  double progressValue = 0;

  @override
  void initState() {
    KqPadThemeManager.instance.register(KqPadThemeConfig(
        commonThemeConfig: KqPadCommonThemeConfig(
          mainLightColor: KqPadThemeColors.bgHtGreen,
          mainColor: KqPadThemeColors.bgHtGreen,
        ),
        formConfig: KqPadFormThemeConfig(
            titleFontSize: 7.dm, contentFontSize: 7.dm, unitFontSize: 7.dm)));

    super.initState();
  }

  List<KqPadCoordMapEntity> entitys = [
    KqPadCoordMapEntity(
        typeName: '机筒温度(℃)',
        maxValue: '45.00000000000000000000',
        minValue: '1.00000000000000000000',
        points: [
          "1.0",
          "14.8",
          "18.6",
          "27.4",
          "36.2",
          "45.0",
          "44.9",
          "36.2",
          "27.4",
          "18.6",
          "9.8",
          "1.0",
          "1.0",
          "9.8",
          "18.6",
          "27.4",
          "36.2",
          "44.0",
          "44.0",
          "36.2",
          "27.4",
          "18.6",
          "9.8",
          "1.2",
          "27.4"
        ]),
    KqPadCoordMapEntity(
        typeName: '模具温度(℃)',
        maxValue: '45.00000000000000000000',
        minValue: '1.00000000000000000000',
        points: [
          "1.0",
          "9.8",
          "18.6",
          "27.4",
          "36.2",
          "45.0",
          "36.2",
          "27.4",
          "18.6",
          "9.8",
          "1.0",
          "45.0",
          "36.2",
          "27.4",
          "45.0",
          "1.0",
          "9.8",
          "18.6",
          "9.8",
          "1.0",
          "45.0",
          "36.2",
          "18.6",
          "27.4",
          "9.8"
        ]),
    KqPadCoordMapEntity(
        typeName: '射出速度(mm/s)',
        maxValue: '23.00000000000000000000',
        minValue: '1.00000000000000000000',
        points: [
          "1.0",
          "5.4",
          "9.8",
          "14.2",
          "18.6",
          "23.0",
          "14.2",
          "9.8",
          "5.4",
          "1.0",
          "23.0",
          "18.6",
          "9.8",
          "1.0",
          "5.4",
          "18.6",
          "14.2",
          "18.6",
          "9.8",
          "5.4",
          "1.0",
          "23.0",
          "14.2",
          "23.0",
          "23.0"
        ]),
    KqPadCoordMapEntity(
        typeName: '保压压力(bar)',
        maxValue: '16.00000000000000000000',
        minValue: '1.00000000000000000000',
        points: [
          "1.0",
          "4.0",
          "7.0",
          "10.0",
          "13.0",
          "16.0",
          "7.0",
          "4.0",
          "1.0",
          "16.0",
          "13.0",
          "10.0",
          "16.0",
          "7.0",
          "13.0",
          "4.0",
          "1.0",
          "10.0",
          "13.0",
          "7.0",
          "4.0",
          "1.0",
          "16.0",
          "13.0",
          "7.0"
        ]),
    KqPadCoordMapEntity(
        typeName: '机筒温度(摄氏度)',
        maxValue: '55.00000000000000000000',
        minValue: '4.00000000000000000000',
        points: [
          "4.0",
          "4.0",
          "4.0",
          "4.0",
          "4.0",
          "4.0",
          "14.2",
          "14.2",
          "14.2",
          "14.2",
          "14.2",
          "14.2",
          "24.4",
          "24.4",
          "24.4",
          "24.4",
          "24.4",
          "34.6",
          "44.8",
          "44.8",
          "44.8",
          "44.8",
          "44.8",
          "34.6",
          "55.0"
        ]),
  ];

  List<KqEventTimeLineEntity> timeLines = [
    KqEventTimeLineEntity(
        time: '2024-02-23',
        title: '工艺参数优化0620-T0031工艺参数优化0620-T0031工艺参数优化0620-T0031',
        id: 0),
    KqEventTimeLineEntity(
        time: '2024-02-24', title: '工艺参数优化0620-T0032', id: 1, event: '推荐：重量优先'),
    KqEventTimeLineEntity(time: '2024-02-25', title: '工艺参数优化0620-T0033', id: 2),
    KqEventTimeLineEntity(time: '2024-02-26', title: '工艺参数优化0620-T0034', id: 3),
    KqEventTimeLineEntity(
        time: '2024-02-27', title: '工艺参数优化0620-T0035', id: 4, event: '推荐：效率优先'),
    KqEventTimeLineEntity(time: '2024-02-28', title: '工艺参数优化0620-T0036', id: 5),
    KqEventTimeLineEntity(time: '2024-03-23', title: '工艺参数优化0620-T0037', id: 6),
    KqEventTimeLineEntity(time: '2024-04-23', title: '工艺参数优化0620-T0038', id: 7),
    KqEventTimeLineEntity(time: '2024-05-23', title: '工艺参数优化0620-T0039', id: 8),
  ];

  List<CustomMold> dioArr = [
    CustomMold(name: 'name1', content: 'content1', id: '1'),
    CustomMold(name: 'name2', content: 'content2', id: '2'),
    CustomMold(name: 'name3', content: 'content3', id: '3'),
    CustomMold(name: 'name4', content: 'content4', id: '4'),
    CustomMold(name: 'name5', content: 'content5', id: '5'),
    CustomMold(name: 'name6', content: 'content6', id: '6'),
    CustomMold(name: 'name7', content: 'content7', id: '7'),
    CustomMold(name: 'name8', content: 'content8', id: '8'),
    CustomMold(name: 'name9', content: 'content9', id: '9'),
    CustomMold(name: 'name10', content: 'content10', id: '10'),
    CustomMold(name: 'name11', content: 'content11', id: '11'),
    CustomMold(name: 'name12', content: 'content12', id: '12'),
  ];

  double minProgress = 10;
  double maxProgress = 70;

  Widget lineWidget() {
    KqPdaChartLineModel model = KqPdaChartLineModel(
      xDividerList: [
        KqPdaDividerValue(value: 0.7, dividerColor: KqPadThemeColors.textGreen),
        KqPdaDividerValue(value: 0.4, dividerColor: KqPadThemeColors.textRed)
      ],
      yDividerList: [
        KqPdaDividerValue(
            value: 0.9, dividerColor: KqPadThemeColors.textYellow),
        KqPdaDividerValue(
            value: 0.3, dividerColor: KqPadThemeColors.textLightBlue)
      ],
      baseModel: KqPadChartLineBaseModel(
          lineColors: [KqPadThemeColors.bgBlue],
          rightLineColors: [KqPadThemeColors.textRed]),
      chartModel: KqPadChartModel(
        angle: 45,
        isDivide: true,
        isShowClickValue: true,
        isShowSlideValue: true,
        isShowPoint: true,
        isFixMaxY: false,
        sizeHeight: 400,
        sizeWidth: 500,
        unit: '切换压力',
        rightUnit: '有效黏度(K)',
        xRight: 0,
        leftYAxisList: [20, 40, 80, 100, 120],
        rightYAxisList: [30, 60, 90, 120, 150],
      ),
    );
    model.chartArray = [
      [
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '11111',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(xValue: '222222', yValue: '60', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '3333',
            yValue: '40',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textGreen,
            lineColor: KqPadThemeColors.textYellow,
            isShow: true),
        KqPdaChartLineValue(xValue: '444444', yValue: '90', lineWidth: 2.dm),
        KqPdaChartLineValue(
            xValue: '555555',
            yValue: '20',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            lineColor: KqPadThemeColors.textRed,
            isShow: true),
        KqPdaChartLineValue(
            xValue: '66666', yValue: '120', lineWidth: 2.dm, isShow: true),
        KqPdaChartLineValue(
            xValue: '7777777',
            yValue: '100',
            lineWidth: 2.dm,
            pointColor: KqPadThemeColors.textYellow,
            isShow: true),
      ]
    ];
    model.chartRightArray = [
      [
        KqPdaChartLineValue(xValue: '1', yValue: '30'),
        KqPdaChartLineValue(xValue: '2', yValue: '60'),
        KqPdaChartLineValue(xValue: '3', yValue: '40'),
        KqPdaChartLineValue(xValue: '4', yValue: '150'),
        KqPdaChartLineValue(xValue: '5', yValue: '70'),
        KqPdaChartLineValue(xValue: '6', yValue: '120'),
        KqPdaChartLineValue(xValue: '7', yValue: '80'),
      ]
    ];
    return Container(
      width: 500,
      height: 500,
      // color: KqPadThemeColors.text8C,
      // alignment: Alignment.center,
      child: KqPdaChartLine(
        // backGroundColor: KqPadThemeColors.bgBlack05,
        height: 400,
        width: 500,
        chartModel: model,
      ),
    );
  }

  final EasyRefreshController easyRefreshController =
      EasyRefreshController(controlFinishRefresh: true);

  KqPadFormEntity entity = KqPadFormEntity(
      itemType: ItemType.edit,
      title: '名称',
      editable: true,
      unit: 'ton',
      mustInput: true,
      forceHideDivider1: false,
      titleColor: Colors.red);
  KqPadFormEntity entity1 = KqPadFormEntity(
      itemType: ItemType.edit,
      title: '名称',
      editable: true,
      unit: 'Ton',
      forceHideDivider1: false,
      titleColor: Colors.white,
      titleFontWeight: FontWeight.w600);
  KqPadFormEntity entity2 = KqPadFormEntity(
      itemType: ItemType.edit,
      title: '名称',
      editable: true,
      forceHideDivider1: false,
      titleColor: Colors.red);
  KqPadFormEntity entity3 = KqPadFormEntity(
    itemType: ItemType.select,
    title: '名称',
    editable: true,
    valueForShow: '1111',
    forceHideDivider1: false,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffEBECF0),
      appBar: AppBar(
        title: const Text('Pad组件演示'),
      ),
      body: EasyRefresh.builder(
        header: const KqPadRefreshHeader(),
        controller: easyRefreshController,
        onRefresh: () {
          Future.delayed(const Duration(seconds: 3))
              .then((value) => easyRefreshController.finishRefresh());
        },
        childBuilder: (context, physics) {
          return SingleChildScrollView(
            physics: physics,
            child: Padding(
              padding: EdgeInsets.all(8.dm),
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  KqPadFormUtil.renderFormWidget(entity1),
                  KqPadFormUtil.renderFormWidget(entity),
                  KqPadFormUtil.renderFormWidget(entity2),
                  KqPadFormUtil.renderFormWidget(entity3),
                  InkWell(
                    onTap: () {
                      RouteMap.goChartDemoPage();
                    },
                    child: Text('折线图'),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  SizedBox(
                      height: 50,
                      width: 800,
                      child: Row(
                        children: [
                          KqPadInfoItem(
                            title: '锁模力',
                            value: '160',
                            unit: 'dar',
                            valueFontFamily: 'DINCond-Black',
                            height: 36.dm,
                          ),
                          SizedBox(
                            width: 10.dm,
                          ),
                          KqPadInfoItem(
                            title: '锁模力',
                            value: '160',
                            unit: 'dar',
                            valueFontFamily: 'DINCond-Black',
                            height: 36.dm,
                          ),
                          SizedBox(
                            width: 10.dm,
                          ),
                          KqPadInfoItem(
                            title: '锁模力',
                            value: '160',
                            unit: 'dar',
                            valueFontFamily: 'DINCond-Black',
                            height: 36.dm,
                          ),
                        ],
                      )),
                  KqPadTemperatureCompleteExpo(
                    data: '100,200.93,384.283,122.333',
                    isCanEdit: false,
                    fontColor: Color(0xFF262626),
                    tempImageColor: Color(0xFFAFAFAF),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  KqPadImmTemperatureViewExpo(
                    data: '100,200.93,384.283,122.333',
                    isCanEdit: false,
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  KqPadImmTemperatureView(
                    data: ',13,14,55,67,88',
                    isCanEdit: false,
                    valueCallback: (data) => print(data),
                  ),

                  SizedBox(
                    height: 60,
                  ),
                  KqPadImmTemperatureView(
                    data: ',13,14,55,67,88',
                    isCanEdit: true,
                    valueCallback: (data) => print(data),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  lineWidget(),
                  SizedBox(
                    width: 96.dm,
                    child: KqPadShadowButton(
                      centerText: "黏度试验",
                      selected: selected,
                      leftWidget: SizedBox(
                        width: 7.dm,
                        height: 7.dm,
                        child: const ColoredBox(
                          color: Colors.red,
                        ),
                      ),
                      rightWidget: SizedBox(
                        width: 7.dm,
                        height: 7.dm,
                        child: const ColoredBox(
                          color: Colors.yellow,
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          selected = !selected;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: [
                      Align(
                        child: KqInkWell(
                          backgroundColor: KqPadThemeColors.bgBlue,
                          radius: 4.dm,
                          horizontalPadding: 8.dm,
                          verticalPadding: 4.dm,
                          child: Text(
                            '列表选择弹窗',
                            style: TextStyle(
                                fontSize: 8.dm,
                                color: KqPadThemeColors.textWhite),
                          ),
                          onTap: () {
                            KqPadDialog.showPadListChooseDialog(
                              title: '选择种类',
                              onSearch: (result) {
                                print(result);
                                return [dioArr[0], dioArr[1]];
                              },
                              itemContent: (index, item) => item.name ?? '',
                              itemList: () => dioArr,
                              selectedItem: (index, item) {
                                print(item);
                              },
                              isSelected: (index, item) =>
                                  item.name == 'name1' ? true : false,
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        width: 8.dm,
                      ),
                      Align(
                        child: KqInkWell(
                          backgroundColor: KqPadThemeColors.bgBlue,
                          radius: 4.dm,
                          horizontalPadding: 8.dm,
                          verticalPadding: 4.dm,
                          child: Text(
                            '确认对话框',
                            style: TextStyle(
                                fontSize: 8.dm,
                                color: KqPadThemeColors.textWhite),
                          ),
                          onTap: () {
                            KqPadDialog.showConfirmDialog(
                                title: '测试',
                                onConfirm: () {
                                  setState(() {});
                                },
                                msg:
                                    '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测？');
                          },
                        ),
                      ),
                      SizedBox(
                        width: 8.dm,
                      ),
                      Align(
                        child: KqInkWell(
                          backgroundColor: KqPadThemeColors.bgBlue,
                          radius: 4.dm,
                          horizontalPadding: 8.dm,
                          verticalPadding: 4.dm,
                          child: Text(
                            '输入对话框',
                            style: TextStyle(
                                fontSize: 8.dm,
                                color: KqPadThemeColors.textWhite),
                          ),
                          onTap: () {
                            KqPadDialog.showInputDialog(
                              '测试',
                              onConfirm: (text) {},
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        width: 8.dm,
                      ),
                      Align(
                        child: KqInkWell(
                          backgroundColor: KqPadThemeColors.bgBlue,
                          radius: 4.dm,
                          horizontalPadding: 8.dm,
                          verticalPadding: 4.dm,
                          child: Text(
                            '消息对话框',
                            style: TextStyle(
                                fontSize: 8.dm,
                                color: KqPadThemeColors.textWhite),
                          ),
                          onTap: () {
                            KqPadDialog.showMsgDialog(
                                title: '数据不足',
                                msg:
                                    '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测');
                          },
                        ),
                      ),
                      SizedBox(
                        width: 8.dm,
                      ),
                      Align(
                        child: KqInkWell(
                          backgroundColor: KqPadThemeColors.bgBlue,
                          radius: 4.dm,
                          horizontalPadding: 8.dm,
                          verticalPadding: 4.dm,
                          child: Text(
                            '下拉弹窗',
                            style: TextStyle(
                                fontSize: 8.dm,
                                color: KqPadThemeColors.textWhite),
                          ),
                          onTap: () {
                            KqPadTwoPullDownDialog.show(
                                firstTitle: '选择参数',
                                secondTitle: '选择配方',
                                firstPlaceholder: '请选择参数',
                                secondPlaceholder: '请选择配方',
                                firstDataSource: [
                                  {'title': '初始参数', 'id': 1},
                                  {'title': '试模参数', 'id': 2}
                                ],
                                secondDataSource: [
                                  {'title': '配方1', 'id': 1},
                                  {'title': '配方2', 'id': 2},
                                  {'title': '配方3', 'id': 3},
                                  {'title': '配方4', 'id': 4},
                                  {'title': '配方5', 'id': 5},
                                  {'title': '配方5', 'id': 5}
                                ],
                                firstNameRender: (p0, p1) {
                                  return p1['title'] as String;
                                },
                                secondNameRender: (p0, p1) {
                                  return p1['title'] as String;
                                },
                                firstSelectedCallback: (p0) {
                                  return p0['title'];
                                  print(p0);
                                },
                                secondSelectedCallback: (p0) {
                                  return p0['title'];
                                  print(p0);
                                },
                                dialogTitle: '重新修正');
                          },
                        ),
                      ),
                      SizedBox(
                        width: 8.dm,
                      ),
                      Align(
                        child: KqInkWell(
                          backgroundColor: KqPadThemeColors.bgBlue,
                          radius: 4.dm,
                          horizontalPadding: 8.dm,
                          verticalPadding: 4.dm,
                          child: Text(
                            '操作对话框',
                            style: TextStyle(
                                fontSize: 8.dm,
                                color: KqPadThemeColors.textWhite),
                          ),
                          onTap: () {
                            KqPadDialog.showOptDialog(
                                title: '测试标题',
                                onBtnTap: () {
                                  KqToast.showNormal('111');
                                },
                                child: Padding(
                                  padding: EdgeInsets.all(12.dm),
                                  child: Column(
                                    children: [
                                      Text(
                                          '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测'),
                                    ],
                                  ),
                                ));
                          },
                        ),
                      ),
                      SizedBox(
                        width: 8.dm,
                      ),
                      Align(
                        child: KqInkWell(
                          backgroundColor: KqPadThemeColors.bgBlue,
                          radius: 4.dm,
                          horizontalPadding: 8.dm,
                          verticalPadding: 4.dm,
                          child: Text(
                            '自定义弹窗',
                            style: TextStyle(
                                fontSize: 8.dm,
                                color: KqPadThemeColors.textWhite),
                          ),
                          onTap: () {
                            KqPadDialog.showCustomDialog(
                                dialogWidth: 200.dm,
                                title: '标题标题标题标题标题标题标题标题标题标题标题标题',
                                isShowClose: true,
                                customWidget: SizedBox(
                                  height: 100.dm,
                                  child: const Column(
                                    children: [
                                      Text(
                                          '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测'),
                                    ],
                                  ),
                                ));
                          },
                        ),
                      ),
                    ],
                  ),
                  InkWell(
                    child: Container(
                      width: 100,
                      height: 30,
                      color: Colors.black,
                    ),
                    onTap: () {
                      setState(() {
                        selectIndex = selectIndex + 1;
                      });
                      print(selectIndex);
                    },
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text('平面坐标图'),
                  KqPadCoordMap(
                    height: 500,
                    entitys: entitys,
                    selectedIndex: selectIndex,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text('时间轴'),
                  SizedBox(
                    width: 200,
                    height: 500,
                    child: KqEventTimeLine(data: timeLines),
                  ),

                  SizedBox(
                    height: 30,
                  ),
                  Text('范围输入框'),
                  // KqPadRangInput(
                  //   title: '机筒温度',
                  //   onSubmittedForMax: (value) {
                  //     print(value);
                  //   },
                  //   onSubmittedForMin: (value) {
                  //     print(value);
                  //   },
                  // ),
                  // KqPadRangInput(
                  //   title: '注射速度',
                  //   unit: 'mm/s',
                  //   onSubmittedForMax: (value) {
                  //     print(value);
                  //   },
                  //   onSubmittedForMin: (value) {
                  //     print(value);
                  //   },
                  // ),
                  SizedBox(
                    height: 30.dm,
                  ),
                  KqPadHorizontalSteps(
                    controller: KqPadStepsController(
                        currentIndex: 1,
                        steps: [
                          {'title': '试验说明'},
                          {'title': '创建试验'},
                          {'title': '试验数据填写'},
                          {'title': '图表和结论'}
                        ].map((e) {
                          return KqPadStep(
                            stepContentText: e['title'],
                          );
                        }).toList()),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  KqPadHorizontalSteps(
                    controller: KqPadStepsController(
                        currentIndex: 1,
                        steps: [
                          {'title': '试验说明试验说明试验说明试验说明'},
                          {'title': '创建试验创建试验'},
                          {'title': '试验数据填写试验数据填写试验数据填写试验数据填写试验数据填写'},
                          {'title': '图表和结论图表和结论'}
                        ].map((e) {
                          return KqPadStep(
                            stepContentText: e['title'],
                          );
                        }).toList()),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  KqPadHorizontalSteps(
                    controller: KqPadStepsController(
                        currentIndex: 1,
                        steps: [
                          {'title': '00000'},
                          {'title': '1111111111111'},
                          {'title': '22222222222222'},
                          {'title': '444444'},
                          {'title': '555555555'},
                        ].map((e) {
                          return KqPadStep(
                            stepContentText: e['title'],
                          );
                        }).toList()),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  SizedBox(
                    width: 100,
                    height: 200,
                    child: KqPadDateLine(
                      physics: const NeverScrollableScrollPhysics(),
                      data: [
                        KqPadDateLineEntity(title: '步骤一'),
                        KqPadDateLineEntity(title: '步骤二'),
                        KqPadDateLineEntity(title: '步骤三'),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  KqPadRadioButton(
                    radioEntity: [
                      IRadioEntity("title1"),
                      IRadioEntity("title2"),
                      IRadioEntity("title3"),
                      IRadioEntity("title4"),
                      IRadioEntity("title5"),
                      IRadioEntity("title6"),
                      IRadioEntity("title7"),
                      IRadioEntity("title8"),
                      IRadioEntity("title9"),
                      IRadioEntity("title10"),
                      IRadioEntity("title11"),
                      IRadioEntity("title12"),
                      IRadioEntity("title13"),
                      IRadioEntity("title14"),
                      IRadioEntity("title15"),
                      IRadioEntity("title16"),
                      IRadioEntity("title17"),
                      IRadioEntity("title18"),
                      IRadioEntity("title19"),
                      IRadioEntity("title20"),
                      IRadioEntity("title21"),
                      IRadioEntity("title22"),
                      IRadioEntity("title23"),
                      IRadioEntity("title24"),
                    ],
                    onChanged: (index) {
                      return Future(() => true);
                    },
                    defaultIndex: 1,
                    showType: ShowType.lineBreakDisplay,
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  ...KqPadFormUtil.renderFormWidgetList([
                    KqPadFormEntity(
                      itemType: ItemType.radio,
                      title: "参数类型",
                      editable: true,
                      valueForSubmit: '0',
                      radioData: [
                        RadioData(item: '重量参数(g)', itemId: '0'),
                        RadioData(item: '尺寸参数(mm)', itemId: '1'),
                        RadioData(item: '尺寸参数(mm)', itemId: '2'),
                        RadioData(item: '尺寸参数(mm)', itemId: '3'),
                        RadioData(item: '尺寸参数(mm)', itemId: '4'),
                      ],
                      onRadioChanged: (entity, radioData) {
                        KqToast.showNormal(radioData.item);
                      },
                    ),
                    KqPadFormEntity(
                      itemType: ItemType.edit,
                      title: "切换压力（Mpa)",
                      hint: "请输入射出时间",
                      editable: true,
                      // valueForShow: '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试',
                      valueForShow: '测试',
                      valueForSubmit: '测试',
                      multiline: true,
                      onMonitor: (entity, value) {
                        KqToast.showNormal(value);
                      },
                    ),
                    KqPadFormEntity(
                      itemType: ItemType.select,
                      title: "切换压力（Mpa)",
                      valueForShow:
                          '测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试',
                      // valueForShow: '测试',
                      valueForSubmit: '测试',
                      enableClearButton: true,
                      onClearTap: (entity) {
                        entity.valueForShow = null;
                        entity.valueForSubmit = null;
                        entity.updateWidget();
                      },
                      multiline: true,
                      editable: true,
                      onTap: (entity) {
                        entity.valueForShow = '111';
                        entity.updateWidget();
                      },
                    ),
                    KqPadFormEntity(
                      itemType: ItemType.editUpperLower,
                      title: "输入范围测试",
                      unit: 'kg',
                      valuesForShow: ['180.0', '280.0'],
                      editable: true,
                    ),
                    KqPadFormEntity(
                      itemType: ItemType.imageBox,
                      title: "上传图片",
                      editable: true,
                      imageUrls: <ImageUrl>[],
                    ),
                  ]),
                  SizedBox(
                    height: 100,
                    child: KqPadDeleteAndDragListView(
                      itemBuilder: (index) => Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.zero,
                            border: Border.all(
                              width: 1.dm,
                              color: const Color(0xFFe0e0e0),
                            )),
                        child: Row(
                          children: [
                            Container(
                              width: 60.dm,
                              height: double.infinity,
                              color: const Color(0xFFF0F0F0),
                              padding: EdgeInsets.only(left: 40.dm),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "参数1",
                                style: TextStyle(
                                  fontSize: 6.dm,
                                  color: KqPadThemeColors.text26,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                color: const Color(0xFFFFFFFF),
                                height: double.infinity,
                                padding:
                                    EdgeInsets.only(left: 6.dm, right: 20.dm),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "产品长度(mm)",
                                  style: TextStyle(
                                    fontSize: 6.dm,
                                    color: KqPadThemeColors.text26,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      itemCount: 3,
                      onReorder: (int oldIndex, int newIndex) {
                        KqToast.showNormal("$oldIndex移动到$newIndex");

                        ///处理数据移动
                        setState(() {});
                      },
                      onDeleted: (index) {
                        KqToast.showNormal("删除$index");

                        ///删除数据
                        setState(() {});
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  const KqGradientBar(
                    colors: [
                      Colors.red,
                      Colors.orange,
                      Colors.yellow,
                      Colors.green,
                      Colors.blue,
                    ],
                    data: ['10', '20', '30', '40', '50'],
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Container(
                    child: KqPadTabBar(
                      items: [
                        PadTabDataEntity("弘讯控制器"),
                        PadTabDataEntity("长飞亚控制器"),
                      ],
                      indicatorType: PadIndicatorType.roundBg,
                      scrollable: false,
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Container(
                    width: 74.dm,
                    height: 74.dm,
                    decoration: KqBoxDecoration(
                      gradient: const LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          stops: [
                            0.0,
                            1.0
                          ],
                          colors: [
                            Color.fromRGBO(230, 231, 237, 1),
                            Color.fromRGBO(247, 248, 250, 1)
                          ]),
                      borderRadius: BorderRadius.circular(4.dm),
                      boxShadow: [
                        KqBoxShadow(
                          color: const Color(0xFFBDC1D1),
                          offset: Offset(2.dm, 2.dm),
                          spreadRadius: 0,
                        ),
                        KqBoxShadow(
                          color: const Color(0xFFFAFBFC),
                          offset: Offset(-3.dm, -3.dm),
                          blurRadius: 8.dm,
                          spreadRadius: 0,
                        ),
                        KqBoxShadow(
                          color: const Color(0xFFE9EAF2),
                          offset: Offset(1.dm, 1.dm),
                          blurRadius: 8.dm,
                          spreadRadius: 0,
                          inset: true,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Container(
                    width: 100,
                    height: 32,
                    child: KqPadTextFiled(
                      useParentHeight: true,
                      showShadow: true,
                      fontSize: 8.dm,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  KqPadShadowTabBar(
                    tabs: [
                      PadTabDataEntity("二字"),
                      PadTabDataEntity("三文字"),
                      PadTabDataEntity("四个文字"),
                      PadTabDataEntity("20231226-T0002"),
                      PadTabDataEntity("20231226-T0003"),
                      PadTabDataEntity("20231226-T0004"),
                      PadTabDataEntity("20231226-T0005"),
                      PadTabDataEntity("20231226-T0006"),
                      PadTabDataEntity("20231226-T0007"),
                      PadTabDataEntity("20231226-T0008"),
                      PadTabDataEntity("20231226-T0009"),
                      PadTabDataEntity("20231226-T0010"),
                      PadTabDataEntity("20231226-T0011"),
                      PadTabDataEntity("20231226-T0012"),
                      PadTabDataEntity("20231226-T0013"),
                    ],
                    index: tabIndex,
                    onTap: (index) {
                      setState(() {
                        tabIndex = index;
                      });
                    },
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Container(
                    width: 300,
                    height: 17.dm,
                    color: Colors.black,
                    child: KqPadSlider(
                      enable: true,
                      max: 220,
                      min: 0,
                      defluatValue: 100,
                      valueViewHeight: 25.dm,
                      valueViewWidth: 15.dm,
                      viewSliderSpacing: 16,
                      valueView: Visibility(
                          child: KqPadShadowPop(
                        width: 25.dm,
                        height: 15.dm,
                        radius: 4.dm,
                        arrowWidth: 4.5.dm,
                        arrowHeight: 2.5.dm,
                        text: progressValue.toString(),
                      )),
                      onChanged: (sliderValue) {
                        setState(() {
                          progressValue = sliderValue;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Container(
                    width: 800,
                    height: 100,
                    color: Colors.yellow,
                    child: KqPadRangeSlider(
                      max: 80000,
                      min: 10,
                      start: minProgress,
                      end: maxProgress,
                      onChange: (double one, double two) {
                        setState(() {
                          minProgress = one;
                          maxProgress = two;
                        });
                      },
                      onChangeEnd: (start, end) {
                        print("one1=$start;two2=$end");
                        setState(() {
                          minProgress = start;
                          maxProgress = end;
                        });
                      },
                    ),
                  ),
                  // KqpadRange(
                  //   isTwoSeekBar: true,
                  //   showSeekBarText: true,
                  //   height: 32.dm,
                  //   progressHeight: 6.dm,
                  //   callback: (int one, int two) {
                  //     print("one=$one;two=$two");
                  //   },
                  // ),
                  // RangeSlider(
                  //   values: RangeValues(minProgress, maxProgress),
                  //   min: 0,
                  //   max: 100,
                  //   labels: RangeLabels('${10.round()}', '${90.round()}'),
                  //   onChanged: (RangeValues values) {
                  //     setState(() {
                  //       minProgress = values.start;
                  //       maxProgress = values.end;
                  //     });
                  //     print('左滑块值: ${values.start.round()}');
                  //     print('右滑块值: ${values.end.round()}');
                  //   },
                  // ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  SizedBox(
                    width: 120.dm,
                    child: KqPadSearchBar(),
                  ),

                  SizedBox(
                    height: 10.dm,
                  ),
                  const KqGradientSliderBar(
                    gradientColors: [
                      Color(0xff006b6b),
                      Color(0xff02fdfe),
                    ],
                    progress: 10,
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  KqGradientSliderBar(
                    gradientColors: const [
                      Color(0xffFF453A),
                      Color(0xffFFEF09),
                      Color(0xff34C759),
                    ],
                    progress: 100,
                    dragEnable: true,
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),

                  KqDashedBox(
                    borderWidth: 1,
                    borderColor: Colors.red,
                    dashSpace: 4.dm,
                    dashWidth: 4.dm,
                    child: Container(
                      width: 400, // 控制容器大小
                      height: 50.dm,
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Container(
                    width: 600,
                    height: 500,
                    color: Colors.red,
                    child: KqPadSectionListView(
                      section: 4,
                      isShowIndex: true,
                      isShowScrollbar: false,
                      indexList: const ['A', 'B', 'D', 'X'],
                      headerBuilder: (section) => SizedBox(
                        height: 1,
                      ),
                      itemBuilder: (section, row, object) {
                        return Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: Container(
                            width: 500,
                            color: Colors.green,
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Row(
                                children: [
                                  Image.asset(
                                    Images.commonIcBuzhou,
                                    width: 20.dm,
                                    height: 20.dm,
                                    package: KqPadGlobal.packageName,
                                  ),
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        object['name']!,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: KqPadThemeColors.bgBlack50),
                                      ),
                                      Text(
                                        object['content']!,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: KqPadThemeColors.bgBlack50),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      itemList: (section) {
                        if (section == 0) {
                          return [
                            {"name": "ABS", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "ABS1", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "ABS2", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "ABS3", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "ABS4", "content": "丙稀腈、丁二稀、苯乙烯"}
                          ];
                        } else if (section == 1) {
                          return [
                            {"name": "BBS", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "BBS1", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "BBS2", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "BBS3", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "BBS4", "content": "丙稀腈、丁二稀、苯乙烯"}
                          ];
                        } else if (section == 2) {
                          return [
                            {"name": "DBS", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "DBS1", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "DBS2", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "DBS3", "content": "丙稀腈、丁二稀、苯乙烯"},
                            {"name": "DBS4", "content": "丙稀腈、丁二稀、苯乙烯"}
                          ];
                        }
                        return [
                          {"name": "XBS", "content": "丙稀腈、丁二稀、苯乙烯"},
                          {"name": "XBS1", "content": "丙稀腈、丁二稀、苯乙烯"},
                          {"name": "XDBS2", "content": "丙稀腈、丁二稀、苯乙烯"},
                          {"name": "XDBS3", "content": "丙稀腈、丁二稀、苯乙烯"},
                          {"name": "XDBS4", "content": "丙稀腈、丁二稀、苯乙烯"}
                        ];
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Container(
                    height: 8.dm,
                    color: Colors.black26,
                    child: const KqPadBurst(
                      bgColors: [
                        KqPadThemeColors.bgBlue,
                        KqPadThemeColors.db46,
                        KqPadThemeColors.bgBlue
                      ],
                      burstValue: ['10', '30.5'],
                      min: 10,
                      max: 50.5,
                      currentValue: '49',
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Container(
                    color: KqPadThemeColors.bgBlue,
                    width: 600,
                    child: KqPadVerticalSteps(
                      totalStepNum: 4,
                      titles: ['步骤一', '步骤二', '步骤三', '步骤四'],
                      contentCallback: (index) {
                        return Container(
                          decoration: const KqBoxDecoration(
                            color: KqPadThemeColors.bgBlack05,
                            borderRadius: BorderRadius.all(Radius.circular(2)),
                            boxShadow: [
                              KqBoxShadow(
                                color: KqPadThemeColors.bgF5,
                                offset: Offset(0, 1),
                                blurRadius: 0,
                                spreadRadius: 0,
                              ),
                            ],
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8.dm, vertical: 6.dm),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(
                                  '1、优化锁模力设定值，降低模具损伤，提高模具使用寿命。',
                                  style: TextStyle(
                                      fontSize: 7.dm,
                                      color: KqPadThemeColors.text26),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  KqPadFilterCheckView(
                    onTap: () {
                      setState(() {
                        selected = !selected;
                      });
                    },
                    text: '长飞亚VE系列',
                    selected: selected,
                    width: 64.dm,
                    height: 19.dm,
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Row(
                    children: [
                      Text(
                        '生产能耗',
                        style: TextStyle(
                            fontSize: 8.dm, color: KqPadThemeColors.text26),
                      ),
                    ],
                  ),
                  KqPointMultiProgressBar(
                    gradientColors: const [
                      Color(0xffFF453A),
                      Color(0xffFFEF09),
                      Color(0xff34C759),
                    ],
                    data: [
                      KqPointMultiProgressData(
                        progress: 0,
                      ),
                      KqPointMultiProgressData(
                        progress: 100,
                      ),
                      KqPointMultiProgressData(
                        progress: 0,
                        highlight: true,
                        highlightText: '0',
                      ),
                      KqPointMultiProgressData(
                        progress: 100,
                        highlight: true,
                        highlightText: '100',
                      )
                    ],
                  ),
                  SizedBox(
                    height: 100.dm,
                  ),
                  SizedBox(
                    width: 36.dm,
                    height: 16.dm,
                    child: KqPadTextFiled(
                      useParentHeight: true,
                      showShadow: true,
                      fontSize: 8.dm,
                      textAlign: TextAlign.center,
                      newValue: '98.78',
                      inputType: TextInputType.number,
                      inputFormatters:
                          TextInputFormatUtils.decimal(decimalLength: 2),
                    ),
                  ),
                  SizedBox(
                    height: 10.dm,
                  ),
                  Row(
                    children: [
                      Expanded(child: Text("333")),
                      SizedBox(
                        width: 36.dm,
                        height: 12.dm,
                        child: KqPadTextFiled(
                          useParentHeight: true,
                          showShadow: true,
                          fontSize: 8.dm,
                          textAlign: TextAlign.center,
                          newValue: '98.78',
                          inputType: TextInputType.number,
                          inputFormatters:
                              TextInputFormatUtils.decimal(decimalLength: 2),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10.dm),
                  KqPadRecordSteps(
                    currentIndex: 3,
                    callBack: (index, step) {},
                    data: [
                      '初始',
                      '缺料',
                      '末端困气缺料',
                      '飞边',
                      '飞边',
                      '光泽不良',
                      '黑点/杂志',
                      '顶凸',
                      '顶凸',
                      '顶凸',
                      '顶凸'
                    ]
                        .map((e) => KqPadBaseRecordEntity(
                            defectName: e, enabled: e == '顶凸' ? 1 : 0))
                        .toList(),
                  ),
                  SizedBox(
                    height: 100.dm,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
