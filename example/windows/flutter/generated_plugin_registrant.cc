//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <kq_flutter_pad_widgets/kq_flutter_pad_widgets_plugin_c_api.h>
#include <objectbox_flutter_libs/objectbox_flutter_libs_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  KqFlutterPadWidgetsPluginCApiRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("KqFlutterPadWidgetsPluginCApi"));
  ObjectboxFlutterLibsPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("ObjectboxFlutterLibsPlugin"));
}
